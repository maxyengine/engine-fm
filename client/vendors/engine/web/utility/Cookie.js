let self = engine.web.utility.Cookie = class {

    static enabled() {
        return navigator.cookieEnabled;
    }

    static set(name, value, options = {}) {
        if (!self.enabled()) {
            return;
        }

        if (options.expires) {
            options.expires = options.expires.toUTCString();
        }

        let cookie = name + '=' + encodeURIComponent(value);
        for (let key in options) {
            cookie += ';' + (key !== 'secure' ? key + '=' + options[key] : key);
        }

        document.cookie = cookie;
    }

    static get(name, byDefault = null) {
        if (!self.enabled()) {
            return byDefault;
        }

        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));

        return matches ? decodeURIComponent(matches[1]) : byDefault;
    }

    static remove(name) {
        if (!self.enabled()) {
            return;
        }

        self.set(name, '', {
            expires: new Date(Date.now() - 1000)
        });
    }
}
