let
    Query = engine.web.utility.Query;

engine.web.value.Url = class {

    constructor(href, basePathname) {
        let link = document.createElement('a');
        link.href = href;
        this.protocol = link.protocol && link.protocol.slice(0, -1);
        this.hostname = link.hostname;
        this.port = link.port ? link.port : 80;
        this.basePathname = basePathname;
        this.pathname = link.pathname;
        this.search = link.search && link.search.slice(1);
        this.hash = link.hash && link.hash.slice(1);
        if (this.basePathname) {
            this.pathname = this.pathname.slice(this.basePathname.length);
        }
    }

    get href() {
        return this.toString();
    }

    get params() {
        let params = {};
        if (this.search) {
            Query.parse(this.search, params);
        }

        return params;
    }

    set params(params) {
        this.search = Query.stringify(params);
    }

    toString() {
        let href = '';
        if (this.protocol) {
            href += this.protocol + '://';
        }
        if (this.hostname) {
            href += this.hostname;
        }
        if (this.port && 80 !== this.port) {
            href += ':' + this.port;
        }
        if (this.basePathname) {
            href += '/' + this.basePathname.replace(/^\/|\/$/g, '');
        }
        if (this.pathname) {
            href += '/' + this.pathname.replace(/^\/|\/$/g, '');
        }
        if (this.search) {
            href += '?' + this.search;
        }
        if (this.hash) {
            href += '#' + this.hash;
        }

        return href;
    }
}
