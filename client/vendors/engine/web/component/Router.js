let
    Component = engine.react.Component,
    Url = engine.web.value.Url;

engine.web.component.Router = class extends Component {

    get stateStack() {
        return window.history;
    }

    initialize() {

        window.onpopstate = event => {
            let href = event.state ? event.state.href : this.createLocalUrl().href,
                url = new Url(href, this.basePathname);
            this.setState(url.pathname, url.params);
        };
    }

    switchOnLocation() {
        let url = this.createLocalUrl();
        this.setState(url.pathname, url.params);
    }

    switchState(state, params = {}) {
        let url = this.createUrl(state, params);
        this.pushState(url.href);
        this.setState(url.pathname, url.params);
    }

    pushState(href) {
        this.stateStack.pushState({href: href}, '', href);
    }

    setState(state, params) {
        this.trigger('switchState', {
            state: state,
            params: params
        });
        this.trigger(state, params);
    }

    createUrl(pathname, params) {
        let url = new Url(this.basePathname ? this.basePathname + pathname : pathname, this.basePathname);
        url.params = params;

        return url;
    }

    createLocalUrl() {
        return new Url(location, this.basePathname);
    }
}
