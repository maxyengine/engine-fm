let
    Object = engine.lang.type.Object,
    Cookie = engine.web.utility.Cookie;

engine.web.component.User = class extends Object {

    read(name, byDefault = null) {
        return this.load()[name] || byDefault;
    }

    write(name, value) {
        let data = this.load();
        data[name] = value;
        this.store(data);
    }

    load() {
        if (!this.applicationId) {
            return {};
        }

        let cookie = Cookie.get(this.applicationId);

        return cookie !== null ? JSON.parse(cookie) : {};
    }

    store(data) {
        Cookie.set(this.applicationId, JSON.stringify(data), {
            expires: new Date(Date.now() + this.expires)
        });
    }
}
