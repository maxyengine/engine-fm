let
    Object = engine.lang.type.Object;

engine.web.component.Model = class extends Object {

    request(request, ...observers) {
        this.client.exchange(request, ...observers);
    }

    get(request, ...observers) {
        request.method = 'GET';
        this.request(request, ...observers);
    }

    post(request, ...observers) {
        request.method = 'POST';
        this.request(request, ...observers);
    }
}
