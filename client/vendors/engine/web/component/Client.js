let
    Component = engine.react.Component,
    Observable = engine.react.Observable,
    Obj = engine.lang.utility.Object;

engine.web.component.Client = class extends Component {

    exchange(request, ...observers) {
        let
            client = new XMLHttpRequest(),
            eventResource = this.mergeObservers(...observers);

        client.onload = client.onerror = () => {
            let event = {
                request: request,
                response: {
                    status: {
                        code: client.status,
                        text: client.statusText
                    },
                    body: client.responseText
                }
            };

            eventResource.trigger('close', event);

            switch (String(client.status).charAt(0)) {
                case '1':
                    eventResource.trigger('information', event);
                    break;
                case '2':
                    eventResource.trigger('success', event);
                    break;
                case '3':
                    eventResource.trigger('redirection', event);
                    break;
                case '4':
                    eventResource.trigger('clientError', event);
                    break;
                case '5':
                    eventResource.trigger('serverError', event);
                    break;
            }

            eventResource.trigger('statusCode' + client.status, event);
        };

        client.upload.onprogress = progress => {
            if (progress.lengthComputable) {
                request.prevProgress = request.progress || null;
                request.progress = progress;

                eventResource.trigger('progress', {
                    request: request
                });
            }
        };

        eventResource.trigger('before', {
            request: request
        });

        client.open(request.method, request.url, true);

        request.abort = () => {
            client.abort();
            eventResource.trigger('abort', {
                request: request
            });
        };

        eventResource.trigger('open', {
            request: request
        });

        request.headers && Obj.forEach(request.headers, (name, value) => {
            client.setRequestHeader(name, value);
        });

        client.send(request.body);
    }

    mergeObservers(...observers) {
        let eventResource = new Observable(...this.eventResource.observers);

        return eventResource.addObservers(...observers);
    }
}
