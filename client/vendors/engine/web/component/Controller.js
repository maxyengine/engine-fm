let
    Component = engine.react.Component,
    Obj = engine.lang.utility.Object,
    Type = engine.lang.utility.Type;

engine.web.component.Controller = class extends Component {

    _constructor(...args) {
        super._constructor(...args);
        this.actionMap = this.actions || {};
        this.subscribeActions(this.actionMap);
    }

    subscribeActions(actions) {
        Obj.forEach(actions, (eventName, action) => {
            this.subscribeAction(eventName, action);
        });
    }

    subscribeAction(eventName, action) {
        this.actionMap[eventName] = action;
        this.on(eventName, (...args) => {
            this.executeAction(eventName, ...args);
        });
    }

    getAction(eventName) {
        let action = this.actionMap[eventName];
        if (Type.isObject(action)) {
            return action;
        }
        action = new action(this.properties || {}, this);
        this.actionMap[eventName] = action;

        return action;
    }

    executeAction(eventName, ...args) {
        if (!this.isActionAllowed(eventName)) {
            return;
        }
        try {
            this.getAction(eventName).execute(...args);
        } catch (throwable) {
            this.handleActionError(throwable);
        }
    }

    isActionAllowed(eventName) {
        return true;
    }

    handleActionError(throwable) {
        throw throwable;
    }

    render(data) {
        this.view.set(data);
    }
}
