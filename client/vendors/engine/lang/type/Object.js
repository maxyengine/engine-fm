let
    Obj = engine.lang.utility.Object,
    Type = engine.lang.utility.Type;

engine.lang.type.Object = class {

    constructor(properties = {}, context) {
        this._constructor(properties, context);
    }

    _constructor(properties, context) {
        this.set(Obj.merge(this.defaults || {}, properties), context);
        this.use(...this.traits || []);
        this.initialize && this.initialize();
    }

    set(property, value, context) {
        if (Type.isObject(property)) {
            context = value;
            Obj.forEach(property, (name, value) => {
                this.set(name, value, context);
            });
        } else if (Type.isFunction(value) && context) {
            this[property] = (...args) => {
                return value.call(context, ...args);
            };
        } else {
            this[property] = value;
        }

        return this;
    }

    use(...traits) {
        traits.forEach(trait => {
            //todo: check if trait have used already
            if (!Type.isObject(trait)) {
                trait = new trait({owner: this});
            }
            trait.properties && this.set(trait.properties, trait);
        });

        return this;
    }
}
