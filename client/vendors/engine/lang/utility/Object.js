let self = engine.lang.utility.Object = class {

    static forEach(object, callback) {
        for (let property in object) {
            if (object.hasOwnProperty(property)) {
                callback(property, object[property]);
            }
        }
    }

    static merge(result, ...objects) {
        objects.forEach(object => {
            self.forEach(object, (property, value) => {
                result[property] = value;
            });
        });

        return result;
    }
}
