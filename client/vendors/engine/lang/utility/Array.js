engine.lang.utility.Array = class {

    static contains(list, value) {
        return list.indexOf(value) > -1;
    }
}
