let self = engine.lang.utility.String = class {

    static capitalize(string) {
        return self.upperCaseFirst(string);
    }

    static format(string, ...args) {
        return string.replace(/{(\d+)}/g, (match, number) => {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    }

    static contains(string, substring, caseSensitive = true) {
        return caseSensitive ?
            string.indexOf(substring) >= 0 :
            string.toLocaleLowerCase().indexOf(substring.toLocaleLowerCase()) >= 0;
    }

    static lowerCaseFirst(string) {
        return string.charAt(0).toLocaleLowerCase() + string.slice(1);
    }

    static upperCaseFirst(string) {
        return string.charAt(0).toLocaleUpperCase() + string.slice(1);
    }
}
