let self = engine.lang.utility.Type = class {

    static isString(value) {
        return typeof value === 'string';
    }

    static isNumber(value) {
        return typeof value === 'number';
    }

    static isObject(value, strict = true) {
        return typeof value === 'object' && (!strict || !self.isArray(value));
    }

    static isFunction(value) {
        return typeof value === 'function';
    }

    static isArray(value) {
        return value instanceof Array;
    }

    static arrayToObject(value, recursively) {
        let result = {};

        for (let i = 0; i < value.length; ++i) {
            result[i] = recursively && self.isArray(value[i]) ?
                self.arrayToObject(value[i], recursively) :
                value[i];
        }

        return result;
    }
}
