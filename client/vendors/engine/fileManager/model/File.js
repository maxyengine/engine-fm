let
    Model = engine.web.component.Model;

engine.fileManager.model.File = class extends Model {

    open(params) {
        this.client.open('open', {id: params.id}, '_blank');
    }

    openHyperlink(params) {
        window.open(params.url, '_blank');
    }

    download(params) {
        this.client.open('download', {id: params.id});
    }

    read(bodyParams, ...observers) {
        this.post({
            target: bodyParams.pattern ? 'search' : 'read',
            bodyParams: bodyParams
        }, ...observers);
    }

    upload(form, ...observers) {
        this.post({
            target: 'upload',
            body: form
        }, ...observers);
    }

    copy(params, ...observers) {
        this.post({
            target: 'copy',
            bodyParams: {
                id: params.id,
                parentId: params.parentId
            }
        }, ...observers);
    }

    move(params, ...observers) {
        this.post({
            target: 'move',
            bodyParams: {
                id: params.id,
                parentId: params.parentId
            }
        }, ...observers);
    }

    createDirectory(params, ...observers) {
        this.post({
            target: 'create/directory',
            bodyParams: {
                parentId: params.parentId,
                name: params.name
            }
        }, ...observers);
    }

    createHyperlink(params, ...observers) {
        this.post({
            target: 'create/hyperlink',
            bodyParams: {
                parentId: params.parentId,
                name: params.name,
                url: params.url
            }
        }, ...observers);
    }

    rename(params, ...observers) {
        this.post({
            target: 'rename',
            bodyParams: {
                id: params.id,
                name: params.name
            }
        }, ...observers);
    }

    setPermissions(params, ...observers) {
        this.post({
            target: 'permissions',
            bodyParams: {
                id: params.id,
                permissions: params.permissions
            }
        }, ...observers);
    }

    remove(params, ...observers) {
        this.post({
            target: 'remove',
            bodyParams: {
                id: params.id
            }
        }, ...observers);
    }

    trash(params, ...observers) {
        this.post({
            target: 'trash',
            bodyParams: {
                id: params.id
            }
        }, ...observers);
    }
}
