let
    Object = engine.lang.type.Object;

engine.fileManager.template.fileList.breadcrumbs.Item = class extends Object {

    initialize() {
        let element0 = document.createElement('span');element0.setAttribute('class', 'item');this.owner._element = element0;let element1 = document.createTextNode('');this.owner._caption = element1;element0.appendChild(element1);
    }
}
