let
    Object = engine.lang.type.Object;

engine.fileManager.template.fileList.breadcrumbs.Separator = class extends Object {

    initialize() {
        let element0 = document.createElement('span');element0.setAttribute('class', 'engine-fileManager__list-breadcrumbs-separator');this.owner._element = element0;let element1 = document.createTextNode('/');element0.appendChild(element1);
    }
}
