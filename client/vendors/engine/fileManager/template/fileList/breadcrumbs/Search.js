let
    Object = engine.lang.type.Object;

engine.fileManager.template.fileList.breadcrumbs.Search = class extends Object {

    initialize() {
        let element0 = document.createElement('span');element0.setAttribute('class', 'item');this.owner._element = element0;let element1 = document.createTextNode('');element0.appendChild(element1);let element2 = document.createElement('i');element2.setAttribute('class', 'engine-fileManager__i icon-search');element0.appendChild(element2);let element3 = document.createTextNode('');this.owner._caption = element3;element0.appendChild(element3);
    }
}
