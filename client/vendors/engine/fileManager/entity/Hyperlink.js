let
    File = engine.fileManager.entity.File;

let self = engine.fileManager.entity.Hyperlink = class extends File {

    get url() {
        return this._data.url;
    }
}
