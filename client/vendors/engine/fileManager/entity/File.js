let
    Size = engine.fileManager.value.Size;

engine.fileManager.entity.File = class {

    constructor(data) {
        this._data = data;
    }

    get id() {
        return this._data.id;
    }

    get prevId() {
        return this._data.prevId;
    }

    get parentId() {
        return this._data.parentId;
    }

    get class() {
        return this._data.class;
    }

    get breadcrumbs() {
        return this._data.breadcrumbs;
    }

    get baseName() {
        return this._data.baseName;
    }

    get name() {
        return this._data.name;
    }

    set name(value) {
        this._data.name = value;
    }

    get type() {
        return this._data.type;
    }

    get extension() {
        return this._data.extension;
    }

    get permissions() {
        return this._data.permissions;
    }

    get size() {
        if (!this._size) {
            this._size = new Size(this._data.size);
        }

        return this._size;
    }

    get lastModified() {
        if (!this._lastModified) {
            this._lastModified = new Date(this._data.lastModified * 1000);
        }

        return this._lastModified;
    }

    get isTrash() {
        return '$trash' === this.name;
    }

    inBreadcrumbs(id) {
        for (let i = 0; this.breadcrumbs[i]; i++) {
            if (this.breadcrumbs[i].id === id) {
                return true;
            }
        }

        return false;
    }
}
