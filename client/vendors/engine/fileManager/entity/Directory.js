let
    File = engine.fileManager.entity.File,
    Hyperlink = engine.fileManager.entity.Hyperlink;

let self = engine.fileManager.entity.Directory = class extends File {

    get pattern() {
        return this._data.pattern;
    }

    get names() {
        if (!this._names) {
            this._names = [];
            this._data.children && this._data.children.forEach(childRaw => {
                this._names.push(childRaw.name);
            });
        }

        return this._names;
    }

    get children() {
        if (!this._children) {
            this._children = [];
            this._data.children && this._data.children.forEach(childRaw => {
                switch (childRaw.class) {
                    case 'file':
                        this._children.push(new File(childRaw));
                        break;
                    case 'hyperlink':
                        this._children.push(new Hyperlink(childRaw));
                        break;
                    case 'directory':
                        this._children.push(new self(childRaw));
                        break;
                }
            });
        }

        return this._children;
    }

    hasChildren() {
        return !!this.children.length;
    }

    containsName(name) {
        return this.names.indexOf(name) > -1;
    }
}
