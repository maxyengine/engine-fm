let
    DirectoryEntity = engine.fileManager.entity.Directory;

engine.fileManager.entity.factory.Directory = class {

    createEntity(raw) {
        return new DirectoryEntity(raw);
    }

    createCollection(collectionRaw) {
        let collection = [];
        collectionRaw.forEach(raw => {
            collection.push(this.createEntity(raw));
        });

        return collection;
    }
}
