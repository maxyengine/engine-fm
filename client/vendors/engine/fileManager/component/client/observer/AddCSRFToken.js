let
    Observer = engine.react.Observer;

engine.fileManager.component.client.observer.AddCSRFToken = class extends Observer {

    onBefore(event) {
        event.request.headers = event.request.headers || {};
        event.request.headers[this.owner.csrfTokenName] = this.owner.csrfToken;
    }
}
