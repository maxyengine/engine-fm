let
    Observer = engine.react.Observer;

engine.fileManager.component.client.observer.MakeRequestURL = class extends Observer {

    onBefore(event) {
        if (event.request.target) {
            event.request.url = this.owner.createUrl(event.request.target, event.request.queryParams);
        }
    }
}
