let
    Observer = engine.react.Observer;

engine.fileManager.component.client.observer.ManageRequestProgress = class extends Observer {

    get requestProgress() {
        return this.owner.requestProgress;
    }

    onOpen() {
        this.requestProgress.show();
    }

    onProgress() {
        this.requestProgress.show();
    }

    onClose() {
        this.requestProgress.hide();
    }
}
