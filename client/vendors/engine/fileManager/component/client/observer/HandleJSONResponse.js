let
    Observer = engine.react.Observer,
    Type = engine.lang.utility.Type,
    DirectoryFactory = engine.fileManager.entity.factory.Directory;

engine.fileManager.component.client.observer.HandleJSONResponse = class extends Observer {

    get factory() {
        if (!this._factory) {
            this._factory = new DirectoryFactory();
        }
        
        return this._factory
    }

    onSuccess(event) {
        let parsedBody = JSON.parse(event.response.body);

        if (Type.isArray(parsedBody)) {
            event.collection = this.factory.createCollection(parsedBody);
        } else {
            event.entity = this.factory.createEntity(parsedBody);
        }
    }
}
