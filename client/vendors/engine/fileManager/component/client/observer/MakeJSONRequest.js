let
    Observer = engine.react.Observer;

engine.fileManager.component.client.observer.MakeJSONRequest = class extends Observer {

    onBefore(event) {
        if (event.request.bodyParams) {
            event.request.body = JSON.stringify(event.request.bodyParams);
        }
    }
}
