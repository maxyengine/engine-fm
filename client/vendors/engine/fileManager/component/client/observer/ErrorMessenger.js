let
    Observer = engine.react.Observer;

engine.fileManager.component.client.observer.ErrorMessenger = class extends Observer {

    get errorManager() {
        return this.owner.errorManager;
    }

    onClientError(event) {
        this.errorManager.handleError(event.response.status.text);
    }

    onServerError(event) {
        this.errorManager.handleError(event.response.status.text);
    }

    onError(throwable) {
        this.errorManager.handleError(throwable);
    }
}
