let
    Object = engine.lang.type.Object,
    Type = engine.lang.utility.Type;

engine.fileManager.component.ErrorManager = class extends Object {

    handleError(error) {
        if (Type.isString(error)) {
            this.showErrorMessage(error);
        }
    }

    showErrorMessage(message) {
        let
            container = this.container,
            flash = document.createElement('div');

        flash.appendChild(document.createTextNode(message));
        flash.className = 'flash errors';

        container.appendChild(flash);
        setTimeout(() => {
            container.removeChild(flash);
        }, 3000);
    }
}
