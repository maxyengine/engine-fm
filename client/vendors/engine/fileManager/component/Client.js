let
    WebClient = engine.web.component.Client,
    MakeRequestURL = engine.fileManager.component.client.observer.MakeRequestURL,
    AddCSRFToken = engine.fileManager.component.client.observer.AddCSRFToken,
    MakeJSONRequest = engine.fileManager.component.client.observer.MakeJSONRequest,
    HandleJSONResponse = engine.fileManager.component.client.observer.HandleJSONResponse,
    ManageRequestProgress = engine.fileManager.component.client.observer.ManageRequestProgress,
    ErrorMessenger = engine.fileManager.component.client.observer.ErrorMessenger,
    Query = engine.web.utility.Query;

engine.fileManager.component.Client = class extends WebClient {

    get traits() {
        return [
            MakeRequestURL,
            AddCSRFToken,
            MakeJSONRequest,
            HandleJSONResponse,
            ManageRequestProgress,
            ErrorMessenger
        ];
    }

    open(target, queryParams, ...args) {
        queryParams[this.csrfTokenName] = this.csrfToken;
        window.open(this.createUrl(target, queryParams), ...args);
    }

    createUrl(target, queryParams = {}) {
        let url = this.serverUrl;

        queryParams.action = target;

        url += '?' + Query.stringify(queryParams);

        return url;
    }
}
