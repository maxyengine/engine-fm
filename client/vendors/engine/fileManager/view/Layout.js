let
    View = engine.gui.component.View,
    IconFactory = engine.fileManager.view.icon.Factory,
    Toolbar = engine.fileManager.view.Toolbar,
    List = engine.fileManager.view.fileList.Layout,
    SwitchLeft = engine.fileManager.view.fileList.trigger.SwitchLeft,
    SwitchRight = engine.fileManager.view.fileList.trigger.SwitchRight,
    ConfirmDialog = engine.fileManager.view.control.ModalConfirmDialog,
    ChooserDialog = engine.fileManager.view.control.ModalChooserDialog,
    InputDialog = engine.fileManager.view.control.ModalInputDialog,
    HyperlinkDialog = engine.fileManager.view.control.ModalHyperlinkDialog,
    UploadProgressDialog = engine.fileManager.view.control.uploadProgress.ModalDialog,
    RequestProgress = engine.fileManager.view.control.RequestProgress,
    Template = engine.fileManager.template.Layout;

engine.fileManager.view.Layout = class extends View {

    get template() {
        return Template;
    }

    initialize() {
        let icon = new IconFactory({
            iconClasses: this.config.icons,
            defaultIconClass: this.config.defaultIcon
        });

        this.toolbar = new Toolbar({
            icon: icon,
            hotKeys: this.config.hotKeys
        });

        this.uploadProgressDialog = new UploadProgressDialog({
            icon: icon
        });

        this.leftList = new List({
            hotKeys: this.config.hotKeys,
            uploadProgressDialog: this.uploadProgressDialog,
            icon: icon
        });
        this.leftList.use(SwitchRight);

        this.rightList = new List({
            hotKeys: this.config.hotKeys,
            uploadProgressDialog: this.uploadProgressDialog,
            icon: icon
        });
        this.rightList.use(SwitchLeft);

        this.confirmDialog = new ConfirmDialog({
            icon: icon
        });

        this.inputDialog = new InputDialog({
            icon: icon
        });

        this.hyperlinkDialog = new HyperlinkDialog({
            icon: icon
        });

        this.chooserDialog = new ChooserDialog({
            icon: icon
        });

        this.requestProgress = new RequestProgress();
    }

    render() {
        this.clear();

        this.element.appendChild(this.toolbar.render());
        this.element.appendChild(this.leftList.element);
        this.element.appendChild(this.rightList.element);
        this.element.appendChild(this.inputDialog.element);
        this.element.appendChild(this.hyperlinkDialog.element);
        this.element.appendChild(this.confirmDialog.element);
        this.element.appendChild(this.chooserDialog.element);
        this.element.appendChild(this.uploadProgressDialog.element);
        this.element.appendChild(this.requestProgress.element);

        this.appendTo(this.wrapper);
    }
}
