let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.Logo;

engine.fileManager.view.Logo = class extends View {

    get traits() {
        return [
            Template
        ];
    }
}
