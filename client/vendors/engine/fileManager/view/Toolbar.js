let
    View = engine.gui.component.View,
    Button = engine.fileManager.view.control.Button,
    UploadButton = engine.fileManager.view.control.UploadButton,
    Logo = engine.fileManager.view.Logo,
    Template = engine.fileManager.template.Toolbar;

engine.fileManager.view.Toolbar = class extends View {

    get template() {
        return Template;
    }

    initialize() {


        this.logo = new Logo();

        this.open = new Button({
            icon: this.icon.create('folderOpen'),
            caption: 'Open',
            title: this.hotKeys.openKey
        });

        this.rename = new Button({
            icon: this.icon.create('edit'),
            caption: 'Rename',
            title: this.hotKeys.renameKey
        });

        this.permissions = new Button({
            icon: this.icon.create('permissions'),
            caption: 'Permissions',
            title: this.hotKeys.permissionsKey
        });

        this.copy = new Button({
            icon: this.icon.create('copy'),
            caption: 'Copy',
            title: this.hotKeys.copyKey
        });

        this.move = new Button({
            icon: this.icon.create('move'),
            caption: 'Move',
            title: this.hotKeys.moveKey
        });

        this.trash = new Button({
            icon: this.icon.create('trash'),
            caption: 'Trash',
            title: this.hotKeys.trashKey
        });

        this.remove = new Button({
            icon: this.icon.create('remove'),
            caption: 'Remove',
            title: this.hotKeys.removeKey
        });

        this.createDirectory = new Button({
            icon: this.icon.create('dir'),
            caption: 'Create Folder',
            title: this.hotKeys.createFolderKey
        });

        this.createHyperlink = new Button({
            icon: this.icon.create('hyperlink'),
            caption: 'Create Hyperlink',
            title: this.hotKeys.createHyperlinkKey
        });

        this.upload = new UploadButton({
            icon: this.icon.create('upload'),
            caption: 'Upload',
            title: this.hotKeys.uploadKey
        });

        this.download = new Button({
            icon: this.icon.create('download'),
            caption: 'Download',
            title: this.hotKeys.downloadKey
        });

        this.search = new Button({
            icon: this.icon.create('search'),
            caption: 'Search',
            title: this.hotKeys.searchKey
        });

        this.openTrash = new Button({
            icon: this.icon.create('trash'),
            caption: 'Open Trash'
        });
    }

    get events() {
        return {
            open: {open: 'click'},
            rename: {rename: 'click'},
            permissions: {permissions: 'click'},
            copy: {copy: 'click'},
            move: {move: 'click'},
            trash: {trash: 'click'},
            remove: {remove: 'click'},
            createDirectory: {createDirectory: 'click'},
            createHyperlink: {createHyperlink: 'click'},
            upload: {upload: 'selectFiles'},
            download: {download: 'click'},
            search: {search: 'click'},
            openTrash: {openTrash: 'click'}
        }
    }

    render() {
        this.clear();

        this.element.appendChild(this.logo.element);
        this.element.appendChild(this.open.element);
        this.element.appendChild(this.rename.element);
        this.element.appendChild(this.permissions.element);
        this.element.appendChild(this.copy.element);
        this.element.appendChild(this.move.element);
        this.element.appendChild(this.trash.element);
        this.element.appendChild(this.remove.element);
        this.element.appendChild(this.createDirectory.element);
        this.element.appendChild(this.createHyperlink.element);
        this.element.appendChild(this.upload.element);
        this.element.appendChild(this.download.element);
        this.element.appendChild(this.search.element);
        this.element.appendChild(this.openTrash.element);

        return this.element;
    }
}
