let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.fileList.Body;

engine.fileManager.view.fileList.Body = class extends View {

    get template() {
        return Template;
    }

    render() {
        this.clear();
        this.element.appendChild(this.table);
        this.items.forEach(item => {
            this.table.appendChild(item.element);
        });

        return this.element;
    }
}
