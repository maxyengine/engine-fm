let
    Object = engine.lang.type.Object,
    Parent = engine.fileManager.view.fileList.item.Parent,
    File = engine.fileManager.view.fileList.item.File;

engine.fileManager.view.fileList.item.Factory = class extends Object {

    createFromDirectory(directory) {
        let items = [];

        if (directory.pattern) {
            items.push(new Parent({
                entity: {
                    id: directory.id,
                    isParent: true
                }
            }));
        } else if (directory.parentId) {
            items.push(new Parent({
                entity: {
                    id: directory.parentId,
                    isParent: true
                }
            }));
        }

        directory.children && directory.children.forEach(entity => {
            let icon = null;
            switch (entity.class) {
                case 'directory':
                    icon = this.icon.create('dir');
                    break;
                case 'file':
                    icon = this.icon.create(entity.type);
                    break;
                case 'hyperlink':
                    icon = this.icon.create('hyperlink');
                    break;
            }

            items.push(new File({
                icon: icon,
                entity: entity
            }));
        });

        return items;
    }
}
