let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.fileList.item.Parent;

engine.fileManager.view.fileList.item.Parent = class extends View {

    get template() {
        return Template;
    }
};
