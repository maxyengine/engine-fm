let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.fileList.item.File;

engine.fileManager.view.fileList.item.File = class extends View {

    get template() {
        return Template;
    }

    initialize() {
        // icon
        this.contents[1].insertBefore(this.icon.element, this.contents[1].firstChild);

        // fields
        this.name.nodeValue = this.entity.baseName;
        this.extension.nodeValue = this.entity.extension.toLocaleLowerCase();
        this.size.nodeValue = this.entity.size.toHumanString();
        this.permissions.nodeValue = this.entity.permissions;
        this.modified.nodeValue = this.entity.lastModified.toLocaleString();
    }
}
