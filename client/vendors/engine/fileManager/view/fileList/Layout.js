let
    View = engine.gui.component.View,
    Activity = engine.gui.trigger.Activity,
    HotKeys = engine.gui.trigger.HotKeys,
    Breadcrumbs = engine.fileManager.view.fileList.breadcrumbs.Layout,
    UploadProgress = engine.fileManager.view.fileList.UploadProgress,
    Header = engine.fileManager.view.fileList.Header,
    Body = engine.fileManager.view.fileList.Body,
    Parent = engine.fileManager.view.fileList.item.Parent,
    ItemsFactory = engine.fileManager.view.fileList.item.Factory,
    Open = engine.fileManager.view.fileList.trigger.Open,
    Select = engine.fileManager.view.fileList.trigger.Select,
    Trash = engine.fileManager.view.fileList.trigger.Trash,
    Remove = engine.fileManager.view.fileList.trigger.Remove,
    Rename = engine.fileManager.view.fileList.trigger.Rename,
    Permissions = engine.fileManager.view.fileList.trigger.Permissions,
    Copy = engine.fileManager.view.fileList.trigger.Copy,
    Move = engine.fileManager.view.fileList.trigger.Move,
    Upload = engine.fileManager.view.fileList.trigger.Upload,
    Download = engine.fileManager.view.fileList.trigger.Download,
    Search = engine.fileManager.view.fileList.trigger.Search,
    CreateDirectory = engine.fileManager.view.fileList.trigger.CreateDirectory,
    CreateHyperlink = engine.fileManager.view.fileList.trigger.CreateHyperlink,
    ListStyle = engine.fileManager.view.fileList.observer.ListStyle,
    ListScrolling = engine.fileManager.view.fileList.observer.ListScrolling,
    ListSorting = engine.fileManager.view.fileList.observer.ListSorting,
    ResizeParent = engine.fileManager.view.fileList.observer.ResizeParent,
    CheckboxSelection = engine.fileManager.view.fileList.observer.CheckboxSelection,
    Template = engine.fileManager.template.fileList.Layout;

engine.fileManager.view.fileList.Layout = class extends View {

    get template() {
        return Template;
    }

    get traits() {
        return [
            Activity,
            HotKeys,
            Open,
            Select,
            Trash,
            Remove,
            Rename,
            Permissions,
            Copy,
            Move,
            Upload,
            Download,
            Search,
            CreateDirectory,
            CreateHyperlink,
            ListStyle,
            ListScrolling,
            ListSorting,
            ResizeParent,
            CheckboxSelection
        ];
    }

    get events() {
        return {
            selectUploadedFiles: {uploadInput: 'change'}
        }
    }

    onSelectUploadedFiles(event) {
        let files = Array.prototype.slice.call(event.target.files || event.dataTransfer.files);
        this.trigger('upload', {
            files: files
        });
        this.uploadInput.value = null;
    }

    initialize() {
        this.itemsFactory = new ItemsFactory({
            icon: this.icon
        });

        this.uploadProgress = new UploadProgress({
            uploadProgressDialog: this.uploadProgressDialog
        });
    }

    render(directory) {
        if (directory) {
            this.directory = directory;
        }

        this.clear();
        this.trigger('beforeRender');

        this.items = this.itemsFactory.createFromDirectory(this.directory);
        this.breadcrumbs = new Breadcrumbs({
            directory: this.directory,
            icon: this.icon
        });
        this.header = new Header({
            icon: this.icon
        });
        this.body = new Body({
            icon: this.icon,
            items: this.items
        });
        this.element.appendChild(this.breadcrumbs.render());
        this.element.appendChild(this.header.element);
        this.element.appendChild(this.body.render());
        this.element.appendChild(this.uploadProgress.element);

        this.setHeaderWidth();
        this.trigger('render');

        return this.element;
    }

    setHeaderWidth() {
        if (this.body.element.clientWidth !== this.body.element.offsetWidth) {
            this.header.element.style.width = this.body.element.clientWidth + 'px';
        }
        if (!this.items.length || (this.items[0] instanceof Parent && 1 === this.items.length)) {
            return;
        }
        let item = this.items[0] instanceof Parent ? this.items[1] : this.items[0];
        item.contents.forEach((content, index) => {
            this.header.contents[index].style.width = content.offsetWidth + 'px';
        });
    }
}
