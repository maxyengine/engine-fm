let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.fileList.breadcrumbs.Root;

engine.fileManager.view.fileList.breadcrumbs.Root = class extends View {

    get template() {
        return Template;
    }
}
