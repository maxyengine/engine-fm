let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.fileList.breadcrumbs.Separator;

engine.fileManager.view.fileList.breadcrumbs.Separator = class extends View {

    get template() {
        return Template;
    }
}
