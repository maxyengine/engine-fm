let
    View = engine.gui.component.View,
    Root = engine.fileManager.view.fileList.breadcrumbs.Root,
    Item = engine.fileManager.view.fileList.breadcrumbs.Item,
    Search = engine.fileManager.view.fileList.breadcrumbs.Search,
    Separator = engine.fileManager.view.fileList.breadcrumbs.Separator,
    Template = engine.fileManager.template.fileList.breadcrumbs.Layout;

engine.fileManager.view.fileList.breadcrumbs.Layout = class extends View {

    get template() {
        return Template;
    }

    render() {
        this.clear();
        this.items = [];

        this.directory.breadcrumbs.forEach((pathItem, index) => {
            let item = null;

            if (0 === index) {
                item = new Root({
                    id: pathItem.id
                });
            } else {
                this.element.appendChild((new Separator()).element);
                item = new Item({
                    id: pathItem.id,
                    caption: pathItem.name
                });
            }

            this.element.appendChild(item.element);
            this.items.push(item);
        });

        if (this.directory.pattern) {
            this.element.appendChild((new Separator()).element);
            this.element.appendChild((new Search({caption: this.directory.pattern})).element);
        }

        return this.element;
    }
}
