let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.fileList.breadcrumbs.Search;

engine.fileManager.view.fileList.breadcrumbs.Search = class extends View {

    get template() {
        return Template;
    }

    set caption(value) {
        this._caption.nodeValue = value;
    }
}
