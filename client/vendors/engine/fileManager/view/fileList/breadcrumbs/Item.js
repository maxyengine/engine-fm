let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.fileList.breadcrumbs.Item;

engine.fileManager.view.fileList.breadcrumbs.Item = class extends View {

    get template() {
        return Template;
    }

    set caption(value) {
        this._caption.nodeValue = value;
    }
}
