let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Remove = class extends Observer {

    onRemoveKey(keyboardEvent, view) {
        if (view.hasActiveItem()) {
            view.trigger('remove', {
                index: view.getActiveIndex()
            });
        }
    }
}
