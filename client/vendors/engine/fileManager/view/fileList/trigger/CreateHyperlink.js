let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.CreateHyperlink = class extends Observer {

    onCreateHyperlinkKey() {
        this.owner.trigger('createHyperlink');
    }
}
