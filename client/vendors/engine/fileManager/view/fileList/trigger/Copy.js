let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Copy = class extends Observer {

    onCopyKey(keyboardEvent, view) {
        if (view.hasActiveItem()) {
            view.trigger('copy');
        }
    }
}
