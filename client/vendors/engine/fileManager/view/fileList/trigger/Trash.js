let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Trash = class extends Observer {

    onTrashKey(keyboardEvent, view) {
        if (view.hasActiveItem()) {
            view.trigger(view.directory.isTrash ? 'remove' : 'trash', {
                index: view.getActiveIndex()
            });
        }
    }
}
