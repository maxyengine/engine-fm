let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Permissions = class extends Observer {

    onPermissionsKey(keyboardEvent, view) {
        if (view.hasActiveItem()) {
            view.trigger('permissions');
        }
    }
}
