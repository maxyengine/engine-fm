let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Open = class extends Observer {

    onOpenKey() {
        if (this.owner.hasActiveItem()) {
            this.owner.trigger('open');
        }
    }

    onRender() {
        this.owner.breadcrumbs.items.forEach(item => {
            item.element.addEventListener('click', () => {
                this.owner.trigger('open', {
                    id: item.id
                });
            });
        });

        this.owner.items.forEach((item) => {
            item.element.addEventListener('dblclick', () => {
                this.owner.trigger('open');
            });
        });
    }
}
