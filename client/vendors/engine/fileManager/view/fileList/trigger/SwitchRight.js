let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.SwitchRight = class extends Observer {

    onSwitchRightKey() {
        this.owner.trigger('switch');
    }
}
