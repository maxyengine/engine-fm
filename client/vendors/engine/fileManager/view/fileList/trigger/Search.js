let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Search = class extends Observer {

    onSearchKey() {
        this.owner.trigger('search');
    }
}
