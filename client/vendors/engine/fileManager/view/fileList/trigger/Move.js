let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Move = class extends Observer {

    onMoveKey(keyboardEvent, view) {
        if (view.hasActiveItem()) {
            view.trigger('move');
        }
    }
}
