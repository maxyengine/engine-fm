let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Download = class extends Observer {

    onDownloadKey() {
        this.owner.trigger('download');
    }
}
