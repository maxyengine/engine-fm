let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Upload = class extends Observer {

    onUploadKey() {
        this.owner.uploadInput.click();
    }
}
