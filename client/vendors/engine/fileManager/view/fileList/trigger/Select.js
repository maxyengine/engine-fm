let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Select = class extends Observer {

    initialize() {
        super.initialize();
        this.selectedIndexList = [];
        this.clipboard = [];
        this.indexById = {};
    }

    get view() {
        return this.owner;
    }

    get properties() {
        return {
            getActiveIndex: this.getActiveIndex,
            hasActiveItem: this.hasActiveItem,
            getActiveItem: this.getActiveItem,
            activateEntity: this.activateEntity,
            getClipboard: this.getClipboard,
            selectAll: this.selectAll,
            deselectAll: this.deselectAll,
            selectItem: this.selectItem
        };
    }

    onNextItemKey(event) {
        this.activateNextItem(event.shiftKey);
    }

    onPrevItemKey(event) {
        this.activatePrevItem(event.shiftKey);
    }

    onFirstItemKey(event) {
        this.activateFirstItem(event.shiftKey);
    }

    onLastItemKey(event) {
        this.activateLastItem(event.shiftKey);
    }

    onRender() {
        let clipboard = this.clipboard.slice(0);

        this.selectedIndexList = [];
        this.clipboard = [];
        this.indexById = {};

        this.view.items.forEach((item, index) => {
            this.indexById[item.entity.id] = index;
            item.element.addEventListener('click', () => {
                this.activateItem(index);
            });

            // restore clipboard
            if (clipboard.indexOf(item.entity.id) !== -1) {
                this.selectItem(index);
            }
        });
    }

    getActiveIndex() {
        return this.activeIndex;
    };

    getActiveItem() {
        return this.view.items[this.activeIndex];
    }

    hasActiveItem() {
        return !isNaN(this.activeIndex);
    }

    activateEntity(entity) {
        return this.activateItem(this.indexById[entity && entity.id]);
    }

    getClipboard() {
        return this.clipboard;
    }

    selectAll() {
        for (let index = 0; this.view.items[index]; index++) {
            if (this.selectedIndexList.indexOf(index) === -1) {
                this.selectItem(index);
            }
        }
    }

    deselectAll() {
        let selectedIndexList = this.selectedIndexList.slice(0);
        selectedIndexList.forEach(index => {
            this.selectItem(index);
        });
    }

    selectItem(index) {
        if (!this.view.items.length || index < 0 || index >= this.view.items.length) {
            return;
        }

        if (this.view.items[index].entity.isParent) {
            return;
        }

        let
            indexOfIndex = this.selectedIndexList.indexOf(index),
            entity = this.view.items[index].entity;
        if (indexOfIndex === -1) {
            this.selectedIndexList.push(index);
            this.clipboard.push(entity.id);

            this.view.trigger('selectItem', {
                index: index,
                entity: entity
            });
        } else {
            this.selectedIndexList.splice(indexOfIndex, 1);
            this.clipboard.splice(this.clipboard.indexOf(entity.id), 1);

            this.view.trigger('deselectItem', {
                index: index,
                entity: entity
            });
        }
    }

    activateItem(index = 0) {
        if (!this.view.items.length || index < 0 || index >= this.view.items.length) {
            return;
        }

        if (this.view.items[this.activeIndex]) {
            this.view.trigger('deactivateItem', {
                index: this.activeIndex,
                entity: this.view.items[this.activeIndex].entity
            });
        }

        this.activeIndex = index;
        this.view.trigger('activateItem', {
            index: this.activeIndex,
            entity: this.view.items[this.activeIndex].entity
        });
    }

    activatePrevItem(shiftKey) {
        shiftKey && this.selectItem(this.activeIndex);
        this.activateItem(this.activeIndex - 1);
    }

    activateNextItem(shiftKey) {
        shiftKey && this.selectItem(this.activeIndex);
        this.activateItem(this.activeIndex + 1);
    }

    activateFirstItem(shiftKey) {
        if (shiftKey) {
            let index = 0;
            while (this.activeIndex >= index) {
                this.selectItem(index);
                index++;
            }
        }
        this.activateItem(0);
    }

    activateLastItem(shiftKey) {
        if (shiftKey) {
            let index = this.view.items.length - 1;
            while (this.activeIndex <= index) {
                this.selectItem(index);
                index--;
            }
        }
        this.activateItem(this.view.items.length - 1);
    }
}
