let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.SwitchLeft = class extends Observer {

    onSwitchLeftKey() {
        this.owner.trigger('switch');
    }
}
