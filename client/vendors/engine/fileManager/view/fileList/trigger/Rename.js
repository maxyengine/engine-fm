let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.Rename = class extends Observer {

    onRenameKey(keyboardEvent, view) {
        if (view.hasActiveItem() && !view.getActiveItem().entity.isParent) {
            view.trigger('rename');
        }
    }
}
