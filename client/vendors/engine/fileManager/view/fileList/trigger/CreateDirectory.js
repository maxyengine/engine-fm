let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.trigger.CreateDirectory = class extends Observer {

    onCreateFolderKey() {
        this.owner.trigger('createDirectory');
    }
}
