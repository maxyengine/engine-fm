let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.fileList.UploadProgress;

engine.fileManager.view.fileList.UploadProgress = class extends View {

    get template() {
        return Template;
    }

    get events() {
        return {
            showDetails: {element: 'click'}
        };
    }

    set title(value) {
        this._title.nodeValue = value;
    }

    update(percent) {
        this.value = percent;
        this.line.style.width = percent + '%';
        this.percent.nodeValue = percent > 0 ? percent + '%' : '';
        this.element.style.cursor = percent ? 'pointer' : 'default';
    }

    onShowDetails() {
        this.value && this.uploadProgressDialog.show();
    }
}
