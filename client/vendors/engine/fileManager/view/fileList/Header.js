let
    View = engine.gui.component.View,
    ClassName = engine.gui.utility.ClassName,
    Template = engine.fileManager.template.fileList.Header;

engine.fileManager.view.fileList.Header = class extends View {

    get template() {
        return Template;
    }

    sortBy(field, desc) {
        ClassName.set(this.sortIcons[field], this.icon.classByKey(desc ? 'sortDesc' : 'sortAsc'));
    }
}
