let
    Object = engine.lang.type.Object;

engine.fileManager.view.fileList.observer.ResizeParent = class extends Object {

    initialize() {
        window.addEventListener('resize', () => {
            this.owner.setHeaderWidth();
        });
    }
}
