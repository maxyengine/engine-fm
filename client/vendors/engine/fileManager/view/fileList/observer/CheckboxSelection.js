let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.observer.CheckboxSelection = class extends Observer {

    onSelectItem(event, view) {
        view.items[event.index].checkbox.checked = true;

        let difference = view.directory.parentId ? 1 : 0;

        if (view.getClipboard().length === view.items.length - difference) {
            view.header.checkbox.checked = true;
        }
    }

    onDeselectItem(event, view) {
        view.items[event.index].checkbox.checked = false;

        if (view.getClipboard().length !== view.items.length) {
            view.header.checkbox.checked = false;
        }
    }

    onRender(event, view) {
        view.items.forEach((item, index) => {
            item.checkbox && item.checkbox.addEventListener('change', () => {
                view.selectItem(index);
            });
        });

        view.header.checkbox.addEventListener('change', () => {
            if (view.header.checkbox.checked) {
                view.selectAll();
            } else {
                view.deselectAll();
            }
        });
    }
}
