let
    Obj = engine.lang.utility.Object,
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.observer.ListSorting = class extends Observer {

    get field() {
        if (!this._field) {
            this._field = 'name';
        }

        return this._field;
    }

    set field(value) {
        this._field = value;
    }

    get desc() {
        if (!this._desc) {
            this._desc = false;
        }

        return this._desc;
    }

    set desc(value) {
        this._desc = value;
    }

    onBeforeRender(event, view) {
        this.sort(view);
    }

    onRender(event, view) {
        view.header.sortBy(this.field, this.desc);

        Obj.forEach(view.header.columns, (field, element) => {
            element.addEventListener('click', () => {
                this.desc = this.field === field ? !this.desc : false;
                this.field = field;

                view.render();
            });
        });
    }

    sort(view) {
        view.directory.children.sort((a, b) => {
            let
                fieldA = a[this.field],
                fieldB = b[this.field];

            if ('size' === this.field) {
                fieldA = fieldA.value;
                fieldB = fieldB.value;
            } else if ('lastModified' === this.field) {
                fieldA = fieldA.getTime();
                fieldB = fieldB.getTime();
            }

            let result = 0;
            if (a.class === 'directory' && b.class === 'directory') {
                result = fieldA > fieldB ? 1 : (fieldB > fieldA ? -1 : 0);
            } else if (a.class === 'directory') {
                return -1;
            } else if (b.class === 'directory') {
                return 1;
            } else {
                result = fieldA > fieldB ? 1 : (fieldB > fieldA ? -1 : 0);
            }

            if (this.desc) {
                result = -result;
            }

            return result;
        });
    }
}
