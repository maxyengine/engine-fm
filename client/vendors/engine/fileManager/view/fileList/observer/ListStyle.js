let
    ClassName = engine.gui.utility.ClassName,
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.observer.ListStyle = class extends Observer {

    onActivateItem(event, view) {
        ClassName.add(view.items[event.index].element, 'active');
    }

    onDeactivateItem(event, view) {
        ClassName.remove(view.items[event.index].element, 'active');
    }

    onSelectItem(event, view) {
        ClassName.add(view.items[event.index].element, 'selected');
    }

    onDeselectItem(event, view) {
        ClassName.remove(view.items[event.index].element, 'selected');
    }
}
