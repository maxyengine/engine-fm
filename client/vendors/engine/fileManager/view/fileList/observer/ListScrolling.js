let
    Observer = engine.react.Observer;

engine.fileManager.view.fileList.observer.ListScrolling = class extends Observer {

    onActivateItem(event) {
        let
            body = this.owner.body,
            element = body.items[event.index].element,
            relativeTop = element.getBoundingClientRect().top - body.element.getBoundingClientRect().top,
            screenHeight = body.element.offsetHeight - element.offsetHeight;

        if (relativeTop < 0 || relativeTop >= screenHeight) {
            body.element.scrollTop = element.offsetTop - screenHeight + 5;
        }
    }
}
