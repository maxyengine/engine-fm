let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.control.Button;

engine.fileManager.view.control.Button = class extends View {

    get template() {
        return Template;
    }

    get events() {
        return {
            click: {element: 'click'}
        }
    }

    set icon(value) {
        this._icon = value;
        this.render();
    }

    set caption(value) {
        this._caption = value;
        this.render();
    }

    set title(value) {
        this._title = value;
        this.render();
    }

    disabled(value) {
        this.element.disabled = value;
    }

    blur() {
        this.element.blur();
    }

    render() {
        this.clear();

        this._icon && this.element.appendChild(this._icon.element);
        this._caption && this.element.appendChild(document.createTextNode(this._caption));

        if (this._title) {
            this.element.title = this._title;
        }

        return this.element;
    }
}
