let
    View = engine.gui.component.View,
    Activity = engine.gui.trigger.Activity,
    Visibility = engine.gui.trigger.Visibility,
    HotKeys = engine.gui.trigger.HotKeys,
    Template = engine.fileManager.template.control.ModalHyperlinkDialog;

engine.fileManager.view.control.ModalHyperlinkDialog = class extends View {

    get template() {
        return Template;
    }

    get traits() {
        return [
            Activity,
            Visibility,
            HotKeys
        ];
    }

    get events() {
        return {
            confirm: {confirm: 'click'},
            cancel: {cancel: 'click', close: 'click'}
        };
    }

    get hotKeys() {
        return {
            confirm: 'Enter',
            cancel: 'Esc'
        }
    }

    set caption(value) {
        this._caption.nodeValue = value;
    }

    set placeholderName(value) {
        this.name.placeholder = value;
    }

    set placeholderUrl(value) {
        this.url.placeholder = value;
    }

    onShow(params) {
        this.params = params;

        this.caption = this.params.caption || '';
        this.name.value = this.params.name || '';
        this.url.value = this.params.url || '';
        this.placeholderName = this.params.placeholderName || '';
        this.placeholderUrl = this.params.placeholderUrl || '';

        this.activate();
        this.name.focus();
    }

    onConfirm() {
        this.hide();
        this.deactivate();

        if (
            this.name.value === this.params.name &&
            this.url.value === this.params.url
        ) {
            this.params.onCancel && this.params.onCancel();
        } else {
            this.params.onConfirm && this.params.onConfirm({
                name: this.name.value,
                url: this.url.value
            });
        }
    }

    onCancel() {
        this.hide();
        this.deactivate();
        this.params.onCancel && this.params.onCancel();
    }
}
