let
    View = engine.gui.component.View,
    Activity = engine.gui.trigger.Activity,
    Visibility = engine.gui.trigger.Visibility,
    HotKeys = engine.gui.trigger.HotKeys,
    Template = engine.fileManager.template.control.ModalChooserDialog,
    Checkbox = engine.fileManager.view.control.Checkbox,
    Element = engine.gui.utility.Element;

engine.fileManager.view.control.ModalChooserDialog = class extends View {

    get template() {
        return Template;
    }

    get traits() {
        return [
            Activity,
            Visibility,
            HotKeys
        ];
    }

    get events() {
        return {
            confirm: {confirm: 'click'},
            cancel: {cancel: 'click', close: 'click'}
        };
    }

    get hotKeys() {
        return {
            confirm: 'Enter',
            cancel: 'Esc'
        }
    }

    set caption(value) {
        this._caption.nodeValue = value;
    }

    set message(value) {
        this._message.nodeValue = value;
    }

    onShow(params) {
        this.params = params;

        this.caption = this.params.caption || '';
        this.message = this.params.message || '';
        this.checkboxes = [];

        Element.clear(this.form);
        this.params.fileList.forEach((name, index) => {
            let checkbox = new Checkbox({
                label: name,
                value: index,
                checked: true
            });

            this.form.appendChild(checkbox.element);
            this.checkboxes.push(checkbox);
        });

        this.activate();
    }

    onConfirm() {
        this.hide();
        this.deactivate();

        let indexes = [];
        this.checkboxes.forEach(checkbox => {
            indexes[checkbox.value] = checkbox.checked;
        });

        this.params.onConfirm && this.params.onConfirm({
            indexes: indexes
        });
    }

    onCancel() {
        this.hide();
        this.deactivate();
        this.params.onCancel && this.params.onCancel();
    }
}
