let
    View = engine.gui.component.View,
    Element = engine.gui.utility.Element,
    Template = engine.fileManager.template.control.UploadButton;

engine.fileManager.view.control.UploadButton = class extends View {

    get template() {
        return Template;
    }

    get events() {
        return {
            click: {button: 'click'},
            change: {input: 'change'}
        }
    }

    set icon(value) {
        this._icon = value;
        this.render();
    }

    set caption(value) {
        this._caption = value;
        this.render();
    }

    set title(value) {
        this._title = value;
        this.render();
    }

    disabled(value) {
        this.button.disabled = value;
    }

    blur() {
        this.button.blur();
    }

    reset() {
        this.input.value = null;
    }

    render() {
        Element.clear(this.button);

        this._icon && this.button.appendChild(this._icon.element);
        this._caption && this.button.appendChild(document.createTextNode(this._caption));

        if (this._title) {
            this.element.title = this._title;
        }

        return this.element;
    }

    onClick() {
        this.blur();
        this.input.click();
    }

    onChange(event) {
        let files = Array.prototype.slice.call(event.target.files || event.dataTransfer.files);
        this.trigger('selectFiles', {
            files: files
        });
        this.reset();
    }
}
