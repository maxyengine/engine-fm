let
    View = engine.gui.component.View,
    Element = engine.gui.utility.Element,
    ActivityTrigger = engine.gui.trigger.Activity,
    Button = engine.fileManager.view.control.Button;

engine.fileManager.view.control.EmbeddedInputDialog = class extends View {

    initialize() {


        this.tagName = 'span';
        this.input = document.createElement('input');
        this.confirmButton = new Button({icon: this.icon.create('ok')});
        this.cancelButton = new Button({icon: this.icon.create('cancel')});
        this.element.appendChild(this.input);

        // don't display buttons:
        //this.element.appendChild(this.confirmButton.element());
        //this.element.appendChild(this.cancelButton.element());

        this.subscribe();
    }

    get traits() {
        return [
            ActivityTrigger
        ];
    }

    subscribe() {
        this.input.addEventListener('keyup', event => {
            event.keyCode === 13 && this.confirm();
            event.keyCode === 27 && this.cancel();
        });
        this.input.addEventListener('keydown', event => {
            event.keyCode === 9 && this.cancel();
        });
        this.confirmButton.on('click', () => {
            this.confirm();
        });
        this.cancelButton.on('click', () => {
            this.cancel();
        });

        this.element.addEventListener('mousedown', () => {
            this.innerAction = true;
        });
        this.input.addEventListener('blur', () => {
            if (!this.innerAction) {
                this.restored || this.cancel();
            }
            this.innerAction = false;
        });
    }

    appendTo(params) {
        this.restored = false;
        this.innerAction = false;
        this.params = params;

        this.keepOriginal();
        this.replaceOriginal();
    }

    confirm() {
        if (this.input.value === this.params.value) {
            this.cancel();
        } else {
            this.restoreOriginal();
            this.params.onConfirm && this.params.onConfirm({
                value: this.input.value
            });
        }
    }

    cancel() {
        this.restoreOriginal();
        this.params.onCancel && this.params.onCancel();
    }

    keepOriginal() {
        this.paramsContent = [];
        for (let i = 0; this.params.element.childNodes[i]; i++) {
            this.paramsContent.push(this.params.element.childNodes[i]);
        }
    }

    replaceOriginal() {
        Element.clear(this.params.element);
        this.params.element.appendChild(this.element);
        this.input.value = this.params.value;
        this.input.focus();
        this.activate();
    }

    restoreOriginal() {
        this.restored = true;
        Element.clear(this.params.element);
        this.paramsContent.forEach(element => {
            this.params.element.appendChild(element);
        });
        this.deactivate();
    }
}
