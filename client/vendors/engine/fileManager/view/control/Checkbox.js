let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.control.Checkbox;

engine.fileManager.view.control.Checkbox = class extends View {

    get template() {
        return Template;
    }

    set label(value) {
        this._label.nodeValue = value;
    }

    set value(value) {
        this.input.value = value;
    }

    get value() {
        return this.input.value;
    }

    set checked(value) {
        this.input.checked = value;
    }

    get checked() {
        return this.input.checked;
    }
}
