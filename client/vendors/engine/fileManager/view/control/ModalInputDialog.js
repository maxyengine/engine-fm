let
    View = engine.gui.component.View,
    Activity = engine.gui.trigger.Activity,
    Visibility = engine.gui.trigger.Visibility,
    HotKeys = engine.gui.trigger.HotKeys,
    Template = engine.fileManager.template.control.ModalInputDialog;

engine.fileManager.view.control.ModalInputDialog = class extends View {

    get template() {
        return Template;
    }

    get traits() {
        return [
            Activity,
            Visibility,
            HotKeys
        ];
    }

    get events() {
        return {
            confirm: {confirm: 'click'},
            cancel: {cancel: 'click', close: 'click'}
        };
    }

    get hotKeys() {
        return {
            confirm: 'Enter',
            cancel: 'Esc'
        }
    }

    set caption(value) {
        this._caption.nodeValue = value;
    }

    set label(value) {
        this._label.nodeValue = value;
    }

    set value(value) {
        this.input.value = value;
    }

    set placeholder(value) {
        this.input.placeholder = value;
    }

    onShow(params) {
        this.params = params;

        this.caption = this.params.caption || '';
        this.label = this.params.label || '';
        this.value = this.params.value || '';
        this.placeholder = this.params.placeholder || '';

        this.activate();
        this.input.focus();
    }

    onConfirm() {
        this.hide();
        this.deactivate();

        if (this.input.value === this.params.value) {
            this.params.onCancel && this.params.onCancel();
        } else {
            this.params.onConfirm && this.params.onConfirm({
                value: this.input.value
            });
        }
    }

    onCancel() {
        this.hide();
        this.deactivate();
        this.params.onCancel && this.params.onCancel();
    }
}
