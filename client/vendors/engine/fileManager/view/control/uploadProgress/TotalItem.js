let
    Item = engine.fileManager.view.control.uploadProgress.Item,
    Template = engine.fileManager.template.control.uploadProgress.TotalItem;

engine.fileManager.view.control.uploadProgress.TotalItem = class extends Item {

    get template() {
        return Template;
    }
}
