let
    View = engine.gui.component.View,
    ClassName = engine.gui.utility.ClassName,
    Template = engine.fileManager.template.control.uploadProgress.Item;

engine.fileManager.view.control.uploadProgress.Item = class extends View {

    get template() {
        return Template;
    }

    get events() {
        return {
            abort: {marker: 'click'}
        }
    }

    set title(value) {
        this._title.nodeValue = value;
    }

    update(percent) {
        this.line.style.width = percent + '%';
        this.percent.nodeValue = percent + '%';

        if (100 === percent) {
            ClassName.set(this.marker, 'engineFM__progress-done done');
        }
    }
}
