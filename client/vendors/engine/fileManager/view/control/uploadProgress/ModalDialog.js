let
    View = engine.gui.component.View,
    Activity = engine.gui.trigger.Activity,
    Visibility = engine.gui.trigger.Visibility,
    HotKeys = engine.gui.trigger.HotKeys,
    Item = engine.fileManager.view.control.uploadProgress.Item,
    TotalItem = engine.fileManager.view.control.uploadProgress.TotalItem,
    Template = engine.fileManager.template.control.uploadProgress.ModalDialog;

engine.fileManager.view.control.uploadProgress.ModalDialog = class extends View {

    get template() {
        return Template;
    }

    get traits() {
        return [
            Activity,
            Visibility,
            HotKeys
        ];
    }

    get events() {
        return {
            toBackground: {toBackground: 'click', close: 'click'}
        };
    }

    get hotKeys() {
        return {
            toBackground: 'Enter'
        }
    }

    initialize() {
        this.total = new TotalItem({
            title: 'Total',
            icon: this.icon
        });
        this.body.appendChild(this.total.element);
    }

    set caption(value) {
        if (null !== value) {
            this._caption.nodeValue = value;
        }
    }

    show(params = {}) {
        this.params = params;
        this.caption = this.params.caption || null;

        this.activate();
        super.show();
    }

    appendItem(params) {
        params.icon = this.icon;
        let item = new Item(params);
        this.body.appendChild(item.element);

        return item;
    }

    removeItem(item) {
        this.body.removeChild(item.element);
    }

    hide() {
        super.hide();
        this.deactivate();
    }

    onToBackground() {
        this.hide();
    }
}
