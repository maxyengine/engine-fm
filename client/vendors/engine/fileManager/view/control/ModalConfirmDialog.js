let
    View = engine.gui.component.View,
    Activity = engine.gui.trigger.Activity,
    Visibility = engine.gui.trigger.Visibility,
    HotKeys = engine.gui.trigger.HotKeys,
    Template = engine.fileManager.template.control.ModalConfirmDialog;

engine.fileManager.view.control.ModalConfirmDialog = class extends View {

    get template() {
        return Template;
    }

    get traits() {
        return [
            Activity,
            Visibility,
            HotKeys
        ];
    }

    get events() {
        return {
            confirm: {confirm: 'click'},
            cancel: {cancel: 'click', close: 'click'}
        };
    }

    get hotKeys() {
        return {
            confirm: 'Enter',
            cancel: 'Esc'
        }
    }

    set caption(value) {
        this._caption.nodeValue = value;
    }

    set message(value) {
        this._message.nodeValue = value;
    }

    onShow(params) {
        this.params = params;

        this.caption = this.params.caption || '';
        this.message = this.params.message || '';

        this.activate();
    }

    onConfirm() {
        this.hide();
        this.deactivate();
        this.params.onConfirm && this.params.onConfirm();
    }

    onCancel() {
        this.hide();
        this.deactivate();
        this.params.onCancel && this.params.onCancel();
    }
}
