let
    View = engine.gui.component.View,
    Visibility = engine.gui.trigger.Visibility,
    Template = engine.fileManager.template.control.RequestProgress;

engine.fileManager.view.control.RequestProgress = class extends View {

    get template() {
        return Template;
    }

    get traits() {
        return [
            Visibility
        ];
    }
}
