let
    View = engine.gui.component.View,
    Template = engine.fileManager.template.icon.Layout;

engine.fileManager.view.icon.Layout = class extends View {

    get template() {
        return Template;
    }
}
