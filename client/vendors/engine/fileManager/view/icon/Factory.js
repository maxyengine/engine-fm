let
    Object = engine.lang.type.Object,
    ClassName = engine.gui.utility.ClassName,
    Icon = engine.fileManager.view.icon.Layout;

engine.fileManager.view.icon.Factory = class extends Object {

    create(key) {
        let icon = new Icon();
        ClassName.add(icon.element, key ? this.classByKey(key) : '');

        return icon;
    }

    classByKey(key) {
        return this.iconClasses[key] || this.defaultIconClass;
    }
}
