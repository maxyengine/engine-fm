let
    Obj = engine.lang.utility.Object,
    Size = engine.fileManager.value.Size,
    Arr = engine.lang.utility.Array,
    Type = engine.lang.utility.Type;

engine.fileManager.setting.Config = class {

    constructor(raw) {
        Obj.merge(this, raw);
    }

    set uploadMaxFileSize(value) {
        this._uploadMaxFileSize = new Size(value);
    }

    get uploadMaxFileSize() {
        return this._uploadMaxFileSize;
    }

    isMimeTypeAllowed(type) {
        if (Type.isArray(this.allowMimeTypes)) {
            return Arr.contains(this.allowMimeTypes, type);
        }

        if (Type.isArray(this.denyMimeTypes)) {
            return !Arr.contains(this.denyMimeTypes, type);
        }

        return true;
    }

    isFileSizeAllowed(size) {
        return this.uploadMaxFileSize.value >= size;
    }

    isNumberOfUploadFilesAllowed(number) {
        return !this.maxNumberOfUploadFiles || this.maxNumberOfUploadFiles >= number;
    }
}
