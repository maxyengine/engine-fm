let
    Component = engine.react.Component,
    User = engine.web.component.User,
    Client = engine.fileManager.component.Client,
    WebClient = engine.web.component.Client,
    Layout = engine.fileManager.view.Layout,
    FileModel = engine.fileManager.model.File,
    FileListController = engine.fileManager.controller.FileList,
    ErrorManager = engine.fileManager.component.ErrorManager,
    Config = engine.fileManager.setting.Config;

let self = engine.fileManager.Application = class extends Component {

    initialize() {
        this.wrapper = this.wrapper || document.body;
        this.csrfTokenName = this.csrfTokenName || 'CSRF-Token';
        this.configUrl = this.configUrl + this.csrfTokenName + '=' + this.csrfToken;

        this.loadConfig({
            onSuccess: event => {
                this.rawConfig = JSON.parse(event.response.body);
                this.run();
            }
        });
    }

    get instanceId() {
        if (!this._instanceId) {
            if (!self.instanceCounter) {
                self.instanceCounter = 0;
            }
            this._instanceId = 'engine.fileManager.Application-' + self.instanceCounter++;
        }

        return this._instanceId;
    }

    get config() {
        if (!this._config) {
            this._config = new Config(this.rawConfig);
        }

        return this._config;
    }

    get layout() {
        if (!this._layout) {
            this._layout = new Layout({
                config: this.config,
                wrapper: this.wrapper
            });
        }

        return this._layout;
    }

    get errorManager() {
        if (!this._errorManager) {
            this._errorManager = new ErrorManager({
                container: this.layout.element
            });
        }

        return this._errorManager;
    }

    get client() {
        if (!this._client) {
            this._client = new Client({
                serverUrl: this.config.serverUrl,
                requestProgress: this.layout.requestProgress,
                errorManager: this.errorManager,
                csrfTokenName: this.csrfTokenName,
                csrfToken: this.csrfToken
            });
        }

        return this._client;
    }

    get user() {
        if (!this._user) {
            this._user = new User({
                applicationId: this.instanceId,
                expires: 24 * 3600000
            });
        }

        return this._user;
    }

    get fileModel() {
        if (!this._fileModel) {
            this._fileModel = new FileModel({
                client: this.client
            });
        }

        return this._fileModel;
    }

    get rightController() {
        if (!this._rightController) {
            this._rightController = new FileListController({
                view: this.layout.rightList,
                layout: this.layout,
                directory: this.rightPath ? {id: this.rightPath} : this.user.read('rightDirectory', {id: '/'}),
                fileModel: this.fileModel,
                errorManager: this.errorManager,
                config: this.config,
                user: this.user
            });
        }

        return this._rightController;
    }

    get leftController() {
        if (!this._leftController) {
            this._leftController = new FileListController({
                view: this.layout.leftList,
                layout: this.layout,
                directory: this.leftPath ? {id: this.leftPath} : this.user.read('leftDirectory', {id: '/'}),
                fileModel: this.fileModel,
                errorManager: this.errorManager,
                config: this.config,
                user: this.user
            });
        }

        return this._leftController;
    }

    run() {
        this.leftController.setDouble(this.rightController);
        this.leftController.view.activate();
        this.layout.render();

        this.trigger('ready');
    }

    loadConfig(...observers) {
        (new WebClient()).exchange({
            method: 'GET',
            url: this.configUrl
        }, ...observers);
    }
}
