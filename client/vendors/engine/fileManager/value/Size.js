engine.fileManager.value.Size = class {

    constructor(value) {
        this._value = value;
    }

    get value() {
        return this._value;
    }

    toHumanString() {
        let bytes = this.value,
            thresh = 1024;

        if (Math.abs(bytes) < thresh) {
            return bytes + ' B';
        }

        let units = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            u = -1;
        do {
            bytes /= thresh;
            ++u;
        } while (Math.abs(bytes) >= thresh && u < units.length - 1);

        return bytes.toFixed(1) + ' ' + units[u];
    }
}
