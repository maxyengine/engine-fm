let
    Object = engine.lang.type.Object,
    Hyperlink = engine.fileManager.entity.Hyperlink;

engine.fileManager.controller.fileList.action.Rename = class extends Object {

    execute() {
        let entity = this.getActiveEntity(),
            value = entity instanceof Hyperlink ? entity.baseName : entity.name;

        this.layout.inputDialog.show({
            icon: 'edit',
            caption: 'Rename',
            label: 'New Name',
            placeholder: value,
            value: value,
            onCancel: () => {
                this.view.activate();
            },
            onConfirm: event => {
                this.fileModel.rename({
                    id: this.getActiveEntity().id,
                    name: entity instanceof Hyperlink ? event.value + '.' + entity.extension : event.value
                }, this);
            }
        });
    }

    onSuccess(event) {
        if (
            this.getDouble() &&
            this.getDouble().activeEntity &&
            this.getDouble().activeEntity.id === this.getActiveEntity().id
        ) {
            this.getDouble().setActiveEntity(event.entity);
        }
        this.setActiveEntity(event.entity);

        this.fileModel.read({
            id: this.getDirectory().id
        }, {
            onSuccess: event => {
                this.render(event);
                this.view.activate();
                this.getDouble() && this.getDouble().render(event);
            }
        });
    }
}
