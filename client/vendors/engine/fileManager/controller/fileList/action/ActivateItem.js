let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.ActivateItem = class extends Object {

    execute(event) {
        this.setActiveEntity(event.entity);
        this.affectToolbar();
    }
}
