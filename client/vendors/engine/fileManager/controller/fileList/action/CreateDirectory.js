let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.CreateDirectory = class extends Object {

    execute() {
        this.layout.inputDialog.show({
            icon: 'dir',
            caption: 'Create Folder',
            label: 'Folder Name',
            placeholder: 'NewFolder',
            value: '',
            onCancel: () => {
                this.view.activate();
            },
            onConfirm: event => {
                this.fileModel.createDirectory({
                    name: event.value,
                    parentId: this.getDirectory().id
                }, this);
            }
        });
    }

    onSuccess(event) {
        this.setActiveEntity(event.entity);

        this.fileModel.read({
            id: this.getDirectory().id
        }, {
            onSuccess: event => {
                this.render(event);
                this.view.activate();
                this.getDouble() && this.getDouble().render(event);
            }
        });
    }
}
