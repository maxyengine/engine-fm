let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.Initialize = class extends Object {

    execute() {
        this.fileModel.read({
            id: this.getDirectory().id
        }, this);
    }

    onSuccess(event) {
        this.keepLastDirectory(event.entity);
        this.render(event);

        this.controller.trigger('initialize');
    }

    onStatusCode404() {
        this.setDirectory({id: '/'});
        this.execute();
    }
}
