let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.SetPermissions = class extends Object {

    execute() {
        let value = this.getActiveEntity().permissions;

        this.layout.inputDialog.show({
            icon: 'permissions',
            caption: 'Permissions',
            label: 'New Permissions',
            placeholder: value,
            value: value,
            onCancel: () => {
                this.view.activate();
            },
            onConfirm: event => {
                this.fileModel.setPermissions({
                    id: this.getActiveEntity().id,
                    permissions: event.value
                }, this);
            }
        });
    }

    onSuccess(event) {
        if (
            this.getDouble() &&
            this.getDouble().activeEntity &&
            this.getDouble().activeEntity.id === this.getActiveEntity().id
        ) {
            this.getDouble().activeEntity = event.entity;
        }
        this.setActiveEntity(event.entity);

        this.fileModel.read({
            id: this.getDirectory().id
        }, {
            onSuccess: event => {
                this.render(event);
                this.view.activate();
                this.getDouble() && this.getDouble().render(event);
            }
        });
    }
}
