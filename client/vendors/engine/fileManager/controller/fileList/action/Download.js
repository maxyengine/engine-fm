let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.Download = class extends Object {

    execute() {
        let clipboard = this.view.getClipboard();
        if ((!this.getActiveEntity() || this.getActiveEntity().isParent) && !clipboard.length) {
            return;
        }

        this.fileModel.download({
            id: clipboard.length ? clipboard : this.getActiveEntity().id
        });
    }
}
