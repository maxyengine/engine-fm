let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.OpenTrash = class extends Object {

    execute() {
        this.fileModel.read({
            id: '/$trash'
        }, this);
    }

    onSuccess(event) {
        this.render(event);
        this.view.activate();
    }
}
