let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.CreateHyperlink = class extends Object {

    execute() {
        this.layout.hyperlinkDialog.show({
            caption: 'Create Hyperlink',
            name: '',
            url: '',
            placeholderName: 'MyFavoriteSite',
            placeholderUrl: 'http://example.com',
            onCancel: () => {
                this.view.activate();
            },
            onConfirm: event => {
                this.fileModel.createHyperlink({
                    parentId: this.getDirectory().id,
                    name: event.name,
                    url: event.url
                }, this);
            }
        });
    }

    onSuccess(event) {
        this.setActiveEntity(event.entity);

        this.fileModel.read({
            id: this.getDirectory().id
        }, {
            onSuccess: event => {
                this.render(event);
                this.view.activate();
                this.getDouble() && this.getDouble().render(event);
            }
        });
    }
}
