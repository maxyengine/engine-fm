let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.Search = class extends Object {

    execute() {
        this.layout.inputDialog.show({
            icon: 'search',
            caption: 'Search',
            label: 'Keyword',
            value: '',
            onCancel: () => {
                this.view.activate();
            },
            onConfirm: event => {
                this.fileModel.read({
                    id: this.getDirectory().id,
                    pattern: event.value
                }, this);
            }
        });
    }

    onSuccess(event) {
        this.render(event);
        this.view.activate();
    }
}
