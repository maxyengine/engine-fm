let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.Move = class extends Object {

    execute() {
        let clipboard = this.view.getClipboard();
        if ((!this.getActiveEntity() || this.getActiveEntity().isParent) && !clipboard.length) {
            return;
        }

        if (this.getDouble() && this.getDirectory().id !== this.getDouble().getDirectory().id) {
            this.fileModel.move({
                id: clipboard.length ? clipboard : this.getActiveEntity().id,
                parentId: this.getDouble().getDirectory().id
            }, this);
        }
    }

    onSuccess(event) {
        for (let i = 0; event.collection[i]; i++) {
            if (this.getActiveEntity().id === event.collection[i].id) {
                this.setActiveEntity(null);
                break;
            }
        }

        this.fileModel.read({
            id: this.getDirectory().id
        }, {
            onSuccess: event => {
                this.render(event);
                this.view.activate();
            }
        });

        this.getDouble().fileModel.read({
            id: this.getDouble().getDirectory().id
        }, {
            onSuccess: event => {
                this.view.deselectAll();
                this.view.activate();
                this.getDouble().render(event);
            }
        });
    }
}
