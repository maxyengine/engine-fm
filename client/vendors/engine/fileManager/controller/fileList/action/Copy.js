let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.Copy = class extends Object {

    execute() {
        let clipboard = this.view.getClipboard();
        if ((!this.getActiveEntity() || this.getActiveEntity().isParent) && !clipboard.length) {
            return;
        }

        if (this.getDouble()) {
            this.fileModel.copy({
                id: clipboard.length ? clipboard : this.getActiveEntity().id,
                parentId: this.getDouble().getDirectory().id
            }, this);
        }
    }

    onSuccess() {
        this.getDouble().fileModel.read({
            id: this.getDouble().getDirectory().id
        }, {
            onSuccess: event => {
                this.view.deselectAll();
                this.view.activate();
                this.getDouble().render(event);
            }
        });
    }
}
