let
    Object = engine.lang.type.Object,
    Str = engine.lang.utility.String;

let self = engine.fileManager.controller.fileList.action.Upload = class extends Object {

    execute(event) {
        this.view.activate();
        let files = event.files;

        if (!this.getDirectory().hasChildren()) {
            this.upload(files);
            return;
        }

        let matches = [];
        files.forEach((file, index) => {
            if (this.getDirectory().containsName(files[index].name)) {
                matches[index] = files[index].name;
            }
        });

        if (!matches.length) {
            this.upload(files);
            return;
        }

        this.layout.chooserDialog.show({
            caption: 'Confirm Dialog',
            message: 'The following files are already exist. Please select files to reloading.',
            fileList: matches,
            onCancel: () => {
                this.view.activate();
            },
            onConfirm: event => {
                this.view.activate();
                let counter = 0;
                event.indexes.forEach((isSelected, index) => {
                    if (!isSelected) {
                        files.splice(index - counter, 1);
                        counter++;
                    }
                });
                this.upload(files);
            }
        });
    }

    upload(files) {
        this.validateFiles(files);
        let
            directoryId = this.getDirectory().id,
            shortProgressView = this.view.uploadProgress,
            progressDialog = this.layout.uploadProgressDialog,
            total = 0,
            loaded = 0;

        progressDialog.show({
            caption: 'Upload Files'
        });

        files.forEach(file => {
            let
                form = new FormData(),
                itemView = null,
                removeRequest = event => {
                    let request = event.request;

                    total -= request.progress.total;
                    loaded -= request.progress.loaded;

                    if (total > 0) {
                        progressDialog.total.update(self.align(100 * loaded / total));
                        shortProgressView.update(self.align(100 * loaded / total));
                    } else {
                        progressDialog.total.update(0);
                        shortProgressView.update(0);
                        progressDialog.hide();
                        this.view.activate();
                    }
                    progressDialog.removeItem(itemView);

                    if (directoryId === this.getDirectory().id) {
                        this.fileModel.read({
                            id: directoryId
                        }, {
                            onSuccess: event => {
                                this.render(event);
                                if (this.getDouble() && directoryId === this.getDouble().getDirectory().id) {
                                    this.getDouble().render(event);
                                }
                            }
                        });
                    }
                };

            form.append('file', file);
            form.append('parentId', directoryId);

            this.fileModel.upload(form, {
                onOpen: event => {
                    let request = event.request;

                    itemView = progressDialog.appendItem({
                        title: file.name,
                        onAbort: () => {
                            request.abort();
                        }
                    });
                },
                onProgress: event => {
                    let request = event.request;

                    if (!request.prevProgress) {
                        total += request.progress.total;
                        loaded += request.progress.loaded;
                    } else {
                        loaded += request.progress.loaded - request.prevProgress.loaded;
                    }
                    progressDialog.total.update(self.align(100 * loaded / total));
                    shortProgressView.update(self.align(100 * loaded / total));
                    itemView.update(self.align(100 * request.progress.loaded / request.progress.total));
                },
                onSuccess: removeRequest,
                onAbort: removeRequest
            });
        });
    }

    validateFiles(files) {
        if (!this.config.isNumberOfUploadFilesAllowed(files.length)) {
            throw Str.format('The files number exceeds the allowed number {0}.', this.config.maxNumberOfUploadFiles);
        }

        files.forEach(file => {
            if (!this.config.isMimeTypeAllowed(file.type)) {
                throw Str.format('The file \'{0}\' has unsupported type \'{1}\'.', file.name, file.type);
            }
            if (!this.config.isFileSizeAllowed(file.size)) {
                throw Str.format('The file \'{0}\' exceeds the allowed size {1}.', file.name, this.config.uploadMaxFileSize.toHumanString());
            }
        })
    }

    static align(value) {
        return Math.ceil((value) * 100) / 100;
    }
}
