let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.Remove = class extends Object {

    execute() {
        let clipboard = this.view.getClipboard();
        if ((!this.getActiveEntity() || this.getActiveEntity().isParent) && !clipboard.length) {
            return;
        }

        this.layout.confirmDialog.show({
            caption: 'Confirm Dialog',
            message: 'Are you sure to delete selected items permanently?',
            onCancel: () => {
                this.view.activate();
            },
            onConfirm: () => {
                this.fileModel.remove({
                    id: clipboard.length ? clipboard : this.getActiveEntity().id
                }, this);
            }
        });
    }

    onSuccess(event) {
        let entity = event.collection[0];
        if (this.getDouble() && this.getDouble().getDirectory().inBreadcrumbs(entity.id)) {
            this.getDouble().setDirectory({
                id: entity.parentId
            });
            this.getDouble().keepLastDirectory(this.getDouble().getDirectory());
        }

        for (let i = 0; event.collection[i]; i++) {
            if (this.getActiveEntity().id === event.collection[i].id) {
                this.setActiveEntity(null);
                break;
            }
        }

        this.fileModel.read({
            id: this.getDirectory().id
        }, {
            onSuccess: event => {
                this.render(event);
                this.view.activate();
                this.getDouble() && this.getDouble().render(event);
            }
        });
    }
}
