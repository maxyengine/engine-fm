let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.SwitchList = class extends Object {

    execute() {
        if (this.getDouble()) {
            this.getDouble().view.activate();
            this.getDouble().view.activateEntity(this.getDouble().activeEntity);
        }
    }
}
