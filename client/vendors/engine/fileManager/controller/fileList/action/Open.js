let
    Object = engine.lang.type.Object;

engine.fileManager.controller.fileList.action.Open = class extends Object {

    execute(event) {
        if (event && event.id) {
            this.fileModel.read({
                id: event.id
            }, this);

            return;
        }

        switch (this.getActiveEntity().class) {
            case 'file':
                this.fileModel.open({
                    id: this.getActiveEntity().id
                });
                return;
            case 'hyperlink':
                this.fileModel.openHyperlink({
                    url: this.getActiveEntity().url
                });
                return;
            default:
                this.fileModel.read({
                    id: this.getActiveEntity().id
                }, this);
        }
    }

    onSuccess(event) {
        this.selectIfParent(event.entity);
        this.keepLastDirectory(event.entity);
        this.render(event);
        this.view.activate();
    }

    selectIfParent(entity) {
        if (entity.id === this.getDirectory().parentId) {
            this.setActiveEntity(this.getDirectory());
        }
    }
}
