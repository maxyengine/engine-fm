let
    Observer = engine.react.Observer;

engine.fileManager.controller.fileList.observer.SelfInteraction = class extends Observer {

    onRender(event, controller) {
        if (controller.isActive || controller.getDirectory().id === event.entity.id) {
            controller.setDirectory(event.entity);
        }
    }
}
