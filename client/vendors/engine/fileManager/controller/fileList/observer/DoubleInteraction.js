let
    Observer = engine.react.Observer;

engine.fileManager.controller.fileList.observer.DoubleInteraction = class extends Observer {

    onActivate(event, controller) {
        controller.getDouble() && controller.getDouble().deactivate();
    }
}
