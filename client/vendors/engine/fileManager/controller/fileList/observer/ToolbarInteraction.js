let
    Observer = engine.react.Observer;

engine.fileManager.controller.fileList.observer.ToolbarInteraction = class extends Observer {

    get properties() {
        return {
            affectToolbar: this.affectToolbar
        };
    }

    affectToolbar() {
        if (this.owner.getActiveEntity()) {
            let
                isParent = this.owner.getActiveEntity().isParent,
                emptyClipboard = !this.owner.view.getClipboard().length,
                toolbar = this.owner.toolbar;

            toolbar.trash.disabled(isParent && emptyClipboard || this.owner.getDirectory().isTrash);
            toolbar.remove.disabled(isParent && emptyClipboard);
            toolbar.copy.disabled(isParent && emptyClipboard);
            toolbar.move.disabled(isParent && emptyClipboard);
            toolbar.download.disabled(isParent && emptyClipboard);
            toolbar.rename.disabled(isParent);
            toolbar.permissions.disabled(isParent);
        }
    }

    onActivate(event, controller) {
        controller.affectToolbar();
    }

    onRender(event, controller) {
        if (controller.isActive) {
            let
                hasChildren = event.entity.children && event.entity.children.length,
                toolbar = controller.toolbar;

            toolbar.open.disabled(!hasChildren && !event.entity.parentId);
            toolbar.trash.disabled(!hasChildren || event.entity.isTrash);
            toolbar.remove.disabled(!hasChildren);
            toolbar.copy.disabled(!hasChildren);
            toolbar.move.disabled(!hasChildren);
            toolbar.rename.disabled(!hasChildren);
            toolbar.permissions.disabled(!hasChildren);
            toolbar.download.disabled(!hasChildren);

        }
    }
}
