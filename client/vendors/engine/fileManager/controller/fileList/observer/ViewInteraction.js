let
    ClassName = engine.gui.utility.ClassName,
    Observer = engine.react.Observer;

engine.fileManager.controller.fileList.observer.ViewInteraction = class extends Observer {

    onActivate(event, controller) {
        ClassName.add(controller.view.element, 'active');
    }

    onDeactivate(event, controller) {
        ClassName.remove(controller.view.element, 'active');
    }

    onRender(event, controller) {
        if (controller.isActive || controller.getDirectory().id === event.entity.id) {
            controller.view.render(event.entity);
            controller.view.activateEntity(controller.getActiveEntity());
        }
    }
}
