let
    WebController = engine.web.component.Controller,
    SelfInteraction = engine.fileManager.controller.fileList.observer.SelfInteraction,
    DoubleInteraction = engine.fileManager.controller.fileList.observer.DoubleInteraction,
    ToolbarInteraction = engine.fileManager.controller.fileList.observer.ToolbarInteraction,
    ViewInteraction = engine.fileManager.controller.fileList.observer.ViewInteraction,
    Initialize = engine.fileManager.controller.fileList.action.Initialize,
    Open = engine.fileManager.controller.fileList.action.Open,
    OpenTrash = engine.fileManager.controller.fileList.action.OpenTrash,
    Search = engine.fileManager.controller.fileList.action.Search,
    Download = engine.fileManager.controller.fileList.action.Download,
    Upload = engine.fileManager.controller.fileList.action.Upload,
    CreateDirectory = engine.fileManager.controller.fileList.action.CreateDirectory,
    CreateHyperlink = engine.fileManager.controller.fileList.action.CreateHyperlink,
    Rename = engine.fileManager.controller.fileList.action.Rename,
    SetPermissions = engine.fileManager.controller.fileList.action.SetPermissions,
    Copy = engine.fileManager.controller.fileList.action.Copy,
    Move = engine.fileManager.controller.fileList.action.Move,
    Trash = engine.fileManager.controller.fileList.action.Trash,
    Remove = engine.fileManager.controller.fileList.action.Remove,
    ActivateItem = engine.fileManager.controller.fileList.action.ActivateItem,
    SwitchList = engine.fileManager.controller.fileList.action.SwitchList;

engine.fileManager.controller.FileList = class extends WebController {

    constructor(...args) {
        super(...args);
        this.trigger('ready');
    }

    get properties() {
        return {
            controller: this,
            user: this.user,
            config: this.config,
            fileModel: this.fileModel,
            view: this.view,
            layout: this.layout,
            getActiveEntity: this.getActiveEntity,
            setActiveEntity: this.setActiveEntity,
            getDirectory: this.getDirectory,
            setDirectory: this.setDirectory,
            getDouble: this.getDouble,
            affectToolbar: this.affectToolbar,
            render: this.render,
            keepLastDirectory: this.keepLastDirectory
        };
    }

    get traits() {
        return [
            SelfInteraction,
            DoubleInteraction,
            ToolbarInteraction,
            ViewInteraction
        ];
    }

    get events() {
        return {
            activate: {view: 'activate'},
            open: {view: 'open', toolbar: 'open'},
            openTrash: {view: 'openTrash', toolbar: 'openTrash'},
            search: {view: 'search', toolbar: 'search'},
            download: {view: 'download', toolbar: 'download'},
            upload: {view: 'upload', toolbar: 'upload'},
            createDirectory: {view: 'createDirectory', toolbar: 'createDirectory'},
            createHyperlink: {view: 'createHyperlink', toolbar: 'createHyperlink'},
            rename: {view: 'rename', toolbar: 'rename'},
            setPermissions: {view: 'permissions', toolbar: 'permissions'},
            copy: {view: 'copy', toolbar: 'copy'},
            move: {view: 'move', toolbar: 'move'},
            trash: {view: 'trash', toolbar: 'trash'},
            remove: {view: 'remove', toolbar: 'remove'},
            activateItem: {view: 'activateItem'},
            switchList: {view: 'switch'}
        };
    }

    get actions() {
        return {
            ready: Initialize,
            open: Open,
            openTrash: OpenTrash,
            search: Search,
            download: Download,
            upload: Upload,
            createDirectory: CreateDirectory,
            createHyperlink: CreateHyperlink,
            rename: Rename,
            setPermissions: SetPermissions,
            copy: Copy,
            move: Move,
            trash: Trash,
            remove: Remove,
            activateItem: ActivateItem,
            switchList: SwitchList
        };
    }

    isActionAllowed(eventName) {
        return this.isActive || 'ready' === eventName;
    }

    get toolbar() {
        return this.layout.toolbar;
    }

    getActiveEntity() {
        return this.activeEntity;
    }

    setActiveEntity(value) {
        this.activeEntity = value;
    }

    getDirectory() {
        return this.directory;
    }

    setDirectory(value) {
        this.directory = value;
    }

    getDouble() {
        return this._double;
    }

    setDouble(instance) {
        this._double = instance;
        instance._double = this;
    }

    keepLastDirectory(entity) {
        if (this.layout.leftList === this.view) {
            this.user.write('leftDirectory', {id: entity.id});
        } else {
            this.user.write('rightDirectory', {id: entity.id});
        }
    }

    handleActionError(throwable) {
        this.errorManager.handleError(throwable);
    }

    render(event) {
        this.trigger('render', event);
    }

    activate() {
        this.trigger('activate');
    }

    deactivate() {
        this.trigger('deactivate');
    }

    onActivate() {
        if (this.isActive) {
            return;
        }
        this.isActive = true;
    }

    onDeactivate() {
        if (!this.isActive) {
            return;
        }
        this.isActive = false;
    }
}
