let
    Object = engine.lang.type.Object,
    Obj = engine.lang.utility.Object,
    Observable = engine.react.Observable,
    Str = engine.lang.utility.String,
    Type = engine.lang.utility.Type;

let self = engine.react.Component = class extends Object {

    _constructor(...args) {
        super._constructor(...args);
        this.mapEvents(this.events || {});
        this.on(this);
    }

    get eventResource() {
        if (!this._observable) {
            this._observable = new Observable();
        }

        return this._observable;
    }

    on(...args) {
        this.eventResource.addObserver(...args);

        return this;
    }

    trigger(eventName, event, context) {
        this.eventResource.trigger(eventName, event, context || this);
    }

    mapEvent(property, fromEventName, toEventName) {
        if (property instanceof self) {
            property.on(fromEventName, event => {
                this.trigger(toEventName, event, this);
            });
        } else {
            property.addEventListener(fromEventName, event => {
                this.trigger(toEventName, event, this);
            });
        }
    }

    mapEvents(events) {
        Obj.forEach(events, (toEventName, properties) => {
            Obj.forEach(properties, (property, fromEventName) => {
                if (Type.isArray(this[property])) {
                    this[property].forEach(item => {
                        this.mapEvent(item, fromEventName, toEventName);
                    });
                } else {
                    this.mapEvent(this[property], fromEventName, toEventName);
                }
            });
        });
    }

    set(property, value, context) {
        if (property.length > 2 && property.substr(0, 2) === 'on' &&
            property.charAt(2) === property.charAt(2).toUpperCase()
        ) {
            let eventName = Str.lowerCaseFirst(property.substr(2));

            return this.on(eventName, value);
        }

        return super.set(property, value, context);
    }
}
