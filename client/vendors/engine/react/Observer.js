let
    Object = engine.lang.type.Object;

engine.react.Observer = class extends Object {

    initialize() {
        this.owner.on(this);
    }
}
