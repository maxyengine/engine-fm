let
    Str = engine.lang.utility.String;

engine.react.Observable = class {

    constructor(...observers) {
        this.observers = [];
        this.addObservers(...observers);
    }

    addObserver(observer, callback) {
        if (callback) {
            let onEvent = 'on' + Str.capitalize(observer);
            observer = {};
            observer[onEvent] = callback;
        }
        this.observers.push(observer);

        return this;
    }

    removeObserver(observer) {
        this.observers = this.observers.filter(element => {
            return element !== observer;
        });

        return this;
    }

    addObservers(...observers) {
        observers.forEach(observer => {
            this.addObserver(observer);
        });

        return this;
    }

    trigger(eventName, ...args) {
        let onEvent = 'on' + Str.capitalize(eventName);
        try {
            this.observers.forEach(observer => {
                observer[onEvent] && observer[onEvent](...args);
            });
            this.observers.forEach(observer => {
                observer[onEvent + 'Complete'] && observer[onEvent + 'Complete'](...args);
            });
        } catch (throwable) {
            this.observers.forEach(observer => {
                // todo: remove temporary solution;
                console.error(throwable);
                observer['onError'] && observer['onError'](throwable, ...args);
            });
        }
    }
}
