engine.gui.utility.Element = class {
    
    static clear(element) {
        while (element.hasChildNodes()) {
            element.removeChild(element.firstChild);
        }
    }
}
