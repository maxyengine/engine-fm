let self = engine.gui.utility.ClassName = class {

    static has(element, className) {
        return !!element.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
    }

    static add(element, className) {
        if (!self.has(element, className)) {
            element.className += element.className ? ' ' + className : className;
        }
    }

    static set(element, className) {
        element.className = className;
    }

    static remove(element, className) {
        if (self.has(element, className)) {
            element.className = element.className.replace(new RegExp('(\\s|^)' + className + '(\\s|$)'), '');
        }
    }
}
