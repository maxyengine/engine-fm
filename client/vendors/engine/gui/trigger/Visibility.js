let
    Object = engine.lang.type.Object;

engine.gui.trigger.Visibility = class extends Object {

    get properties() {
        return {
            show: this.show,
            hide: this.hide
        };
    }

    show(params) {
        this.owner.element.style.display = 'block';
        this.owner.trigger('show', params);
    }

    hide() {
        this.owner.element.style.display = 'none';
        this.owner.trigger('hide');
    }
}
