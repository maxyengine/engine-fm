let
    Object = engine.lang.type.Object;

let self = engine.gui.trigger.Activity = class extends Object {

    static get instances() {
        if (!self._instances) {
            self._instances = [];
        }

        return self._instances;
    }

    get properties() {
        return {
            activate: this.activate,
            deactivate: this.deactivate
        };
    }

    initialize() {
        self.instances.push(this.owner);

        this.owner.element.addEventListener('click', () => {
            this.owner.activate();
        }, true);

        document.addEventListener('click', () => {
            this.owner.deactivate();
        }, true);
    }

    activate() {
        if (this.owner.isActive) {
            return;
        }
        this.owner.isActive = true;
        this.owner.trigger('activate');

        self.instances.forEach(instance => {
            instance !== this.owner && instance.deactivate();
        });
    }

    deactivate() {
        this.owner.isActive = false;
        this.owner.trigger('deactivate');
    }
}
