let
    Obj = engine.lang.utility.Object,
    HotKey = engine.gui.component.HotKey,
    Observer = engine.react.Observer;

engine.gui.trigger.HotKeys = class extends Observer {

    initialize() {
        document.addEventListener('keydown', event => {
            if (this.owner.isActive) {
                this.hotKeys.forEach(hotKey => {
                    if (hotKey.isRecognized(event)) {
                        event.preventDefault();
                        this.owner.trigger(hotKey.name, event);
                    }
                });
            }
        });
    }

    get hotKeys() {
        if (!this._hotKeys) {
            this._hotKeys = [];
            Obj.forEach(this.owner.hotKeys, (name, value) => {
                this._hotKeys.push(new HotKey({
                    name: name,
                    keys: value
                }));
            });
        }

        return this._hotKeys;
    }
}
