let
    Object = engine.lang.type.Object,
    Arr = engine.lang.utility.Array,
    Type = engine.lang.utility.Type,
    Str = engine.lang.utility.String;

let self = engine.gui.component.HotKey = class extends Object {

    static isSpecialKey(key) {
        return Arr.contains([
            'alt',
            'ctrl',
            'shift',
            'meta',
            'access'
        ], key.toLocaleLowerCase());
    }

    static codeFromKey(key) {
        if (parseInt(key) == key) {
            return parseInt(key);
        }

        switch (key.toLocaleLowerCase()) {
            case 'enter':
                return 13;
            case 'esc':
                return 27;
            case 'delete':
                return 46;
        }

        return key.toLocaleUpperCase().charCodeAt(0);
    }

    isRecognized(keyboardEvent) {
        for (let i = 0; this.keys[i]; i++) {
            let key = this.keys[i];

            if (self.isSpecialKey(key)) {
                if (!keyboardEvent[key.toLocaleLowerCase() + 'Key']) {
                    return false;
                }
                continue;
            }

            if (Str.contains(key, '-')) {
                let exceptKeys = key.split('-');
                key = exceptKeys.shift();
                for (let j = 0; exceptKeys[j]; j++) {
                    let exceptKey = exceptKeys[j];
                    if (!self.isSpecialKey(exceptKey) || keyboardEvent[exceptKey.toLocaleLowerCase() + 'Key']) {
                        return false;
                    }
                }
            }

            if (keyboardEvent.keyCode !== self.codeFromKey(key)) {
                return false;
            }
        }

        return true;
    }

    set keys(value) {
        if (Type.isNumber) {
            value = value.toString();
        }

        this._keys = value.split('+');
    }

    get keys() {
        return this._keys;
    }
}
