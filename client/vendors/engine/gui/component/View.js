let
    Component = engine.react.Component,
    Element = engine.gui.utility.Element,
    Template = engine.gui.component.Template;

let self = engine.gui.component.View = class extends Component {

    get template() {
        return Template;
    }

    get element() {
        return this._element;
    }

    _constructor(...args) {
        this.use(this.template);
        super._constructor(...args);
        this.data = {};
    }

    clear() {
        Element.clear(this.element);
    }

    appendTo(element) {
        element.appendChild(this.element);
    }
}
