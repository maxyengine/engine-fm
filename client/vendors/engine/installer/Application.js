let
    View = engine.gui.component.View,
    Client = engine.web.component.Client,
    Element = engine.gui.utility.Element,
    Template = engine.installer.template.Layout;

engine.installer.Application = class extends View {

    get template() {
        return Template;
    }

    get events() {
        return {
            cancel: {cancel: 'click'},
            submit: {submit: 'click'},
            success: {client: 'success'}
        };
    }

    set config(value) {
        this.editor.value = JSON.stringify(value, undefined, 4);
    }

    get config() {
        return JSON.parse(this.editor.value);
    }

    initialize() {


        this.configUrl = this.configUrl || './config';
        this.wrapper = this.wrapper || document.body;
        this.client = new Client();
    }

    run() {
        this.original = this.config;
        Element.clear(this.wrapper);
        this.wrapper.appendChild(this.render());
    }

    onCancel() {
        this.config = this.original;
    }

    onSubmit() {
        this.client.exchange({
            url: this.configUrl,
            method: 'POST',
            body: JSON.stringify(this.config)
        });
    }
}
