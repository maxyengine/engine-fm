let
    Component = engine.react.Component,
    Layout = engine.widget.calendarGrid.view.Layout,
    Controller = engine.widget.calendarGrid.Controller;

engine.widget.CalendarGrid = class extends Component {

    get defaultProperties() {
        return {
            wrapper: document.body,
            year: (new Date()).getFullYear(),
            fromMonday: true,
            highlightWeekends: true,
            reverseDirection: false,
            shortMonth: false,
            showWeekDays: true,
            showDayOut: true,
            shortWeekDay: true,
            showWeekDaysInTheRow: false,
            showMonthsInTheRow: true,
            monthsPerRow: 4,
            monthsPerColumn: 4,
            months: [
                {full: 'January', short: 'Jan'},
                {full: 'February', short: 'Feb'},
                {full: 'March', short: 'Mar'},
                {full: 'April', short: 'Apr'},
                {full: 'May', short: 'May'},
                {full: 'June', short: 'Jun'},
                {full: 'Jule', short: 'Jul'},
                {full: 'August', short: 'Aug'},
                {full: 'September', short: 'Sep'},
                {full: 'October', short: 'Oct'},
                {full: 'November', short: 'Nov'},
                {full: 'December', short: 'Dec'}
            ],
            days: [
                {full: 'Sunday', short: 'Su'},
                {full: 'Monday', short: 'Mo'},
                {full: 'Tuesday', short: 'Tu'},
                {full: 'Wednesday', short: 'We'},
                {full: 'Thursday', short: 'Th'},
                {full: 'Friday', short: 'Fr'},
                {full: 'Saturday', short: 'St'}
            ]
        };
    }

    get layout() {
        if (!this._layout) {
            this._layout = new Layout({
                wrapper: this.wrapper
            });
        }

        return this._layout;
    }

    get controller() {
        if (!this._controller) {
            this._controller = new Controller({
                view: this.layout,
                config: this.config
            });
        }

        return this._controller;
    }

    initialize() {
        this.config = {
            year: this.year,
            fromMonday: this.fromMonday,
            highlightWeekends: this.highlightWeekends,
            reverseDirection: this.reverseDirection,
            shortMonth: this.shortMonth,
            showWeekDays: this.showWeekDays,
            showDayOut: this.showDayOut,
            shortWeekDay: this.shortWeekDay,
            showWeekDaysInTheRow: this.showWeekDaysInTheRow,
            showMonthsInTheRow: this.showMonthsInTheRow,
            monthsPerRow: this.monthsPerRow,
            monthsPerColumn: this.monthsPerColumn,
            months: this.months,
            days: this.days
        };
    }

    render() {
        this.controller.render();
    }
}
