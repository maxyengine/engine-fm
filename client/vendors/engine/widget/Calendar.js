let
    Component = engine.react.Component,
    Layout = engine.widget.calendar.view.Layout,
    Controller = engine.widget.calendar.Controller,
    Model = engine.widget.calendar.Model;

engine.widget.Calendar = class extends Component {

    constructor(...args) {
        super(...args);
        this.setDate(new Date());
    }

    initialize() {
        this.wrapper = this.wrapper || document.body;
        this.fromMonday = this.fromMonday !== undefined ? this.fromMonday : true;
        this.mode = this.mode || 'month';
        this.days = this.days || [
            {full: 'Monday', short: 'Mo'},
            {full: 'Tuesday', short: 'Tu'},
            {full: 'Wednesday', short: 'We'},
            {full: 'Thursday', short: 'Th'},
            {full: 'Friday', short: 'Fr'},
            {full: 'Saturday', short: 'St'},
            {full: 'Sunday', short: 'Su'},
        ];
        this.months = this.months || [
            {full: 'January', short: 'Jan'},
            {full: 'February', short: 'Feb'},
            {full: 'March', short: 'Mar'},
            {full: 'April', short: 'Apr'},
            {full: 'May', short: 'May'},
            {full: 'June', short: 'Jun'},
            {full: 'Jule', short: 'Jul'},
            {full: 'August', short: 'Aug'},
            {full: 'September', short: 'Sep'},
            {full: 'October', short: 'Oct'},
            {full: 'November', short: 'Nov'},
            {full: 'December', short: 'Dec'}
        ];
    }

    get events() {
        return {
            selectDate: {controller: 'selectDate'}
        };
    }

    get layout() {
        if (!this._layout) {
            this._layout = new Layout();
            this.wrapper.appendChild(this._layout.element);
        }

        return this._layout;
    }

    get model() {
        if (!this._model) {
            this._model = new Model();
        }

        return this._model;
    }

    get controller() {
        if (!this._controller) {
            this._controller = new Controller({
                model: this.model,
                view: this.layout,
                days: this.days,
                months: this.months,
                fromMonday: this.fromMonday,
                mode: this.mode
            });
        }

        return this._controller;
    }

    getDate() {
        return this.controller.getDate();
    }

    setDate(value) {
        this.controller.setDate(value);

        return this;
    }

    selectDate(value) {
        this.controller.selectDate(value);

        return this;
    }
}
