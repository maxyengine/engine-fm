let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Week;

engine.widget.calendarGrid.view.Week = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    appendDate(date) {
        if (this.reverseDirection) {
            this.element.prepend(date.render());
        } else {
            this.element.appendChild(date.render());
        }

        return date;
    }
}
