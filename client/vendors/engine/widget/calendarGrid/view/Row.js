let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Row;

engine.widget.calendarGrid.view.Row = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    appendMonth(month) {
        this.element.appendChild(month.render());

        return month;
    }
}
