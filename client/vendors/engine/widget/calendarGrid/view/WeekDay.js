let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.WeekDay;

engine.widget.calendarGrid.view.WeekDay = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    set value(value) {
        this._text.nodeValue = value;
    }
}
