let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Year,
    Month = engine.widget.calendarGrid.view.Month,
    Row = engine.widget.calendarGrid.view.Row;

let self = engine.widget.calendarGrid.view.Year = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    render(config, months) {
        if (config.showMonthsInTheRow) {
            this.renderByRows(config, months);
        } else {
            this.renderByColumns(config, months);
        }

        return this.element;
    }

    renderByRows(config, months) {
        this.clear();
        let row = null;
        months.forEach((weeks, month) => {
            if (month % config.monthsPerRow === 0) {
                row = new Row();
                this.appendRow(row);
            }
            row.appendMonth(self.createMonth(month, weeks, config));
        });
    }

    renderByColumns(config, months) {
        this.clear();
        let
            rows = [],
            i = 0;
        months.forEach((weeks, month) => {
            if (!rows[i]) {
                rows[i] = new Row();
                this.appendRow(rows[i]);
            }
            rows[i].appendMonth(self.createMonth(month, weeks, config));
            if (++i === config.monthsPerColumn) {
                i = 0;
            }
        });
    }

    static createMonth(month, weeks, config) {
        return new Month({
            value: month,
            days: config.days,
            weeks: weeks,
            monthName: config.shortMonth ? config.months[month].short : config.months[month].full,
            showWeekDays: config.showWeekDays,
            showDayOut: config.showDayOut,
            showWeekDaysInTheRow: config.showWeekDaysInTheRow,
            shortWeekDay: config.shortWeekDay,
            highlightWeekends: config.highlightWeekends,
            reverseDirection: config.reverseDirection,
        });
    }

    appendRow(row) {
        this.element.appendChild(row.render());

        return row;
    }
}
