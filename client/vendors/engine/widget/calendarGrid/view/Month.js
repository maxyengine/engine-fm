let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Month,
    Week = engine.widget.calendarGrid.view.Week,
    DateView = engine.widget.calendarGrid.view.Date,
    DateOut = engine.widget.calendarGrid.view.DateOut,
    WeekDay = engine.widget.calendarGrid.view.WeekDay,
    Element = engine.gui.utility.Element,
    ClassName = engine.gui.utility.ClassName;

let self = engine.widget.calendarGrid.view.Month = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    render() {
        this._monthName.nodeValue = this.monthName;
        this.renderDays();
        this.renderWeeks();

        return this.element;
    }

    renderDays() {
        Element.clear(this.weekDays);
        if (this.showWeekDays && !this.showWeekDaysInTheRow) {
            this.days.forEach((dayName, index) => {
                this.appendWeekDay(this.createWeekDay(index));
            });
        }
    }

    renderWeeks() {
        Element.clear(this.dates);
        if (this.showWeekDaysInTheRow) {
            this.renderDayDates(this.weeks);
        } else {
            this.renderWeekDates(this.weeks);
        }
    }

    renderWeekDates(weeks) {
        Element.clear(this.dates);
        weeks.forEach(dates => {
            let week = this.appendWeek(new Week({
                reverseDirection: this.reverseDirection
            }));
            dates.forEach(date => {
                if (date.getMonth() === this.value) {
                    let dateView = week.appendDate(new DateView({value: date}));
                    if (self.isWeekend(date) && this.highlightWeekends) {
                        ClassName.add(dateView.element, 'weekend');
                    }
                } else {
                    week.appendDate(new DateOut({value: this.showDayOut ? date : null}));
                }
            });
        });

        return this;
    }

    renderDayDates(days) {
        Element.clear(this.dates);
        let rows = [];
        days.forEach(dates => {
            dates.forEach((date, dayIndex) => {
                if (!rows[dayIndex]) {
                    rows[dayIndex] = this.appendWeek(new Week({
                        reverseDirection: this.reverseDirection
                    }));
                    this.showWeekDays && rows[dayIndex].appendDate(this.createWeekDay(dayIndex));
                }
                if (date.getMonth() === this.value) {
                    let dateView = rows[dayIndex].appendDate(new DateView({value: date}));
                    if (self.isWeekend(date) && this.highlightWeekends) {
                        ClassName.add(dateView.element, 'weekend');
                    }
                } else {
                    rows[dayIndex].appendDate(new DateOut({value: this.showDayOut && date}));
                }
            });
        });

        return this;
    }

    appendWeekDay(value) {
        if (this.reverseDirection) {
            this.weekDays.prepend(value.render());
        } else {
            this.weekDays.appendChild(value.render());
        }
    }

    appendWeek(week) {
        this.dates.appendChild(week.render());

        return week;
    }

    createWeekDay(index) {
        let dayName = this.days[index];
        return new WeekDay({
            value: this.shortWeekDay ? dayName.short : dayName.full
        })
    }

    static isWeekend(date) {
        return date.getDay() === 0 || date.getDay() === 6;
    }
}
