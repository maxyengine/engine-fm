let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Layout,
    ControlPanel = engine.widget.calendarGrid.view.ControlPanel,
    Year = engine.widget.calendarGrid.view.Year,
    Obj = engine.lang.utility.Object;

engine.widget.calendarGrid.view.Layout = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            changeConfig: {controlPanel: 'changeConfig'}
        };
    }

    get controlPanel() {
        if (!this._controlPanel) {
            this._controlPanel = new ControlPanel();
        }

        return this._controlPanel;
    }

    get year() {
        if (!this._viewGrid) {
            this._viewGrid = new Year();
        }

        return this._viewGrid;
    }

    get config() {
        return Obj.merge(this.controlPanel.config);
    }

    render(config, months) {
        this.controlPanel.render(config);
        this.year.render(config, months);
        this.wrapUp();

        return this.element;
    }

    wrapUp() {
        if (this.hasWrapped) {
            return;
        }
        this.element.appendChild(this.controlPanel.element);
        this.element.appendChild(this.year.element);
        this.wrapper && this.wrapper.appendChild(this.element);
        this.hasWrapped = true;

    }
}
