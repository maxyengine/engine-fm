let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.ControlPanel,
    Switch = engine.widget.calendarGrid.view.Switch,
    Range = engine.widget.calendarGrid.view.Range,
    ClassName = engine.gui.utility.ClassName;

engine.widget.calendarGrid.view.ControlPanel = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            changeConfig: {
                _fromMonday: 'change',
                _showWeekDays: 'change',
                _highlightWeekends: 'change',
                _showDayOut: 'change',
                _shortWeekDay: 'change',
                _shortMonth: 'change',
                _showWeekDaysInTheRow: 'change',
                _reverseDirection: 'change',
                _showMonthsInTheRow: 'change',
                _monthsPerRow: 'change',
                _monthsPerColumn: 'change',
                applyYear: 'click'
            }
        }
    }

    get config() {
        return {
            year: this.year,
            fromMonday: this.fromMonday,
            highlightWeekends: this.highlightWeekends,
            reverseDirection: this.reverseDirection,
            shortMonth: this.shortMonth,
            showWeekDays: this.showWeekDays,
            showDayOut: this.showDayOut,
            shortWeekDay: this.shortWeekDay,
            showWeekDaysInTheRow: this.showWeekDaysInTheRow,
            showMonthsInTheRow: this.showMonthsInTheRow,
            monthsPerRow: this.monthsPerRow,
            monthsPerColumn: this.monthsPerColumn
        }
    }

    get fromMonday() {
        return this._fromMonday.checked;
    }

    set fromMonday(value) {
        this._fromMonday.checked = value;
    }

    get showWeekDays() {
        return this._showWeekDays.checked;
    }

    set showWeekDays(value) {
        this._showWeekDays.checked = value;
    }

    get shortMonth() {
        return this._shortMonth.checked;
    }

    set shortMonth(value) {
        this._shortMonth.checked = value;
    }

    get highlightWeekends() {
        return this._highlightWeekends.checked;
    }

    set highlightWeekends(value) {
        this._highlightWeekends.checked = value;
    }

    get showDayOut() {
        return this._showDayOut.checked;
    }

    set showDayOut(value) {
        this._showDayOut.checked = value;
    }

    get shortWeekDay() {
        return this._shortWeekDay.checked;
    }

    set shortWeekDay(value) {
        this._shortWeekDay.checked = value;
    }

    get showWeekDaysInTheRow() {
        return this._showWeekDaysInTheRow.isOn;
    }

    set showWeekDaysInTheRow(value) {
        value ? this._showWeekDaysInTheRow.turnOn() : this._showWeekDaysInTheRow.turnOff();
    }

    get reverseDirection() {
        return this._reverseDirection.isOn;
    }

    set reverseDirection(value) {
        value ? this._reverseDirection.turnOn() : this._reverseDirection.turnOff();
    }

    get showMonthsInTheRow() {
        return this._showMonthsInTheRow.isOn;
    }

    set showMonthsInTheRow(value) {
        if (value) {
            this._showMonthsInTheRow.turnOn();
            this.activeMonthsPerRow();
        } else {
            this._showMonthsInTheRow.turnOff();
            this.activeMonthsPerColumn();
        }
    }

    get monthsPerColumn() {
        return this._monthsPerColumn.value;
    }

    set monthsPerColumn(value) {
        this._monthsPerColumn.value = value;
    }
    
    get monthsPerRow() {
        return this._monthsPerRow.value;
    }

    set monthsPerRow(value) {
        this._monthsPerRow.value = value;
    }

    initialize() {
        this._showWeekDaysInTheRow = new Switch({
            captionOff: 'Column',
            captionOn: 'Row'
        });
        this._reverseDirection = new Switch({
            captionOff: 'Left to right',
            captionOn: 'Right to left'
        });
        this._showMonthsInTheRow = new Switch({
            captionOff: 'Column',
            captionOn: 'Row'
        });
        this._monthsPerColumn = new Range({
            values: [1, 2, 3, 4, 6, 12]
        });
        this._monthsPerRow = new Range({
            values: [1, 2, 3, 4, 6, 12]
        });
    }

    get year() {
        return parseInt(this._year.value);
    }

    set year(value) {
        this._year.value = value;
    }

    render(config) {
        this.wrapUp();
        this.setProperties(config);

        return this.element;
    }

    wrapUp() {
        if (this.hasWrapped) {
            return;
        }
        this.showWeekDaysInTheRowWrapper.appendChild(this._showWeekDaysInTheRow.render());
        this.reverseDirectionWrapper.appendChild(this._reverseDirection.render());
        this.showMonthsInTheRowWrapper.appendChild(this._showMonthsInTheRow.render());
        this.monthsPerColumnWrapper.appendChild(this._monthsPerColumn.render());
        this.monthsPerRowWrapper.appendChild(this._monthsPerRow.render());
        this.hasWrapped = true;
    }

    activeMonthsPerColumn() {
        ClassName.remove(this.monthsPerColumnWrapper, 'inactive');
        ClassName.add(this.monthsPerRowWrapper, 'inactive');
    }

    activeMonthsPerRow() {
        ClassName.remove(this.monthsPerRowWrapper, 'inactive');
        ClassName.add(this.monthsPerColumnWrapper, 'inactive');
    }
}
