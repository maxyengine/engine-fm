let
    View = engine.gui.component.View,
    ClassName = engine.gui.utility.ClassName,
    Template = engine.widget.calendarGrid.template.DateOut;

engine.widget.calendarGrid.view.DateOut = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    set value(value) {
        if (value) {
            this._text.nodeValue = value.getDate();
        }
    }
}
