let
    View = engine.gui.component.View,
    ClassName = engine.gui.utility.ClassName,
    Template = engine.widget.calendarGrid.template.Switch;

engine.widget.calendarGrid.view.Switch = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            turnOff: {_turnOff: 'click'},
            turnOn: {_turnOn: 'click'},
        };
    }

    get isOff() {
        return ClassName.has(this._turnOff, 'active');
    }

    get isOn() {
        return ClassName.has(this._turnOn, 'active');
    }

    set captionOff(value) {
        this._captionOff.nodeValue = value;
    }

    set captionOn(value) {
        this._captionOn.nodeValue = value;
    }

    turnOff() {
        ClassName.add(this._turnOff, 'active');
        ClassName.remove(this._turnOn, 'active');
    }

    turnOn() {
        ClassName.add(this._turnOn, 'active');
        ClassName.remove(this._turnOff, 'active');
    }

    onTurnOff() {
        if (this.isOn) {
            this.turnOff();
            this.trigger('change');
        }
    }

    onTurnOn() {
        if (this.isOff) {
            this.turnOn();
            this.trigger('change');
        }
    }
}
