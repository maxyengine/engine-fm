let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Date;

engine.widget.calendarGrid.view.Date = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    set value(value) {
        this._text.nodeValue = value.getDate();
    }
}
