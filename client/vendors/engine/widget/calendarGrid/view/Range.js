let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Range,
    RangeButton = engine.widget.calendarGrid.view.RangeButton;

engine.widget.calendarGrid.view.Range = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    set value(value) {
        this._value = value;
        this.buttons.forEach(button => {
            this.value === button.value ? button.activate() : button.inactivate();
        });
    }

    get value() {
        return this._value;
    }

    set values(values) {
        this._values = values;
    }

    get values() {
        return this._values;
    }

    render() {
        this.clear();
        this.buttons = [];

        this.values.forEach(value => {
            this.buttons.push(
                this.appendButton(
                    new RangeButton({
                        value: value,
                        onSelect: () => {
                            this.value = value;
                            this.trigger('change');
                        }
                    })
                )
            );
        });

        return this.element;
    }

    appendButton(button) {
        this.element.appendChild(button.render());

        return button;
    }
}
