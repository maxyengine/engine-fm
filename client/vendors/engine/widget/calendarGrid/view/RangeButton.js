let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.RangeButton,
    ClassName = engine.gui.utility.ClassName;

engine.widget.calendarGrid.view.RangeButton = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            select: {element: 'click'}
        };
    }

    set value(value) {
        this._value.nodeValue = value;
    }

    get value() {
        return parseInt(this._value.nodeValue);
    }

    activate() {
        ClassName.add(this.element, 'active');
    }

    inactivate() {
        ClassName.remove(this.element, 'active');
    }
}
