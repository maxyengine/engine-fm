let
    WebController = engine.web.component.Controller,
    Obj = engine.lang.utility.Object,
    DateUtil = engine.lang.utility.Date;

engine.widget.calendarGrid.Controller = class extends WebController {

    get events() {
        return {
            changeConfig: {view: 'changeConfig'}
        };
    }

    initialize() {
        this.days = this.config.days;
        this.revertDays = this.days.slice(1);
        this.revertDays.push(this.days[0]);
    }

    onChangeConfig() {
        this.config = Obj.merge(
            this.config,
            this.view.config
        );
        this.render();
    }

    onRender() {
        this.config.days = this.config.fromMonday ? this.revertDays : this.days;
        this.view.render(
            this.config,
            DateUtil.createYearMonths(
                this.config.year,
                this.config.fromMonday
            )
        );
    }
}
