engine.widget.calendar.Model = class {

    getMonth(date, fromMonday) {
        date = new Date(date.getTime());
        let
            firstInMonth = new Date(date.getFullYear(), date.getMonth(), 1),
            delta = fromMonday ? (firstInMonth.getDay() === 0 ? 7 : firstInMonth.getDay()) : 1,
            i = 0,
            j = 0,
            weeks = [];
        date.setDate(firstInMonth.getDate() - delta);
        while (6 > i++) {
            let week = [];
            while (7 > j++) {
                date.setDate(date.getDate() + 1);
                week.push(new Date(date.getTime()));
            }
            weeks.push(week);
            j = 0;
        }

        return weeks;
    }

    getYear(date) {
        date = new Date(date.getTime());
        date.setMonth(0);
        let
            quarters = [],
            i = 0,
            j = 0;
        while (4 > i++) {
            let quarter = [];
            while (3 > j++) {
                quarter.push(date);
                date = this.monthAhead(date);
            }
            quarters.push(quarter);
            j = 0;
        }

        return quarters;
    }

    getDecade(date) {
        date = new Date(date.getTime());
        date.setFullYear(Math.floor(date.getFullYear() / 10) * 10 - 1);
        let
            quarters = [],
            i = 0,
            j = 0;
        while (4 > i++) {
            let quarter = [];
            while (3 > j++) {
                quarter.push(new Date(date.getTime()));
                date.setFullYear(date.getFullYear() + 1);
            }
            quarters.push(quarter);
            j = 0;
        }

        return quarters;
    }

    monthAgo(date) {
        let month = date.getMonth();
        date = new Date(date.getTime());
        date.setMonth(date.getMonth() - 1);
        if (date.getMonth() === month) {
            date.setDate(0);
        }

        return date;
    }

    monthAhead(date) {
        let month = date.getMonth();
        date = new Date(date.getTime());
        date.setMonth(date.getMonth() + 1);
        if (date.getMonth() === month + 2) {
            date.setDate(0);
        }

        return date;
    }

    yearAgo(date) {
        date = new Date(date.getTime());
        date.setFullYear(date.getFullYear() - 1);

        return date;
    }

    yearAhead(date) {
        date = new Date(date.getTime());
        date.setFullYear(date.getFullYear() + 1);

        return date;
    }

    decadeAgo(date) {
        date = new Date(date.getTime());
        date.setFullYear(date.getFullYear() - 10);

        return date;
    }

    decadeAhead(date) {
        date = new Date(date.getTime());
        date.setFullYear(date.getFullYear() + 10);

        return date;
    }
}
