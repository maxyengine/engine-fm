let
    View = engine.gui.component.View,
    ClassName = engine.gui.utility.ClassName,
    Template = engine.widget.calendar.template.Date;

engine.widget.calendar.view.Year = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            pick: {element: 'click'}
        };
    }

    set date(value) {
        this._value = value;
        this._text.nodeValue = value.getFullYear();
    }

    get date() {
        return this._value;
    }

    set isSelected(value) {
        this._isSelected = value;
        if (value) {
            ClassName.add(this.element, 'selected');
        }
    }

    get isSelected() {
        return this._isSelected;
    }
}
