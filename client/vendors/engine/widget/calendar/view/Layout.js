let
    View = engine.gui.component.View,
    Template = engine.widget.calendar.template.Layout,
    Day = engine.widget.calendar.view.Day,
    Week = engine.widget.calendar.view.Week,
    DateView = engine.widget.calendar.view.Date,
    Quarter = engine.widget.calendar.view.Quarter,
    Month = engine.widget.calendar.view.Month,
    YearRow = engine.widget.calendar.view.YearRow,
    Year = engine.widget.calendar.view.Year,
    Element = engine.gui.utility.Element;

engine.widget.calendar.view.Layout = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            prev: {prev: 'click'},
            next: {next: 'click'},
            switchMode: {title: 'click'}
        };
    }

    renderTitleMonth(value) {
        this.month.nodeValue = value;

        return this;
    }

    renderTitleYear(year1, year2) {
        this.year.nodeValue = year1;
        this.year.nodeValue += year2 ? ' - ' + year2 : '';

        return this;
    }

    renderDays(days) {
        Element.clear(this.days);

        days.forEach(day => {
            this.appendDay(new Day({
                value: day.short
            }));
        });

        return this;
    }

    renderMonth(weeks, activeDate) {
        Element.clear(this.body);

        weeks.forEach(dates => {
            let week = this.appendRow(new Week());
            dates.forEach(date => {
                week.appendDate(new DateView({
                    date: date,
                    isSelected: date.getTime() === activeDate.getTime(),
                    onPick: (event, view) => {
                        this.trigger('selectDate', {
                            date: view.date
                        });
                    }
                }));
            });
        });

        return this;
    }

    renderYear(quarters, activeDate, months) {
        Element.clear(this.body);

        quarters.forEach(quarter => {
            let row = this.appendRow(new Quarter());
            quarter.forEach(date => {
                row.appendMonth(new Month({
                    months: months,
                    date: date,
                    isSelected: date.getMonth() === activeDate.getMonth(),
                    onPick: (event, view) => {
                        this.trigger('selectMonth', {
                            date: view.date
                        });
                    }
                }));
            });
        });

        return this;
    }

    renderDecade(quarters, activeDate) {
        Element.clear(this.body);

        quarters.forEach(quarter => {
            let row = this.appendRow(new YearRow());
            quarter.forEach(date => {
                row.appendYear(new Year({
                    date: date,
                    isSelected: date.getFullYear() === activeDate.getFullYear(),
                    onPick: (event, view) => {
                        this.trigger('selectYear', {
                            date: view.date
                        });
                    }
                }));
            });
        });

        return this;
    }

    appendDay(day) {
        this.days.appendChild(day.render());

        return day;
    }

    appendRow(row) {
        this.body.appendChild(row.render());

        return row;
    }
}
