let
    View = engine.gui.component.View,
    Template = engine.widget.calendar.template.Row;

engine.widget.calendar.view.Week = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    appendDate(date) {
        this.element.appendChild(date.render());

        return date;
    }
}
