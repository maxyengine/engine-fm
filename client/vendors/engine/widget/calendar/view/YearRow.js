let
    View = engine.gui.component.View,
    Template = engine.widget.calendar.template.Row;

engine.widget.calendar.view.YearRow = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    appendYear(years) {
        this.element.appendChild(years.render());

        return years;
    }
}
