let
    View = engine.gui.component.View,
    Template = engine.widget.calendar.template.Day;

engine.widget.calendar.view.Day = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    set value(value) {
        this._text.nodeValue = value;
    }
}
