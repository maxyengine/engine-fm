let
    View = engine.gui.component.View,
    Template = engine.widget.calendar.template.Row;

engine.widget.calendar.view.Quarter = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    appendMonth(month) {
        this.element.appendChild(month.render());

        return month;
    }
}
