let
    WebController = engine.web.component.Controller,
    SetDate = engine.widget.calendar.action.SetDate,
    SelectDate = engine.widget.calendar.action.SelectDate,
    SelectMonth = engine.widget.calendar.action.SelectMonth,
    SelectYear = engine.widget.calendar.action.SelectYear,
    Prev = engine.widget.calendar.action.Prev,
    Next = engine.widget.calendar.action.Next,
    SwitchMode = engine.widget.calendar.action.SwitchMode;

engine.widget.calendar.Controller = class extends WebController {

    get properties() {
        return [
            'model',
            'view',
            'days',
            'months',
            'fromMonday'
        ];
    }

    get methods() {
        return [
            'getDate',
            'setDate',
            'selectDate',
            'getMode',
            'setMode',
        ];
    }

    get actions() {
        return {
            setDate: SetDate,
            selectDate: SelectDate,
            selectMonth: SelectMonth,
            selectYear: SelectYear,
            switchMode: SwitchMode,
            prev: Prev,
            next: Next
        };
    }

    get events() {
        return {
            setDate: {view: 'setDate'},
            selectDate: {view: 'selectDate'},
            selectMonth: {view: 'selectMonth'},
            selectYear: {view: 'selectYear'},
            switchMode: {view: 'switchMode'},
            prev: {view: 'prev'},
            next: {view: 'next'}
        };
    }

    getDate() {
        return this.date;
    }

    setDate(value) {
        this.date = value;
        this.trigger('setDate', {
            date: value
        });
    }

    selectDate(value) {
        this.trigger('selectDate', {
            date: value
        });
    }

    getMode() {
        return this.mode;
    }

    setMode(value) {
        this.mode = value;
        this.setDate(this.getDate());
    }
}
