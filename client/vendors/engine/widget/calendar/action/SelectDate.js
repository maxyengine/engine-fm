let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.SelectDate = class extends Object {

    execute(event) {
        this.setDate(event.date);
    }
}
