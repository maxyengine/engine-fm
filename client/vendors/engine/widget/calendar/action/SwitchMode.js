let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.SwitchMode = class extends Object {

    execute() {
        switch (this.getMode()) {
            case 'month':
                this.setMode('year');
                break;
            case 'year':
                this.setMode('decade');
                break;
        }
    }
}
