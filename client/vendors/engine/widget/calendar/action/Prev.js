let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.Prev = class extends Object {

    execute() {
        switch (this.getMode()) {
            case 'month':
                this.setDate(this.model.monthAgo(this.getDate()));
                break;
            case 'year':
                this.setDate(this.model.yearAgo(this.getDate()));
                break;
            case 'decade':
                this.setDate(this.model.decadeAgo(this.getDate()));
        }
    }
}
