let
    Object = engine.lang.type.Object,
    Str = engine.lang.utility.String;

engine.widget.calendar.action.SetDate = class extends Object {

    execute(event) {
        let method = 'render' + Str.capitalize(this.getMode());
        this[method](event.date);
    }

    renderMonth(date) {
        this.view
            .renderTitleMonth(this.months[date.getMonth()].full)
            .renderTitleYear(date.getFullYear())
            .renderDays(this.days)
            .renderMonth(this.model.getMonth(date, this.fromMonday), date);
    }

    renderYear(date) {
        this.view
            .renderTitleMonth('')
            .renderTitleYear(date.getFullYear())
            .renderDays([])
            .renderYear(this.model.getYear(date), date, this.months);
    }

    renderDecade(date) {
        let decade = this.model.getDecade(date);
        this.view
            .renderTitleMonth('')
            .renderTitleYear(decade[0][1].getFullYear(), decade[3][1].getFullYear())
            .renderDays([])
            .renderDecade(decade, date);
    }
}
