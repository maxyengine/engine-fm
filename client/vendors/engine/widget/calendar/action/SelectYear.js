let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.SelectYear = class extends Object {

    execute(event) {
        this.setMode('year');
        this.setDate(event.date);
    }
}
