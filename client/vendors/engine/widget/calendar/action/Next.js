let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.Next = class extends Object {

    execute() {
        switch (this.getMode()) {
            case 'month':
                this.setDate(this.model.monthAhead(this.getDate()));
                break;
            case 'year':
                this.setDate(this.model.yearAhead(this.getDate()));
                break;
            case 'decade':
                this.setDate(this.model.decadeAhead(this.getDate()));
        }
    }
}
