let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.SelectMonth = class extends Object {

    execute(event) {
        this.setMode('month');
        this.setDate(event.date);
    }
}
