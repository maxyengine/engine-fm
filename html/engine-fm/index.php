<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>File Manager</title>
    <link rel="shortcut icon" href="favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="styles/default.css">
</head>
<body>

<div id="file-manager"></div>

<script src="js/app-es5.js"></script>
<script type="text/javascript">
    new engine.fileManager.Application({
        wrapper: document.getElementById('file-manager'),
        configUrl: 'server.php?action=config&',
        csrfToken: 'csrfToken'
    });
</script>

</body>
</html>
