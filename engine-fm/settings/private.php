<?php

return [
    'storagePath' => __DIR__.'/../root',
    'settingsPath' => __DIR__,
    'temporaryPath' => __DIR__.'/../runtime/tmp',
    'csrfTokenName' => 'engine.fileManager.Application.csrfToken',
];
