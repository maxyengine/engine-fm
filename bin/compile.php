<?php

class DOMParser
{
    private $counter;
    private $properties;

    public function parseHTML(string $html)
    {
        $this->counter = 0;
        $this->properties = [];

        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML('<?xml encoding="UTF-8">'.$html);
        libxml_clear_errors();

        foreach ($dom->getElementsByTagName('body')[0]->childNodes as $node) {
            if (!in_array($node->nodeName, ['#text', '#comment'])) {
                $script = $this->parseNode($node);

                return [$script, $this->properties];
            }
        }
    }

    private function parseNode(DOMNode $node)
    {
        $element = "element{$this->counter}";
        $script = $this->createNode($node, $element);

        if ($node->hasChildNodes()) {
            foreach ($node->childNodes as $childNode) {
                ++$this->counter;
                $child = "element{$this->counter}";

                $script .= $this->parseNode($childNode);
                $script .= "{$element}.appendChild({$child});";
            }
        }

        return $script;
    }

    private function createNode(DOMNode $node, $variable)
    {
        $script = '';

        switch ($node->nodeName) {
            case '#text':
                $value = trim($node->nodeValue);
                if (isset($value[0]) && '{' === $value[0] && '}' === $value[strlen($value) - 1]) {
                    $value = trim($value, '{}');
                    $default = null;
                    if (strpos($value, '|')) {
                        list($value, $default) = explode('|', $value);
                    }
                    $script .= "let {$variable} = document.createTextNode('');";
                    $script .= "this.owner.{$value} = {$variable};";
                    if (null !== $default) {
                        $script .= "this.owner.{$value}.nodeValue = '{$default}';";
                    }
                    $this->properties[] = $value;
                } else {
                    $script .= "let {$variable} = document.createTextNode('{$value}');";
                }
                break;
            case '#comment':
                $value = trim($node->nodeValue);
                $script .= "let {$variable} = document.createComment('{$value}');";
                break;
            default:
                $script .= "let {$variable} = document.createElement('{$node->nodeName}');";
                if ($node->hasAttributes()) {
                    foreach ($node->attributes as $attribute) {
                        $value = addslashes($attribute->value);
                        if ('data-property' === $attribute->name) {
                            if (strpos($value, '[]')) {
                                $value = trim($value, '[]');
                                $script .= "this.owner.{$value} = this.owner.{$value} || [];";
                                $script .= "this.owner.{$value}.push({$variable});";
                            } else {
                                if (strpos($value, '.')) {
                                    $object = explode('.', $value)[0];
                                    $script .= "this.owner.{$object} = this.owner.{$object} || {};";
                                }
                                $script .= "this.owner.{$value} = {$variable};";
                            }
                            $property = $object ?? $value;
                            if (!in_array($property, $this->properties)) {
                                $this->properties[] = $property;
                            }
                        } else {
                            $script .= "{$variable}.setAttribute('{$attribute->name}', '{$value}');";
                        }
                    }
                }
        }

        return $script;
    }
}

$sourcePath = str_replace('/', DIRECTORY_SEPARATOR, $argv[1]);
$distancePath = str_replace('/', DIRECTORY_SEPARATOR, $argv[2]);
$template = file_get_contents(__DIR__.str_replace('/', DIRECTORY_SEPARATOR, '/data/template.tpl'));

$parser = new DOMParser();
$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($sourcePath));

$iterator->rewind();
while ($iterator->valid()) {
    if (!$iterator->isDot()) {
        $distanceSubPath = $distancePath.DIRECTORY_SEPARATOR.$iterator->getSubPath();

        if (!file_exists($distanceSubPath)) {
            mkdir($distanceSubPath, 0777, true);
        }

        $path = $distanceSubPath.DIRECTORY_SEPARATOR.str_replace('.html', '.js', $iterator->getFileName());

        $class = str_replace(DIRECTORY_SEPARATOR, '.', $iterator->getSubPath()).'.'.
            str_replace('.html', '', $iterator->getFileName());

        $script = str_replace('{class}', $class, $template);

        list($initialize, $properties) = $parser->parseHTML(file_get_contents($iterator->key()));
        $script = str_replace('{initialize}', $initialize, $script);
        $script = str_replace('{properties}', "'".implode("', '", $properties)."'", $script);

        file_put_contents($path, $script);
    }

    $iterator->next();
}
