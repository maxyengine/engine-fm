<?php

/**
 * Class Linker.
 */
class Linker
{
    /**
     * @var string
     */
    private $vendorsPath;

    /**
     * @var array
     */
    private $vendors;

    /**
     * @var array
     */
    private $dependencies;

    /**
     * @var stdClass
     */
    private $packages;

    /**
     * Linker constructor.
     *
     * @param $vendorsPath
     */
    public function __construct($vendorsPath)
    {
        $this->vendorsPath = $vendorsPath;
    }

    public function build($class, $buildPath)
    {
        $this->vendors = [];
        $this->dependencies = [];
        $this->packages = new stdClass();

        $this->loadDependencies($class);

        $vendorPackages = [];
        foreach ($this->vendors as $vendor) {
            $vendorPackages[] = $vendor.' = '.json_encode($this->packages->$vendor);
        }

        $build = '';
        if (!empty($vendorPackages)) {
            $build .= 'var '.implode(',', $vendorPackages).';'.PHP_EOL;
        }
        foreach ($this->getDependencies() as $dependency) {
            $build .= '{'.
                PHP_EOL.
                file_get_contents($this->vendorsPath.DIRECTORY_SEPARATOR.
                    str_replace('.', DIRECTORY_SEPARATOR, $dependency).'.js').
                '}'.
                PHP_EOL;
        }

        file_put_contents($buildPath, $build);
    }

    private function loadDependencies($class)
    {
        $path = $this->vendorsPath.DIRECTORY_SEPARATOR.str_replace('.', DIRECTORY_SEPARATOR, $class).'.js';
        $file = file($path);

        if (false === $file) {
            throw new InvalidArgumentException(sprintf('Error occurred during reading file \'%s\'', $path));
        }

        if (isset($file[0]) && ("let\r\n" === $file[0] || $file[0] === "let\n")) {
            $require = [];
            for ($i = 1; trim($file[$i]); ++$i) {
                $require[] = trim(explode('=', trim(trim($file[$i]), ';,'))[1]);
            }

            foreach ($require as $item) {
                $this->loadDependencies($item);
            }
        }

        $this->addDependency($class);
    }

    private function getDependencies()
    {
        return $this->dependencies;
    }

    private function addDependency($class)
    {
        if (in_array($class, $this->dependencies)) {
            return;
        }

        $this->dependencies[] = $class;

        $items = explode('.', $class);
        $packages = &$this->packages;
        for ($i = 0; $i < count($items) - 1; ++$i) {
            $name = $items[$i];
            if (0 === $i && !in_array($name, $this->vendors)) {
                $this->vendors[] = $name;
            }
            if (!isset($packages->$name)) {
                $packages->$name = new stdClass();
            }
            $packages = &$packages->$name;
        }
    }

    private function buildPackages($path, $asJSON = true)
    {
        if (!$path instanceof DirectoryIterator) {
            $path = new DirectoryIterator((string) $path);
        }
        $directories = [];
        foreach ($path as $node) {
            if ($node->isDir() && !$node->isDot()) {
                $tree = $this->buildPackages($node->getPathname(), false);
                $directories[$node->getFilename()] = !empty($tree) ? $tree : new stdClass();
            }
        }
        asort($directories);

        return $asJSON ? json_encode($directories) : $directories;
    }
}

$classPath = str_replace('/', DIRECTORY_SEPARATOR, $argv[1]);
$buildPath = str_replace('/', DIRECTORY_SEPARATOR, $argv[2]);
$class = $argv[3];

$linker = new Linker($classPath);
$linker->build($class, $buildPath);
