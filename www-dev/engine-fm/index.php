<?php

/**
 * !!! Important part to protect application.
 */
session_start();
header('Content-Type: text/html; charset=utf-8');

$csrfTokenName = 'engine.fileManager.Application.csrfToken';

if (isset($_SESSION[$csrfTokenName])) {
    $csrfToken = $_SESSION[$csrfTokenName];
} else {
    $csrfToken = bin2hex(random_bytes(32));
    $_SESSION[$csrfTokenName] = $csrfTokenName;
}

$appFileName = 'js/app-es6.js';
$argv = [
    '',
    realpath(__DIR__.'/../../design/vendors'),
    realpath(__DIR__.'/../../client/vendors'),
];

require __DIR__.'/../../bin/compile.php';

$argv = [
    '',
    realpath(__DIR__.'/../../client/vendors'),
    __DIR__.'/'.$appFileName,
    'engine.fileManager.Application',
];

require __DIR__.'/../../bin/link.php';

// prepare app to end user
copy(__DIR__.'/'.$appFileName, __DIR__.'/../../html/engine-fm/'.$appFileName);

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>File Manager</title>
    <link rel="shortcut icon" href="favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="styles/default.css">
</head>
<body>

<div id="file-manager"></div>

<script src="<?= $appFileName; ?>"></script>
<script type="text/javascript">
/*    var callback = function (event, fileManager) {
        fileManager.rightController.on('initialize', function (event, controller) {
            controller.view.activate();
            controller.executeAction('open', {id: '/7777/5555'});
        });
    };

    new engine.fileManager.Application({
        wrapper: document.getElementById('file-manager'),
        configUrl: 'server.php?action=config&',
        csrfToken: 'csrfToken'
        onReady: callback
    });*/

    new engine.fileManager.Application({
        wrapper: document.getElementById('file-manager'),
        configUrl: 'server.php?action=config&',
        csrfToken: 'csrfToken'
    });

</script>
</body>
</html>
