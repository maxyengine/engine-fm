<?php
$appFileName = 'js/app-es6.js';
$argv = [
    '',
    realpath(__DIR__.'/../../design/vendors'),
    realpath(__DIR__.'/../../client/vendors'),
];

require __DIR__.'/../../bin/compile.php';

$argv = [
    '',
    realpath(__DIR__.'/../../client/vendors'),
    __DIR__.'/'.$appFileName,
    'engine.widget.Calendar',
];

require __DIR__.'/../../bin/link.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Calendar</title>
    <link rel="stylesheet" type="text/css" href="styles/default.css">
</head>
<body>
<script src="<?= $appFileName; ?>"></script>
<script type="text/javascript">
    let calendar = new engine.widget.Calendar({
        onSelectDate: event => {
            console.log(event.date);
        }
    });

</script>
</body>
</html>
