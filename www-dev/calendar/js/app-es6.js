var engine = {"lang":{"utility":{},"type":{}},"react":{},"gui":{"utility":{},"component":{}},"widget":{"calendar":{"template":{},"view":{},"action":{}}},"web":{"component":{}}};
{
let self = engine.lang.utility.Object = class {

    static forEach(object, callback) {
        for (let property in object) {
            if (object.hasOwnProperty(property)) {
                callback(property, object[property]);
            }
        }
    }

    static merge(result, ...objects) {
        objects.forEach(object => {
            self.forEach(object, (property, value) => {
                result[property] = value;
            });
        });

        return result;
    }
}
}
{
let self = engine.lang.utility.Type = class {

    static isString(value) {
        return typeof value === 'string';
    }

    static isNumber(value) {
        return typeof value === 'number';
    }

    static isObject(value, strict = true) {
        return typeof value === 'object' && (!strict || !self.isArray(value));
    }

    static isFunction(value) {
        return typeof value === 'function';
    }

    static isArray(value) {
        return value instanceof Array;
    }

    static arrayToObject(value, recursively) {
        let result = {};

        for (let i = 0; i < value.length; ++i) {
            result[i] = recursively && self.isArray(value[i]) ?
                self.arrayToObject(value[i], recursively) :
                value[i];
        }

        return result;
    }
}
}
{
let
    Obj = engine.lang.utility.Object,
    Type = engine.lang.utility.Type;

engine.lang.type.Object = class {

    constructor(properties = {}, context) {
        this._constructor(properties, context);
    }

    _constructor(properties, context) {
        this.set(Obj.merge(this.defaults || {}, properties), context);
        this.use(...this.traits || []);
        this.initialize && this.initialize();
    }

    set(property, value, context) {
        if (Type.isObject(property)) {
            context = value;
            Obj.forEach(property, (name, value) => {
                this.set(name, value, context);
            });
        } else if (Type.isFunction(value) && context) {
            this[property] = (...args) => {
                return value.call(context, ...args);
            };
        } else {
            this[property] = value;
        }

        return this;
    }

    use(...traits) {
        traits.forEach(trait => {
            //todo: check if trait have used already
            if (!Type.isObject(trait)) {
                trait = new trait({owner: this});
            }
            trait.properties && this.set(trait.properties, trait);
        });

        return this;
    }
}
}
{
let self = engine.lang.utility.String = class {

    static capitalize(string) {
        return self.upperCaseFirst(string);
    }

    static format(string, ...args) {
        return string.replace(/{(\d+)}/g, (match, number) => {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    }

    static contains(string, substring, caseSensitive = true) {
        return caseSensitive ?
            string.indexOf(substring) >= 0 :
            string.toLocaleLowerCase().indexOf(substring.toLocaleLowerCase()) >= 0;
    }

    static lowerCaseFirst(string) {
        return string.charAt(0).toLocaleLowerCase() + string.slice(1);
    }

    static upperCaseFirst(string) {
        return string.charAt(0).toLocaleUpperCase() + string.slice(1);
    }
}
}
{
let
    Str = engine.lang.utility.String;

engine.react.Observable = class {

    constructor(...observers) {
        this.observers = [];
        this.addObservers(...observers);
    }

    addObserver(observer, callback) {
        if (callback) {
            let onEvent = 'on' + Str.capitalize(observer);
            observer = {};
            observer[onEvent] = callback;
        }
        this.observers.push(observer);

        return this;
    }

    removeObserver(observer) {
        this.observers = this.observers.filter(element => {
            return element !== observer;
        });

        return this;
    }

    addObservers(...observers) {
        observers.forEach(observer => {
            this.addObserver(observer);
        });

        return this;
    }

    trigger(eventName, ...args) {
        let onEvent = 'on' + Str.capitalize(eventName);
        try {
            this.observers.forEach(observer => {
                observer[onEvent] && observer[onEvent](...args);
            });
            this.observers.forEach(observer => {
                observer[onEvent + 'Complete'] && observer[onEvent + 'Complete'](...args);
            });
        } catch (throwable) {
            this.observers.forEach(observer => {
                // todo: remove temporary solution;
                console.error(throwable);
                observer['onError'] && observer['onError'](throwable, ...args);
            });
        }
    }
}
}
{
let
    Object = engine.lang.type.Object,
    Obj = engine.lang.utility.Object,
    Observable = engine.react.Observable,
    Str = engine.lang.utility.String,
    Type = engine.lang.utility.Type;

let self = engine.react.Component = class extends Object {

    _constructor(...args) {
        super._constructor(...args);
        this.mapEvents(this.events || {});
        this.on(this);
    }

    get eventResource() {
        if (!this._observable) {
            this._observable = new Observable();
        }

        return this._observable;
    }

    on(...args) {
        this.eventResource.addObserver(...args);

        return this;
    }

    trigger(eventName, event, context) {
        this.eventResource.trigger(eventName, event, context || this);
    }

    mapEvent(property, fromEventName, toEventName) {
        if (property instanceof self) {
            property.on(fromEventName, event => {
                this.trigger(toEventName, event, this);
            });
        } else {
            property.addEventListener(fromEventName, event => {
                this.trigger(toEventName, event, this);
            });
        }
    }

    mapEvents(events) {
        Obj.forEach(events, (toEventName, properties) => {
            Obj.forEach(properties, (property, fromEventName) => {
                if (Type.isArray(this[property])) {
                    this[property].forEach(item => {
                        this.mapEvent(item, fromEventName, toEventName);
                    });
                } else {
                    this.mapEvent(this[property], fromEventName, toEventName);
                }
            });
        });
    }

    set(property, value, context) {
        if (property.length > 2 && property.substr(0, 2) === 'on' &&
            property.charAt(2) === property.charAt(2).toUpperCase()
        ) {
            let eventName = Str.lowerCaseFirst(property.substr(2));

            return this.on(eventName, value);
        }

        return super.set(property, value, context);
    }
}
}
{
engine.gui.utility.Element = class {
    
    static clear(element) {
        while (element.hasChildNodes()) {
            element.removeChild(element.firstChild);
        }
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.gui.component.Template = class extends Object {

    initialize() {
        this.owner._element = document.createElement('div');
    }
}
}
{
let
    Component = engine.react.Component,
    Element = engine.gui.utility.Element,
    Template = engine.gui.component.Template;

self = engine.gui.component.View = class extends Component {

    get template() {
        return Template;
    }

    get element() {
        return this._element;
    }

    _constructor(...args) {
        this.use(this.template);
        super._constructor(...args);
        this.data = {};
    }

    clear() {
        Element.clear(this.element);
    }

    appendTo(element) {
        element.appendChild(this.element);
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendar.template.Layout = class extends Object {

    initialize() {
        let element0 = document.createElement('div');element0.setAttribute('class', 'engine-gui-widget-calendar');this.owner._element = element0;let element1 = document.createTextNode('');element0.appendChild(element1);let element2 = document.createElement('div');let element3 = document.createTextNode('');element2.appendChild(element3);let element4 = document.createElement('a');element4.setAttribute('href', '#');this.owner.prev = element4;let element5 = document.createTextNode('<');element4.appendChild(element5);element2.appendChild(element4);let element6 = document.createTextNode(' ');element2.appendChild(element6);let element7 = document.createElement('a');element7.setAttribute('href', '#');this.owner.title = element7;let element8 = document.createTextNode('');element7.appendChild(element8);let element9 = document.createElement('span');let element10 = document.createTextNode('');this.owner.month = element10;element9.appendChild(element10);element7.appendChild(element9);let element11 = document.createTextNode(' ');element7.appendChild(element11);let element12 = document.createElement('span');let element13 = document.createTextNode('');this.owner.year = element13;element12.appendChild(element13);element7.appendChild(element12);let element14 = document.createTextNode('');element7.appendChild(element14);element2.appendChild(element7);let element15 = document.createTextNode(' ');element2.appendChild(element15);let element16 = document.createElement('a');element16.setAttribute('href', '#');this.owner.next = element16;let element17 = document.createTextNode('>');element16.appendChild(element17);element2.appendChild(element16);let element18 = document.createTextNode('');element2.appendChild(element18);element0.appendChild(element2);let element19 = document.createTextNode('');element0.appendChild(element19);let element20 = document.createElement('table');let element21 = document.createTextNode('');element20.appendChild(element21);let element22 = document.createElement('thead');let element23 = document.createTextNode('');element22.appendChild(element23);let element24 = document.createElement('tr');this.owner.days = element24;element22.appendChild(element24);let element25 = document.createTextNode('');element22.appendChild(element25);element20.appendChild(element22);let element26 = document.createTextNode('');element20.appendChild(element26);let element27 = document.createElement('tbody');this.owner.body = element27;element20.appendChild(element27);let element28 = document.createTextNode('');element20.appendChild(element28);element0.appendChild(element20);let element29 = document.createTextNode('');element0.appendChild(element29);
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendar.template.Day = class extends Object {

    initialize() {
        let element0 = document.createElement('th');this.owner._element = element0;let element1 = document.createTextNode('');this.owner._text = element1;element0.appendChild(element1);
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendar.template.Day;

engine.widget.calendar.view.Day = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    set value(value) {
        this._text.nodeValue = value;
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendar.template.Row = class extends Object {

    initialize() {
        let element0 = document.createElement('tr');this.owner._element = element0;
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendar.template.Row;

engine.widget.calendar.view.Week = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    appendDate(date) {
        this.element.appendChild(date.render());

        return date;
    }
}
}
{
let self = engine.gui.utility.ClassName = class {

    static has(element, className) {
        return !!element.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
    }

    static add(element, className) {
        if (!self.has(element, className)) {
            element.className += element.className ? ' ' + className : className;
        }
    }

    static set(element, className) {
        element.className = className;
    }

    static remove(element, className) {
        if (self.has(element, className)) {
            element.className = element.className.replace(new RegExp('(\\s|^)' + className + '(\\s|$)'), '');
        }
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendar.template.Date = class extends Object {

    initialize() {
        let element0 = document.createElement('td');this.owner._element = element0;let element1 = document.createTextNode('');this.owner._text = element1;element0.appendChild(element1);
    }
}
}
{
let
    View = engine.gui.component.View,
    ClassName = engine.gui.utility.ClassName,
    Template = engine.widget.calendar.template.Date;

engine.widget.calendar.view.Date = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            pick: {element: 'click'}
        };
    }

    set date(value) {
        this._value = value;
        this._text.nodeValue = value.getDate();
    }

    get date() {
        return this._value;
    }

    set isSelected(value) {
        this._isSelected = value;
        if (value) {
            ClassName.add(this.element, 'selected');
        }
    }

    get isSelected() {
        return this._isSelected;
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendar.template.Row;

engine.widget.calendar.view.Quarter = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    appendMonth(month) {
        this.element.appendChild(month.render());

        return month;
    }
}
}
{
let
    View = engine.gui.component.View,
    ClassName = engine.gui.utility.ClassName,
    Template = engine.widget.calendar.template.Date;

engine.widget.calendar.view.Month = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            pick: {element: 'click'}
        };
    }

    set date(value) {
        this._value = value;
        this._text.nodeValue = this.months[value.getMonth()].short;
    }

    get date() {
        return this._value;
    }

    set isSelected(value) {
        this._isSelected = value;
        if (value) {
            ClassName.add(this.element, 'selected');
        }
    }

    get isSelected() {
        return this._isSelected;
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendar.template.Row;

engine.widget.calendar.view.YearRow = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    appendYear(years) {
        this.element.appendChild(years.render());

        return years;
    }
}
}
{
let
    View = engine.gui.component.View,
    ClassName = engine.gui.utility.ClassName,
    Template = engine.widget.calendar.template.Date;

engine.widget.calendar.view.Year = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            pick: {element: 'click'}
        };
    }

    set date(value) {
        this._value = value;
        this._text.nodeValue = value.getFullYear();
    }

    get date() {
        return this._value;
    }

    set isSelected(value) {
        this._isSelected = value;
        if (value) {
            ClassName.add(this.element, 'selected');
        }
    }

    get isSelected() {
        return this._isSelected;
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendar.template.Layout,
    Day = engine.widget.calendar.view.Day,
    Week = engine.widget.calendar.view.Week,
    DateView = engine.widget.calendar.view.Date,
    Quarter = engine.widget.calendar.view.Quarter,
    Month = engine.widget.calendar.view.Month,
    YearRow = engine.widget.calendar.view.YearRow,
    Year = engine.widget.calendar.view.Year,
    Element = engine.gui.utility.Element;

engine.widget.calendar.view.Layout = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            prev: {prev: 'click'},
            next: {next: 'click'},
            switchMode: {title: 'click'}
        };
    }

    renderTitleMonth(value) {
        this.month.nodeValue = value;

        return this;
    }

    renderTitleYear(year1, year2) {
        this.year.nodeValue = year1;
        this.year.nodeValue += year2 ? ' - ' + year2 : '';

        return this;
    }

    renderDays(days) {
        Element.clear(this.days);

        days.forEach(day => {
            this.appendDay(new Day({
                value: day.short
            }));
        });

        return this;
    }

    renderMonth(weeks, activeDate) {
        Element.clear(this.body);

        weeks.forEach(dates => {
            let week = this.appendRow(new Week());
            dates.forEach(date => {
                week.appendDate(new DateView({
                    date: date,
                    isSelected: date.getTime() === activeDate.getTime(),
                    onPick: (event, view) => {
                        this.trigger('selectDate', {
                            date: view.date
                        });
                    }
                }));
            });
        });

        return this;
    }

    renderYear(quarters, activeDate, months) {
        Element.clear(this.body);

        quarters.forEach(quarter => {
            let row = this.appendRow(new Quarter());
            quarter.forEach(date => {
                row.appendMonth(new Month({
                    months: months,
                    date: date,
                    isSelected: date.getMonth() === activeDate.getMonth(),
                    onPick: (event, view) => {
                        this.trigger('selectMonth', {
                            date: view.date
                        });
                    }
                }));
            });
        });

        return this;
    }

    renderDecade(quarters, activeDate) {
        Element.clear(this.body);

        quarters.forEach(quarter => {
            let row = this.appendRow(new YearRow());
            quarter.forEach(date => {
                row.appendYear(new Year({
                    date: date,
                    isSelected: date.getFullYear() === activeDate.getFullYear(),
                    onPick: (event, view) => {
                        this.trigger('selectYear', {
                            date: view.date
                        });
                    }
                }));
            });
        });

        return this;
    }

    appendDay(day) {
        this.days.appendChild(day.render());

        return day;
    }

    appendRow(row) {
        this.body.appendChild(row.render());

        return row;
    }
}
}
{
let
    Component = engine.react.Component,
    Obj = engine.lang.utility.Object,
    Type = engine.lang.utility.Type;

engine.web.component.Controller = class extends Component {

    _constructor(...args) {
        super._constructor(...args);
        this.actionMap = this.actions || {};
        this.subscribeActions(this.actionMap);
    }

    subscribeActions(actions) {
        Obj.forEach(actions, (eventName, action) => {
            this.subscribeAction(eventName, action);
        });
    }

    subscribeAction(eventName, action) {
        this.actionMap[eventName] = action;
        this.on(eventName, (...args) => {
            this.executeAction(eventName, ...args);
        });
    }

    getAction(eventName) {
        let action = this.actionMap[eventName];
        if (Type.isObject(action)) {
            return action;
        }
        action = new action(this.properties || {}, this);
        this.actionMap[eventName] = action;

        return action;
    }

    executeAction(eventName, ...args) {
        if (!this.isActionAllowed(eventName)) {
            return;
        }
        try {
            this.getAction(eventName).execute(...args);
        } catch (throwable) {
            this.handleActionError(throwable);
        }
    }

    isActionAllowed(eventName) {
        return true;
    }

    handleActionError(throwable) {
        throw throwable;
    }

    render(data) {
        this.view.set(data);
    }
}
}
{
let
    Object = engine.lang.type.Object,
    Str = engine.lang.utility.String;

engine.widget.calendar.action.SetDate = class extends Object {

    execute(event) {
        let method = 'render' + Str.capitalize(this.getMode());
        this[method](event.date);
    }

    renderMonth(date) {
        this.view
            .renderTitleMonth(this.months[date.getMonth()].full)
            .renderTitleYear(date.getFullYear())
            .renderDays(this.days)
            .renderMonth(this.model.getMonth(date, this.fromMonday), date);
    }

    renderYear(date) {
        this.view
            .renderTitleMonth('')
            .renderTitleYear(date.getFullYear())
            .renderDays([])
            .renderYear(this.model.getYear(date), date, this.months);
    }

    renderDecade(date) {
        let decade = this.model.getDecade(date);
        this.view
            .renderTitleMonth('')
            .renderTitleYear(decade[0][1].getFullYear(), decade[3][1].getFullYear())
            .renderDays([])
            .renderDecade(decade, date);
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.SelectDate = class extends Object {

    execute(event) {
        this.setDate(event.date);
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.SelectMonth = class extends Object {

    execute(event) {
        this.setMode('month');
        this.setDate(event.date);
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.SelectYear = class extends Object {

    execute(event) {
        this.setMode('year');
        this.setDate(event.date);
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.Prev = class extends Object {

    execute() {
        switch (this.getMode()) {
            case 'month':
                this.setDate(this.model.monthAgo(this.getDate()));
                break;
            case 'year':
                this.setDate(this.model.yearAgo(this.getDate()));
                break;
            case 'decade':
                this.setDate(this.model.decadeAgo(this.getDate()));
        }
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.Next = class extends Object {

    execute() {
        switch (this.getMode()) {
            case 'month':
                this.setDate(this.model.monthAhead(this.getDate()));
                break;
            case 'year':
                this.setDate(this.model.yearAhead(this.getDate()));
                break;
            case 'decade':
                this.setDate(this.model.decadeAhead(this.getDate()));
        }
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendar.action.SwitchMode = class extends Object {

    execute() {
        switch (this.getMode()) {
            case 'month':
                this.setMode('year');
                break;
            case 'year':
                this.setMode('decade');
                break;
        }
    }
}
}
{
let
    WebController = engine.web.component.Controller,
    SetDate = engine.widget.calendar.action.SetDate,
    SelectDate = engine.widget.calendar.action.SelectDate,
    SelectMonth = engine.widget.calendar.action.SelectMonth,
    SelectYear = engine.widget.calendar.action.SelectYear,
    Prev = engine.widget.calendar.action.Prev,
    Next = engine.widget.calendar.action.Next,
    SwitchMode = engine.widget.calendar.action.SwitchMode;

engine.widget.calendar.Controller = class extends WebController {

    get properties() {
        return [
            'model',
            'view',
            'days',
            'months',
            'fromMonday'
        ];
    }

    get methods() {
        return [
            'getDate',
            'setDate',
            'selectDate',
            'getMode',
            'setMode',
        ];
    }

    get actions() {
        return {
            setDate: SetDate,
            selectDate: SelectDate,
            selectMonth: SelectMonth,
            selectYear: SelectYear,
            switchMode: SwitchMode,
            prev: Prev,
            next: Next
        };
    }

    get events() {
        return {
            setDate: {view: 'setDate'},
            selectDate: {view: 'selectDate'},
            selectMonth: {view: 'selectMonth'},
            selectYear: {view: 'selectYear'},
            switchMode: {view: 'switchMode'},
            prev: {view: 'prev'},
            next: {view: 'next'}
        };
    }

    getDate() {
        return this.date;
    }

    setDate(value) {
        this.date = value;
        this.trigger('setDate', {
            date: value
        });
    }

    selectDate(value) {
        this.trigger('selectDate', {
            date: value
        });
    }

    getMode() {
        return this.mode;
    }

    setMode(value) {
        this.mode = value;
        this.setDate(this.getDate());
    }
}
}
{
engine.widget.calendar.Model = class {

    getMonth(date, fromMonday) {
        date = new Date(date.getTime());
        let
            firstInMonth = new Date(date.getFullYear(), date.getMonth(), 1),
            delta = fromMonday ? (firstInMonth.getDay() === 0 ? 7 : firstInMonth.getDay()) : 1,
            i = 0,
            j = 0,
            weeks = [];
        date.setDate(firstInMonth.getDate() - delta);
        while (6 > i++) {
            let week = [];
            while (7 > j++) {
                date.setDate(date.getDate() + 1);
                week.push(new Date(date.getTime()));
            }
            weeks.push(week);
            j = 0;
        }

        return weeks;
    }

    getYear(date) {
        date = new Date(date.getTime());
        date.setMonth(0);
        let
            quarters = [],
            i = 0,
            j = 0;
        while (4 > i++) {
            let quarter = [];
            while (3 > j++) {
                quarter.push(date);
                date = this.monthAhead(date);
            }
            quarters.push(quarter);
            j = 0;
        }

        return quarters;
    }

    getDecade(date) {
        date = new Date(date.getTime());
        date.setFullYear(Math.floor(date.getFullYear() / 10) * 10 - 1);
        let
            quarters = [],
            i = 0,
            j = 0;
        while (4 > i++) {
            let quarter = [];
            while (3 > j++) {
                quarter.push(new Date(date.getTime()));
                date.setFullYear(date.getFullYear() + 1);
            }
            quarters.push(quarter);
            j = 0;
        }

        return quarters;
    }

    monthAgo(date) {
        let month = date.getMonth();
        date = new Date(date.getTime());
        date.setMonth(date.getMonth() - 1);
        if (date.getMonth() === month) {
            date.setDate(0);
        }

        return date;
    }

    monthAhead(date) {
        let month = date.getMonth();
        date = new Date(date.getTime());
        date.setMonth(date.getMonth() + 1);
        if (date.getMonth() === month + 2) {
            date.setDate(0);
        }

        return date;
    }

    yearAgo(date) {
        date = new Date(date.getTime());
        date.setFullYear(date.getFullYear() - 1);

        return date;
    }

    yearAhead(date) {
        date = new Date(date.getTime());
        date.setFullYear(date.getFullYear() + 1);

        return date;
    }

    decadeAgo(date) {
        date = new Date(date.getTime());
        date.setFullYear(date.getFullYear() - 10);

        return date;
    }

    decadeAhead(date) {
        date = new Date(date.getTime());
        date.setFullYear(date.getFullYear() + 10);

        return date;
    }
}
}
{
let
    Component = engine.react.Component,
    Layout = engine.widget.calendar.view.Layout,
    Controller = engine.widget.calendar.Controller,
    Model = engine.widget.calendar.Model;

engine.widget.Calendar = class extends Component {

    constructor(...args) {
        super(...args);
        this.setDate(new Date());
    }

    initialize() {
        this.wrapper = this.wrapper || document.body;
        this.fromMonday = this.fromMonday !== undefined ? this.fromMonday : true;
        this.mode = this.mode || 'month';
        this.days = this.days || [
            {full: 'Monday', short: 'Mo'},
            {full: 'Tuesday', short: 'Tu'},
            {full: 'Wednesday', short: 'We'},
            {full: 'Thursday', short: 'Th'},
            {full: 'Friday', short: 'Fr'},
            {full: 'Saturday', short: 'St'},
            {full: 'Sunday', short: 'Su'},
        ];
        this.months = this.months || [
            {full: 'January', short: 'Jan'},
            {full: 'February', short: 'Feb'},
            {full: 'March', short: 'Mar'},
            {full: 'April', short: 'Apr'},
            {full: 'May', short: 'May'},
            {full: 'June', short: 'Jun'},
            {full: 'Jule', short: 'Jul'},
            {full: 'August', short: 'Aug'},
            {full: 'September', short: 'Sep'},
            {full: 'October', short: 'Oct'},
            {full: 'November', short: 'Nov'},
            {full: 'December', short: 'Dec'}
        ];
    }

    get events() {
        return {
            selectDate: {controller: 'selectDate'}
        };
    }

    get layout() {
        if (!this._layout) {
            this._layout = new Layout();
            this.wrapper.appendChild(this._layout.element);
        }

        return this._layout;
    }

    get model() {
        if (!this._model) {
            this._model = new Model();
        }

        return this._model;
    }

    get controller() {
        if (!this._controller) {
            this._controller = new Controller({
                model: this.model,
                view: this.layout,
                days: this.days,
                months: this.months,
                fromMonday: this.fromMonday,
                mode: this.mode
            });
        }

        return this._controller;
    }

    getDate() {
        return this.controller.getDate();
    }

    setDate(value) {
        this.controller.setDate(value);

        return this;
    }

    selectDate(value) {
        this.controller.selectDate(value);

        return this;
    }
}
}
