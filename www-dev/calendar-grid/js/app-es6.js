var engine = {"lang":{"utility":{},"type":{}},"react":{},"gui":{"utility":{},"component":{}},"widget":{"calendarGrid":{"template":{},"view":{}}},"web":{"component":{}}};
{
let self = engine.lang.utility.Object = class {

    static forEach(object, callback) {
        for (let property in object) {
            if (object.hasOwnProperty(property)) {
                callback(property, object[property]);
            }
        }
    }

    static merge(result, ...objects) {
        objects.forEach(object => {
            self.forEach(object, (property, value) => {
                result[property] = value;
            });
        });

        return result;
    }
}
}
{
let self = engine.lang.utility.Type = class {

    static isString(value) {
        return typeof value === 'string';
    }

    static isNumber(value) {
        return typeof value === 'number';
    }

    static isObject(value, strict = true) {
        return typeof value === 'object' && (!strict || !self.isArray(value));
    }

    static isFunction(value) {
        return typeof value === 'function';
    }

    static isArray(value) {
        return value instanceof Array;
    }

    static arrayToObject(value, recursively) {
        let result = {};

        for (let i = 0; i < value.length; ++i) {
            result[i] = recursively && self.isArray(value[i]) ?
                self.arrayToObject(value[i], recursively) :
                value[i];
        }

        return result;
    }
}
}
{
let
    Obj = engine.lang.utility.Object,
    Type = engine.lang.utility.Type;

engine.lang.type.Object = class {

    constructor(properties = {}, context) {
        this._constructor(properties, context);
    }

    _constructor(properties, context) {
        this.set(Obj.merge(this.defaults || {}, properties), context);
        this.use(...this.traits || []);
        this.initialize && this.initialize();
    }

    set(property, value, context) {
        if (Type.isObject(property)) {
            context = value;
            Obj.forEach(property, (name, value) => {
                this.set(name, value, context);
            });
        } else if (Type.isFunction(value) && context) {
            this[property] = (...args) => {
                return value.call(context, ...args);
            };
        } else {
            this[property] = value;
        }

        return this;
    }

    use(...traits) {
        traits.forEach(trait => {
            //todo: check if trait have used already
            if (!Type.isObject(trait)) {
                trait = new trait({owner: this});
            }
            trait.properties && this.set(trait.properties, trait);
        });

        return this;
    }
}
}
{
let self = engine.lang.utility.String = class {

    static capitalize(string) {
        return self.upperCaseFirst(string);
    }

    static format(string, ...args) {
        return string.replace(/{(\d+)}/g, (match, number) => {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    }

    static contains(string, substring, caseSensitive = true) {
        return caseSensitive ?
            string.indexOf(substring) >= 0 :
            string.toLocaleLowerCase().indexOf(substring.toLocaleLowerCase()) >= 0;
    }

    static lowerCaseFirst(string) {
        return string.charAt(0).toLocaleLowerCase() + string.slice(1);
    }

    static upperCaseFirst(string) {
        return string.charAt(0).toLocaleUpperCase() + string.slice(1);
    }
}
}
{
let
    Str = engine.lang.utility.String;

engine.react.Observable = class {

    constructor(...observers) {
        this.observers = [];
        this.addObservers(...observers);
    }

    addObserver(observer, callback) {
        if (callback) {
            let onEvent = 'on' + Str.capitalize(observer);
            observer = {};
            observer[onEvent] = callback;
        }
        this.observers.push(observer);

        return this;
    }

    removeObserver(observer) {
        this.observers = this.observers.filter(element => {
            return element !== observer;
        });

        return this;
    }

    addObservers(...observers) {
        observers.forEach(observer => {
            this.addObserver(observer);
        });

        return this;
    }

    trigger(eventName, ...args) {
        let onEvent = 'on' + Str.capitalize(eventName);
        try {
            this.observers.forEach(observer => {
                observer[onEvent] && observer[onEvent](...args);
            });
            this.observers.forEach(observer => {
                observer[onEvent + 'Complete'] && observer[onEvent + 'Complete'](...args);
            });
        } catch (throwable) {
            this.observers.forEach(observer => {
                // todo: remove temporary solution;
                console.error(throwable);
                observer['onError'] && observer['onError'](throwable, ...args);
            });
        }
    }
}
}
{
let
    Object = engine.lang.type.Object,
    Obj = engine.lang.utility.Object,
    Observable = engine.react.Observable,
    Str = engine.lang.utility.String,
    Type = engine.lang.utility.Type;

let self = engine.react.Component = class extends Object {

    _constructor(...args) {
        super._constructor(...args);
        this.mapEvents(this.events || {});
        this.on(this);
    }

    get eventResource() {
        if (!this._observable) {
            this._observable = new Observable();
        }

        return this._observable;
    }

    on(...args) {
        this.eventResource.addObserver(...args);

        return this;
    }

    trigger(eventName, event, context) {
        this.eventResource.trigger(eventName, event, context || this);
    }

    mapEvent(property, fromEventName, toEventName) {
        if (property instanceof self) {
            property.on(fromEventName, event => {
                this.trigger(toEventName, event, this);
            });
        } else {
            property.addEventListener(fromEventName, event => {
                this.trigger(toEventName, event, this);
            });
        }
    }

    mapEvents(events) {
        Obj.forEach(events, (toEventName, properties) => {
            Obj.forEach(properties, (property, fromEventName) => {
                if (Type.isArray(this[property])) {
                    this[property].forEach(item => {
                        this.mapEvent(item, fromEventName, toEventName);
                    });
                } else {
                    this.mapEvent(this[property], fromEventName, toEventName);
                }
            });
        });
    }

    set(property, value, context) {
        if (property.length > 2 && property.substr(0, 2) === 'on' &&
            property.charAt(2) === property.charAt(2).toUpperCase()
        ) {
            let eventName = Str.lowerCaseFirst(property.substr(2));

            return this.on(eventName, value);
        }

        return super.set(property, value, context);
    }
}
}
{
engine.gui.utility.Element = class {
    
    static clear(element) {
        while (element.hasChildNodes()) {
            element.removeChild(element.firstChild);
        }
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.gui.component.Template = class extends Object {

    initialize() {
        this.owner._element = document.createElement('div');
    }
}
}
{
let
    Component = engine.react.Component,
    Element = engine.gui.utility.Element,
    Template = engine.gui.component.Template;

self = engine.gui.component.View = class extends Component {

    get template() {
        return Template;
    }

    get element() {
        return this._element;
    }

    _constructor(...args) {
        this.use(this.template);
        super._constructor(...args);
        this.data = {};
    }

    clear() {
        Element.clear(this.element);
    }

    appendTo(element) {
        element.appendChild(this.element);
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.Layout = class extends Object {

    initialize() {
        let element0 = document.createElement('div');element0.setAttribute('class', 'engine-gui-widget-calendarGrid content');this.owner._element = element0;
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.ControlPanel = class extends Object {

    initialize() {
        let element0 = document.createElement('div');element0.setAttribute('class', 'controllpanel');this.owner._element = element0;let element1 = document.createTextNode('');element0.appendChild(element1);let element2 = document.createElement('div');element2.setAttribute('class', 'controllpanel__inner');let element3 = document.createTextNode('');element2.appendChild(element3);let element4 = document.createElement('div');let element5 = document.createTextNode('');element4.appendChild(element5);let element6 = document.createElement('h3');let element7 = document.createTextNode('Showing');element6.appendChild(element7);element4.appendChild(element6);let element8 = document.createTextNode('');element4.appendChild(element8);let element9 = document.createElement('div');element9.setAttribute('class', 'controllpanel__option');let element10 = document.createTextNode('');element9.appendChild(element10);let element11 = document.createElement('label');let element12 = document.createTextNode('');element11.appendChild(element12);let element13 = document.createElement('div');element11.appendChild(element13);let element14 = document.createTextNode('Year');element11.appendChild(element14);let element15 = document.createElement('input');element15.setAttribute('type', 'text');this.owner._year = element15;element11.appendChild(element15);let element16 = document.createTextNode('');element11.appendChild(element16);let element17 = document.createElement('button');this.owner.applyYear = element17;let element18 = document.createTextNode('Apply Year');element17.appendChild(element18);element11.appendChild(element17);let element19 = document.createTextNode('');element11.appendChild(element19);element9.appendChild(element11);let element20 = document.createTextNode('');element9.appendChild(element20);element4.appendChild(element9);let element21 = document.createTextNode('');element4.appendChild(element21);let element22 = document.createElement('div');element22.setAttribute('class', 'controllpanel__option');let element23 = document.createTextNode('');element22.appendChild(element23);let element24 = document.createElement('label');let element25 = document.createTextNode('');element24.appendChild(element25);let element26 = document.createElement('input');element26.setAttribute('type', 'checkbox');this.owner._fromMonday = element26;element24.appendChild(element26);let element27 = document.createTextNode('');element24.appendChild(element27);let element28 = document.createElement('div');element24.appendChild(element28);let element29 = document.createTextNode('Start from monday');element24.appendChild(element29);element22.appendChild(element24);let element30 = document.createTextNode('');element22.appendChild(element30);element4.appendChild(element22);let element31 = document.createTextNode('');element4.appendChild(element31);let element32 = document.createElement('div');element32.setAttribute('class', 'controllpanel__option');let element33 = document.createTextNode('');element32.appendChild(element33);let element34 = document.createElement('label');let element35 = document.createTextNode('');element34.appendChild(element35);let element36 = document.createElement('input');element36.setAttribute('type', 'checkbox');this.owner._showWeekDays = element36;element34.appendChild(element36);let element37 = document.createTextNode('');element34.appendChild(element37);let element38 = document.createElement('div');element34.appendChild(element38);let element39 = document.createTextNode('Show days of the week');element34.appendChild(element39);element32.appendChild(element34);let element40 = document.createTextNode('');element32.appendChild(element40);element4.appendChild(element32);let element41 = document.createTextNode('');element4.appendChild(element41);let element42 = document.createElement('div');element42.setAttribute('class', 'controllpanel__option');let element43 = document.createTextNode('');element42.appendChild(element43);let element44 = document.createElement('label');let element45 = document.createTextNode('');element44.appendChild(element45);let element46 = document.createElement('input');element46.setAttribute('type', 'checkbox');this.owner._highlightWeekends = element46;element44.appendChild(element46);let element47 = document.createTextNode('');element44.appendChild(element47);let element48 = document.createElement('div');element44.appendChild(element48);let element49 = document.createTextNode('Highlight the weekend');element44.appendChild(element49);element42.appendChild(element44);let element50 = document.createTextNode('');element42.appendChild(element50);element4.appendChild(element42);let element51 = document.createTextNode('');element4.appendChild(element51);let element52 = document.createElement('div');element52.setAttribute('class', 'controllpanel__option');let element53 = document.createTextNode('');element52.appendChild(element53);let element54 = document.createElement('label');let element55 = document.createTextNode('');element54.appendChild(element55);let element56 = document.createElement('input');element56.setAttribute('type', 'checkbox');this.owner._showDayOut = element56;element54.appendChild(element56);let element57 = document.createTextNode('');element54.appendChild(element57);let element58 = document.createElement('div');element54.appendChild(element58);let element59 = document.createTextNode('Show out days in months');element54.appendChild(element59);element52.appendChild(element54);let element60 = document.createTextNode('');element52.appendChild(element60);element4.appendChild(element52);let element61 = document.createTextNode('');element4.appendChild(element61);let element62 = document.createElement('div');element62.setAttribute('class', 'controllpanel__option');let element63 = document.createTextNode('');element62.appendChild(element63);let element64 = document.createElement('label');let element65 = document.createTextNode('');element64.appendChild(element65);let element66 = document.createElement('input');element66.setAttribute('type', 'checkbox');this.owner._shortWeekDay = element66;element64.appendChild(element66);let element67 = document.createTextNode('');element64.appendChild(element67);let element68 = document.createElement('div');element64.appendChild(element68);let element69 = document.createTextNode('Short name of the week days');element64.appendChild(element69);element62.appendChild(element64);let element70 = document.createTextNode('');element62.appendChild(element70);element4.appendChild(element62);let element71 = document.createTextNode('');element4.appendChild(element71);let element72 = document.createElement('div');element72.setAttribute('class', 'controllpanel__option');let element73 = document.createTextNode('');element72.appendChild(element73);let element74 = document.createElement('label');let element75 = document.createTextNode('');element74.appendChild(element75);let element76 = document.createElement('input');element76.setAttribute('type', 'checkbox');this.owner._shortMonth = element76;element74.appendChild(element76);let element77 = document.createTextNode('');element74.appendChild(element77);let element78 = document.createElement('div');element74.appendChild(element78);let element79 = document.createTextNode('Short name of the months');element74.appendChild(element79);element72.appendChild(element74);let element80 = document.createTextNode('');element72.appendChild(element80);element4.appendChild(element72);let element81 = document.createTextNode('');element4.appendChild(element81);let element82 = document.createElement('h3');let element83 = document.createTextNode('Layout');element82.appendChild(element83);element4.appendChild(element82);let element84 = document.createTextNode('');element4.appendChild(element84);let element85 = document.createElement('div');element85.setAttribute('class', 'controllpanel__option');this.owner.showWeekDaysInTheRowWrapper = element85;let element86 = document.createTextNode('Arrange Days of the Week:');element85.appendChild(element86);element4.appendChild(element85);let element87 = document.createTextNode('');element4.appendChild(element87);let element88 = document.createElement('div');element88.setAttribute('class', 'controllpanel__option');this.owner.showMonthsInTheRowWrapper = element88;let element89 = document.createTextNode('Arrange Months:');element88.appendChild(element89);element4.appendChild(element88);let element90 = document.createTextNode('');element4.appendChild(element90);let element91 = document.createElement('div');element91.setAttribute('class', 'controllpanel__option');this.owner.monthsPerColumnWrapper = element91;let element92 = document.createTextNode('Months per column');element91.appendChild(element92);element4.appendChild(element91);let element93 = document.createTextNode('');element4.appendChild(element93);let element94 = document.createElement('div');element94.setAttribute('class', 'controllpanel__option');this.owner.monthsPerRowWrapper = element94;let element95 = document.createTextNode('Months per row');element94.appendChild(element95);element4.appendChild(element94);let element96 = document.createTextNode('');element4.appendChild(element96);let element97 = document.createElement('h3');let element98 = document.createTextNode('Direction');element97.appendChild(element98);element4.appendChild(element97);let element99 = document.createTextNode('');element4.appendChild(element99);let element100 = document.createElement('div');element100.setAttribute('class', 'controllpanel__option');this.owner.reverseDirectionWrapper = element100;element4.appendChild(element100);let element101 = document.createTextNode('');element4.appendChild(element101);element2.appendChild(element4);let element102 = document.createTextNode('');element2.appendChild(element102);element0.appendChild(element2);let element103 = document.createTextNode('');element0.appendChild(element103);
    }
}
}
{
let self = engine.gui.utility.ClassName = class {

    static has(element, className) {
        return !!element.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
    }

    static add(element, className) {
        if (!self.has(element, className)) {
            element.className += element.className ? ' ' + className : className;
        }
    }

    static set(element, className) {
        element.className = className;
    }

    static remove(element, className) {
        if (self.has(element, className)) {
            element.className = element.className.replace(new RegExp('(\\s|^)' + className + '(\\s|$)'), '');
        }
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.Switch = class extends Object {

    initialize() {
        let element0 = document.createElement('div');element0.setAttribute('class', 'btn-group');this.owner._element = element0;let element1 = document.createTextNode('');element0.appendChild(element1);let element2 = document.createElement('button');element2.setAttribute('type', 'button');element2.setAttribute('class', 'btn');this.owner._turnOff = element2;let element3 = document.createTextNode('');this.owner._captionOff = element3;element2.appendChild(element3);element0.appendChild(element2);let element4 = document.createTextNode('');element0.appendChild(element4);let element5 = document.createElement('button');element5.setAttribute('type', 'button');element5.setAttribute('class', 'btn');this.owner._turnOn = element5;let element6 = document.createTextNode('');this.owner._captionOn = element6;element5.appendChild(element6);element0.appendChild(element5);let element7 = document.createTextNode('');element0.appendChild(element7);
    }
}
}
{
let
    View = engine.gui.component.View,
    ClassName = engine.gui.utility.ClassName,
    Template = engine.widget.calendarGrid.template.Switch;

engine.widget.calendarGrid.view.Switch = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            turnOff: {_turnOff: 'click'},
            turnOn: {_turnOn: 'click'},
        };
    }

    get isOff() {
        return ClassName.has(this._turnOff, 'active');
    }

    get isOn() {
        return ClassName.has(this._turnOn, 'active');
    }

    set captionOff(value) {
        this._captionOff.nodeValue = value;
    }

    set captionOn(value) {
        this._captionOn.nodeValue = value;
    }

    turnOff() {
        ClassName.add(this._turnOff, 'active');
        ClassName.remove(this._turnOn, 'active');
    }

    turnOn() {
        ClassName.add(this._turnOn, 'active');
        ClassName.remove(this._turnOff, 'active');
    }

    onTurnOff() {
        if (this.isOn) {
            this.turnOff();
            this.trigger('change');
        }
    }

    onTurnOn() {
        if (this.isOff) {
            this.turnOn();
            this.trigger('change');
        }
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.Range = class extends Object {

    initialize() {
        let element0 = document.createElement('div');element0.setAttribute('class', 'btn-group');this.owner._element = element0;
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.RangeButton = class extends Object {

    initialize() {
        let element0 = document.createElement('button');element0.setAttribute('type', 'button');element0.setAttribute('class', 'btn');this.owner._element = element0;let element1 = document.createTextNode('');this.owner._value = element1;element0.appendChild(element1);
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.RangeButton,
    ClassName = engine.gui.utility.ClassName;

engine.widget.calendarGrid.view.RangeButton = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            select: {element: 'click'}
        };
    }

    set value(value) {
        this._value.nodeValue = value;
    }

    get value() {
        return parseInt(this._value.nodeValue);
    }

    activate() {
        ClassName.add(this.element, 'active');
    }

    inactivate() {
        ClassName.remove(this.element, 'active');
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Range,
    RangeButton = engine.widget.calendarGrid.view.RangeButton;

engine.widget.calendarGrid.view.Range = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    set value(value) {
        this._value = value;
        this.buttons.forEach(button => {
            this.value === button.value ? button.activate() : button.inactivate();
        });
    }

    get value() {
        return this._value;
    }

    set values(values) {
        this._values = values;
    }

    get values() {
        return this._values;
    }

    render() {
        this.clear();
        this.buttons = [];

        this.values.forEach(value => {
            this.buttons.push(
                this.appendButton(
                    new RangeButton({
                        value: value,
                        onSelect: () => {
                            this.value = value;
                            this.trigger('change');
                        }
                    })
                )
            );
        });

        return this.element;
    }

    appendButton(button) {
        this.element.appendChild(button.render());

        return button;
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.ControlPanel,
    Switch = engine.widget.calendarGrid.view.Switch,
    Range = engine.widget.calendarGrid.view.Range,
    ClassName = engine.gui.utility.ClassName;

engine.widget.calendarGrid.view.ControlPanel = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            changeConfig: {
                _fromMonday: 'change',
                _showWeekDays: 'change',
                _highlightWeekends: 'change',
                _showDayOut: 'change',
                _shortWeekDay: 'change',
                _shortMonth: 'change',
                _showWeekDaysInTheRow: 'change',
                _reverseDirection: 'change',
                _showMonthsInTheRow: 'change',
                _monthsPerRow: 'change',
                _monthsPerColumn: 'change',
                applyYear: 'click'
            }
        }
    }

    get config() {
        return {
            year: this.year,
            fromMonday: this.fromMonday,
            highlightWeekends: this.highlightWeekends,
            reverseDirection: this.reverseDirection,
            shortMonth: this.shortMonth,
            showWeekDays: this.showWeekDays,
            showDayOut: this.showDayOut,
            shortWeekDay: this.shortWeekDay,
            showWeekDaysInTheRow: this.showWeekDaysInTheRow,
            showMonthsInTheRow: this.showMonthsInTheRow,
            monthsPerRow: this.monthsPerRow,
            monthsPerColumn: this.monthsPerColumn
        }
    }

    get fromMonday() {
        return this._fromMonday.checked;
    }

    set fromMonday(value) {
        this._fromMonday.checked = value;
    }

    get showWeekDays() {
        return this._showWeekDays.checked;
    }

    set showWeekDays(value) {
        this._showWeekDays.checked = value;
    }

    get shortMonth() {
        return this._shortMonth.checked;
    }

    set shortMonth(value) {
        this._shortMonth.checked = value;
    }

    get highlightWeekends() {
        return this._highlightWeekends.checked;
    }

    set highlightWeekends(value) {
        this._highlightWeekends.checked = value;
    }

    get showDayOut() {
        return this._showDayOut.checked;
    }

    set showDayOut(value) {
        this._showDayOut.checked = value;
    }

    get shortWeekDay() {
        return this._shortWeekDay.checked;
    }

    set shortWeekDay(value) {
        this._shortWeekDay.checked = value;
    }

    get showWeekDaysInTheRow() {
        return this._showWeekDaysInTheRow.isOn;
    }

    set showWeekDaysInTheRow(value) {
        value ? this._showWeekDaysInTheRow.turnOn() : this._showWeekDaysInTheRow.turnOff();
    }

    get reverseDirection() {
        return this._reverseDirection.isOn;
    }

    set reverseDirection(value) {
        value ? this._reverseDirection.turnOn() : this._reverseDirection.turnOff();
    }

    get showMonthsInTheRow() {
        return this._showMonthsInTheRow.isOn;
    }

    set showMonthsInTheRow(value) {
        if (value) {
            this._showMonthsInTheRow.turnOn();
            this.activeMonthsPerRow();
        } else {
            this._showMonthsInTheRow.turnOff();
            this.activeMonthsPerColumn();
        }
    }

    get monthsPerColumn() {
        return this._monthsPerColumn.value;
    }

    set monthsPerColumn(value) {
        this._monthsPerColumn.value = value;
    }
    
    get monthsPerRow() {
        return this._monthsPerRow.value;
    }

    set monthsPerRow(value) {
        this._monthsPerRow.value = value;
    }

    initialize() {
        this._showWeekDaysInTheRow = new Switch({
            captionOff: 'Column',
            captionOn: 'Row'
        });
        this._reverseDirection = new Switch({
            captionOff: 'Left to right',
            captionOn: 'Right to left'
        });
        this._showMonthsInTheRow = new Switch({
            captionOff: 'Column',
            captionOn: 'Row'
        });
        this._monthsPerColumn = new Range({
            values: [1, 2, 3, 4, 6, 12]
        });
        this._monthsPerRow = new Range({
            values: [1, 2, 3, 4, 6, 12]
        });
    }

    get year() {
        return parseInt(this._year.value);
    }

    set year(value) {
        this._year.value = value;
    }

    render(config) {
        this.wrapUp();
        this.setProperties(config);

        return this.element;
    }

    wrapUp() {
        if (this.hasWrapped) {
            return;
        }
        this.showWeekDaysInTheRowWrapper.appendChild(this._showWeekDaysInTheRow.render());
        this.reverseDirectionWrapper.appendChild(this._reverseDirection.render());
        this.showMonthsInTheRowWrapper.appendChild(this._showMonthsInTheRow.render());
        this.monthsPerColumnWrapper.appendChild(this._monthsPerColumn.render());
        this.monthsPerRowWrapper.appendChild(this._monthsPerRow.render());
        this.hasWrapped = true;
    }

    activeMonthsPerColumn() {
        ClassName.remove(this.monthsPerColumnWrapper, 'inactive');
        ClassName.add(this.monthsPerRowWrapper, 'inactive');
    }

    activeMonthsPerRow() {
        ClassName.remove(this.monthsPerRowWrapper, 'inactive');
        ClassName.add(this.monthsPerColumnWrapper, 'inactive');
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.Year = class extends Object {

    initialize() {
        let element0 = document.createElement('table');element0.setAttribute('class', 'viewgrid');this.owner._element = element0;
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.Month = class extends Object {

    initialize() {
        let element0 = document.createElement('td');this.owner._element = element0;let element1 = document.createTextNode('');element0.appendChild(element1);let element2 = document.createElement('table');let element3 = document.createTextNode('');element2.appendChild(element3);let element4 = document.createElement('thead');let element5 = document.createTextNode('');element4.appendChild(element5);let element6 = document.createElement('tr');let element7 = document.createTextNode('');element6.appendChild(element7);let element8 = document.createElement('td');element8.setAttribute('colspan', '7');let element9 = document.createTextNode('');this.owner._monthName = element9;element8.appendChild(element9);element6.appendChild(element8);let element10 = document.createTextNode('');element6.appendChild(element10);element4.appendChild(element6);let element11 = document.createTextNode('');element4.appendChild(element11);let element12 = document.createElement('tr');this.owner.weekDays = element12;element4.appendChild(element12);let element13 = document.createTextNode('');element4.appendChild(element13);element2.appendChild(element4);let element14 = document.createTextNode('');element2.appendChild(element14);let element15 = document.createElement('tbody');this.owner.dates = element15;element2.appendChild(element15);let element16 = document.createTextNode('');element2.appendChild(element16);element0.appendChild(element2);let element17 = document.createTextNode('');element0.appendChild(element17);
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.Week = class extends Object {

    initialize() {
        let element0 = document.createElement('tr');this.owner._element = element0;
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Week;

engine.widget.calendarGrid.view.Week = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    appendDate(date) {
        if (this.reverseDirection) {
            this.element.prepend(date.render());
        } else {
            this.element.appendChild(date.render());
        }

        return date;
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.Date = class extends Object {

    initialize() {
        let element0 = document.createElement('td');this.owner._element = element0;let element1 = document.createTextNode('');this.owner._text = element1;element0.appendChild(element1);
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Date;

engine.widget.calendarGrid.view.Date = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    set value(value) {
        this._text.nodeValue = value.getDate();
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.DateOut = class extends Object {

    initialize() {
        let element0 = document.createElement('td');this.owner._element = element0;element0.setAttribute('class', 'dateOut');let element1 = document.createTextNode('');this.owner._text = element1;element0.appendChild(element1);
    }
}
}
{
let
    View = engine.gui.component.View,
    ClassName = engine.gui.utility.ClassName,
    Template = engine.widget.calendarGrid.template.DateOut;

engine.widget.calendarGrid.view.DateOut = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    set value(value) {
        if (value) {
            this._text.nodeValue = value.getDate();
        }
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.WeekDay = class extends Object {

    initialize() {
        let element0 = document.createElement('th');this.owner._element = element0;let element1 = document.createTextNode('');this.owner._text = element1;element0.appendChild(element1);
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.WeekDay;

engine.widget.calendarGrid.view.WeekDay = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    set value(value) {
        this._text.nodeValue = value;
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Month,
    Week = engine.widget.calendarGrid.view.Week,
    DateView = engine.widget.calendarGrid.view.Date,
    DateOut = engine.widget.calendarGrid.view.DateOut,
    WeekDay = engine.widget.calendarGrid.view.WeekDay,
    Element = engine.gui.utility.Element,
    ClassName = engine.gui.utility.ClassName;

let self = engine.widget.calendarGrid.view.Month = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    render() {
        this._monthName.nodeValue = this.monthName;
        this.renderDays();
        this.renderWeeks();

        return this.element;
    }

    renderDays() {
        Element.clear(this.weekDays);
        if (this.showWeekDays && !this.showWeekDaysInTheRow) {
            this.days.forEach((dayName, index) => {
                this.appendWeekDay(this.createWeekDay(index));
            });
        }
    }

    renderWeeks() {
        Element.clear(this.dates);
        if (this.showWeekDaysInTheRow) {
            this.renderDayDates(this.weeks);
        } else {
            this.renderWeekDates(this.weeks);
        }
    }

    renderWeekDates(weeks) {
        Element.clear(this.dates);
        weeks.forEach(dates => {
            let week = this.appendWeek(new Week({
                reverseDirection: this.reverseDirection
            }));
            dates.forEach(date => {
                if (date.getMonth() === this.value) {
                    let dateView = week.appendDate(new DateView({value: date}));
                    if (self.isWeekend(date) && this.highlightWeekends) {
                        ClassName.add(dateView.element, 'weekend');
                    }
                } else {
                    week.appendDate(new DateOut({value: this.showDayOut ? date : null}));
                }
            });
        });

        return this;
    }

    renderDayDates(days) {
        Element.clear(this.dates);
        let rows = [];
        days.forEach(dates => {
            dates.forEach((date, dayIndex) => {
                if (!rows[dayIndex]) {
                    rows[dayIndex] = this.appendWeek(new Week({
                        reverseDirection: this.reverseDirection
                    }));
                    this.showWeekDays && rows[dayIndex].appendDate(this.createWeekDay(dayIndex));
                }
                if (date.getMonth() === this.value) {
                    let dateView = rows[dayIndex].appendDate(new DateView({value: date}));
                    if (self.isWeekend(date) && this.highlightWeekends) {
                        ClassName.add(dateView.element, 'weekend');
                    }
                } else {
                    rows[dayIndex].appendDate(new DateOut({value: this.showDayOut && date}));
                }
            });
        });

        return this;
    }

    appendWeekDay(value) {
        if (this.reverseDirection) {
            this.weekDays.prepend(value.render());
        } else {
            this.weekDays.appendChild(value.render());
        }
    }

    appendWeek(week) {
        this.dates.appendChild(week.render());

        return week;
    }

    createWeekDay(index) {
        let dayName = this.days[index];
        return new WeekDay({
            value: this.shortWeekDay ? dayName.short : dayName.full
        })
    }

    static isWeekend(date) {
        return date.getDay() === 0 || date.getDay() === 6;
    }
}
}
{
let
    Object = engine.lang.type.Object;

engine.widget.calendarGrid.template.Row = class extends Object {

    initialize() {
        let element0 = document.createElement('tr');this.owner._element = element0;
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Row;

engine.widget.calendarGrid.view.Row = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    appendMonth(month) {
        this.element.appendChild(month.render());

        return month;
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Year,
    Month = engine.widget.calendarGrid.view.Month,
    Row = engine.widget.calendarGrid.view.Row;

let self = engine.widget.calendarGrid.view.Year = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    render(config, months) {
        if (config.showMonthsInTheRow) {
            this.renderByRows(config, months);
        } else {
            this.renderByColumns(config, months);
        }

        return this.element;
    }

    renderByRows(config, months) {
        this.clear();
        let row = null;
        months.forEach((weeks, month) => {
            if (month % config.monthsPerRow === 0) {
                row = new Row();
                this.appendRow(row);
            }
            row.appendMonth(self.createMonth(month, weeks, config));
        });
    }

    renderByColumns(config, months) {
        this.clear();
        let
            rows = [],
            i = 0;
        months.forEach((weeks, month) => {
            if (!rows[i]) {
                rows[i] = new Row();
                this.appendRow(rows[i]);
            }
            rows[i].appendMonth(self.createMonth(month, weeks, config));
            if (++i === config.monthsPerColumn) {
                i = 0;
            }
        });
    }

    static createMonth(month, weeks, config) {
        return new Month({
            value: month,
            days: config.days,
            weeks: weeks,
            monthName: config.shortMonth ? config.months[month].short : config.months[month].full,
            showWeekDays: config.showWeekDays,
            showDayOut: config.showDayOut,
            showWeekDaysInTheRow: config.showWeekDaysInTheRow,
            shortWeekDay: config.shortWeekDay,
            highlightWeekends: config.highlightWeekends,
            reverseDirection: config.reverseDirection,
        });
    }

    appendRow(row) {
        this.element.appendChild(row.render());

        return row;
    }
}
}
{
let
    View = engine.gui.component.View,
    Template = engine.widget.calendarGrid.template.Layout,
    ControlPanel = engine.widget.calendarGrid.view.ControlPanel,
    Year = engine.widget.calendarGrid.view.Year,
    Obj = engine.lang.utility.Object;

engine.widget.calendarGrid.view.Layout = class extends View {

    get primaryTraits() {
        return [
            Template
        ];
    }

    get events() {
        return {
            changeConfig: {controlPanel: 'changeConfig'}
        };
    }

    get controlPanel() {
        if (!this._controlPanel) {
            this._controlPanel = new ControlPanel();
        }

        return this._controlPanel;
    }

    get year() {
        if (!this._viewGrid) {
            this._viewGrid = new Year();
        }

        return this._viewGrid;
    }

    get config() {
        return Obj.merge(this.controlPanel.config);
    }

    render(config, months) {
        this.controlPanel.render(config);
        this.year.render(config, months);
        this.wrapUp();

        return this.element;
    }

    wrapUp() {
        if (this.hasWrapped) {
            return;
        }
        this.element.appendChild(this.controlPanel.element);
        this.element.appendChild(this.year.element);
        this.wrapper && this.wrapper.appendChild(this.element);
        this.hasWrapped = true;

    }
}
}
{
let
    Component = engine.react.Component,
    Obj = engine.lang.utility.Object,
    Type = engine.lang.utility.Type;

engine.web.component.Controller = class extends Component {

    _constructor(...args) {
        super._constructor(...args);
        this.actionMap = this.actions || {};
        this.subscribeActions(this.actionMap);
    }

    subscribeActions(actions) {
        Obj.forEach(actions, (eventName, action) => {
            this.subscribeAction(eventName, action);
        });
    }

    subscribeAction(eventName, action) {
        this.actionMap[eventName] = action;
        this.on(eventName, (...args) => {
            this.executeAction(eventName, ...args);
        });
    }

    getAction(eventName) {
        let action = this.actionMap[eventName];
        if (Type.isObject(action)) {
            return action;
        }
        action = new action(this.properties || {}, this);
        this.actionMap[eventName] = action;

        return action;
    }

    executeAction(eventName, ...args) {
        if (!this.isActionAllowed(eventName)) {
            return;
        }
        try {
            this.getAction(eventName).execute(...args);
        } catch (throwable) {
            this.handleActionError(throwable);
        }
    }

    isActionAllowed(eventName) {
        return true;
    }

    handleActionError(throwable) {
        throw throwable;
    }

    render(data) {
        this.view.set(data);
    }
}
}
{
self = engine.lang.utility.Date = class {

    static createDateOnMonthAhead(date) {
        let month = date.getMonth();
        date = new Date(date.getTime());
        date.setMonth(date.getMonth() + 1);
        if (date.getMonth() === month + 2) {
            date.setDate(0);
        }

        return date;
    }

    static createMonthWeeks(year, month, fromMonday) {
        let
            date = new Date(year, month, 1),
            delta = fromMonday ? (date.getDay() === 0 ? 6 : date.getDay() - 1) : date.getDay(),
            i = 0,
            j = 0,
            weeks = [];
        date.setDate(date.getDate() - delta);
        while (6 > i++) {
            let week = [];
            while (7 > j++) {
                week.push(new Date(date.getTime()));
                date.setDate(date.getDate() + 1);
            }
            weeks.push(week);
            j = 0;
        }

        return weeks;
    }

    static createYearMonths(year, fromMonday) {
        let
            months = [],
            month = -1;
        while (11 > month++) {
            months.push(self.createMonthWeeks(year, month, fromMonday));
        }

        return months;
    }
}
}
{
let
    WebController = engine.web.component.Controller,
    Obj = engine.lang.utility.Object,
    DateUtil = engine.lang.utility.Date;

engine.widget.calendarGrid.Controller = class extends WebController {

    get events() {
        return {
            changeConfig: {view: 'changeConfig'}
        };
    }

    initialize() {
        this.days = this.config.days;
        this.revertDays = this.days.slice(1);
        this.revertDays.push(this.days[0]);
    }

    onChangeConfig() {
        this.config = Obj.merge(
            this.config,
            this.view.config
        );
        this.render();
    }

    onRender() {
        this.config.days = this.config.fromMonday ? this.revertDays : this.days;
        this.view.render(
            this.config,
            DateUtil.createYearMonths(
                this.config.year,
                this.config.fromMonday
            )
        );
    }
}
}
{
let
    Component = engine.react.Component,
    Layout = engine.widget.calendarGrid.view.Layout,
    Controller = engine.widget.calendarGrid.Controller;

engine.widget.CalendarGrid = class extends Component {

    get defaultProperties() {
        return {
            wrapper: document.body,
            year: (new Date()).getFullYear(),
            fromMonday: true,
            highlightWeekends: true,
            reverseDirection: false,
            shortMonth: false,
            showWeekDays: true,
            showDayOut: true,
            shortWeekDay: true,
            showWeekDaysInTheRow: false,
            showMonthsInTheRow: true,
            monthsPerRow: 4,
            monthsPerColumn: 4,
            months: [
                {full: 'January', short: 'Jan'},
                {full: 'February', short: 'Feb'},
                {full: 'March', short: 'Mar'},
                {full: 'April', short: 'Apr'},
                {full: 'May', short: 'May'},
                {full: 'June', short: 'Jun'},
                {full: 'Jule', short: 'Jul'},
                {full: 'August', short: 'Aug'},
                {full: 'September', short: 'Sep'},
                {full: 'October', short: 'Oct'},
                {full: 'November', short: 'Nov'},
                {full: 'December', short: 'Dec'}
            ],
            days: [
                {full: 'Sunday', short: 'Su'},
                {full: 'Monday', short: 'Mo'},
                {full: 'Tuesday', short: 'Tu'},
                {full: 'Wednesday', short: 'We'},
                {full: 'Thursday', short: 'Th'},
                {full: 'Friday', short: 'Fr'},
                {full: 'Saturday', short: 'St'}
            ]
        };
    }

    get layout() {
        if (!this._layout) {
            this._layout = new Layout({
                wrapper: this.wrapper
            });
        }

        return this._layout;
    }

    get controller() {
        if (!this._controller) {
            this._controller = new Controller({
                view: this.layout,
                config: this.config
            });
        }

        return this._controller;
    }

    initialize() {
        this.config = {
            year: this.year,
            fromMonday: this.fromMonday,
            highlightWeekends: this.highlightWeekends,
            reverseDirection: this.reverseDirection,
            shortMonth: this.shortMonth,
            showWeekDays: this.showWeekDays,
            showDayOut: this.showDayOut,
            shortWeekDay: this.shortWeekDay,
            showWeekDaysInTheRow: this.showWeekDaysInTheRow,
            showMonthsInTheRow: this.showMonthsInTheRow,
            monthsPerRow: this.monthsPerRow,
            monthsPerColumn: this.monthsPerColumn,
            months: this.months,
            days: this.days
        };
    }

    render() {
        this.controller.render();
    }
}
}
