<?php
$appFileName = 'js/app-es6.js';
$argv = [
    '',
    realpath(__DIR__.'/../../design/vendors'),
    realpath(__DIR__.'/../../client/vendors'),
];

require __DIR__.'/../../bin/compile.php';

$argv = [
    '',
    realpath(__DIR__.'/../../client/vendors'),
    __DIR__.'/'.$appFileName,
    'engine.widget.CalendarGrid',
];

require __DIR__.'/../../bin/link.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Calendar Grid</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<script src="<?= $appFileName; ?>"></script>
<script type="text/javascript">
    let grid = new engine.widget.CalendarGrid({
        year: 2018,
        fromMonday: true,
        highlightWeekends: true,
        reverseDirection: false,
        shortMonth: false,
        showWeekDays: true,
        showDayOut: true,
        shortWeekDay: true,
        showWeekDaysInTheRow: false,
        showMonthsInTheRow: true,
        monthsPerRow: 4,
        monthsPerColumn: 4,
        wrapper: document.body
    });
    grid.render();
</script>
</body>
</html>
